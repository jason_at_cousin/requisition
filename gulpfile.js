'use strict';

const gulp = require('gulp');
const changed = require('gulp-changed');
const watch = require('gulp-watch');

gulp.task('copyFiles', function () {
  return watch(['src/*.php'])
    .pipe(
    changed('//srv-sw/southware/Apache24/htdocs/dev/requisition', {
      hasChanged: changed.compareContents
    })
    )
    .pipe(gulp.dest('//srv-sw/southware/Apache24/htdocs/dev/requisition'));
});

gulp.task('copyHelperFiles', function () {
  return watch(['src/helpers/*.php'])
    .pipe(
    changed('//srv-sw/southware/Apache24/htdocs/dev/requisition/helpers', {
      hasChanged: changed.compareContents
    })
    )
    .pipe(gulp.dest('//srv-sw/southware/Apache24/htdocs/dev/requisition/helpers'));
});

gulp.task('runAll', ['copyFiles', 'copyHelperFiles']);
