<?php
require_once('init.php');

//check if form was submited
$id;
$cameFrom;
if (isset($_POST['rl-id'])) {
  $id = $_POST['rl-id'];
  $cameFrom = "form";
  $updated = PushChangesDetail($_POST);
} elseif (isset($_GET['id'])) {
  $id = $_GET['id'];
  $cameFrom = "header";
}

$detailData;
if ($id != "") {
  $detailData = GetSingleDetail($id);
}
if (isset($_GET['closed']) && $_GET['closed'] == 'true') {
  $closed = true;
} else {
  $closed = false;
}
if (isset($_GET['last']) && $_GET['last'] == 'true') {
  $last = true;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Requisition</title>
  <!-- bootstrap styles -->
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/bootstrap-theme.min.css">
  <!-- datatable styles
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.css"> -->
  <link rel="stylesheet" type="text/css" href="DataTables/datatables.min.css">
  <!-- daterangepicker styles -->
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
    body {
      padding-top: 70px;
      padding-bottom: 250px;
    }

    p.navbar-right {
      padding-right: 25px;
    }
  </style>
  <?php echo $id == "" ? "<script>window.location = 'add-header.php';</script>" : ""; ?>
</head>

<body>
  <!-- Nav start -->
  <nav class="navbar navbar-fixed-top <?php echo $env === "prod"?"navbar-default":"navbar-inverse"; ?>">
    <div class="container-fluid">

      <div class="navbar-header">
        <a href="index.php" class="navbar-brand">Requisition</a>
      </div>

      <ul class="nav navbar-nav">
          <li class="active">
            <a href="index.php">Current</a>
          </li>
          <li class="active">
            <a href="#">Detail</a>
          </li>
          <li>
            <a href="closed-reqs.php">History</a>
          </li>
          <li>
            <a href="add-header.php">Add</a>
          </li>
          <li>
            <a href="hist-index.php">Log</a>
          </li>
      </ul>

      <p class="navbar-text navbar-right">
        <?php echo ($env == "prod") ? "Production" : "Development"; ?> Environment
      </p>

    </div>
  </nav>
  <!-- Nav end -->

  <div class="container">



<?php if (isset($_COOKIE['debug']) && $_COOKIE['debug'] === "1") : ?>
  <div class="row">
    <div class="col-sm-8 col-sm-offset-1">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">DEBUG</h3>
        </div>
        <div class="panel-body">
          <pre>$detailData = <?php print_r($detailData) ?></pre>
        </div>
      </div>
    </div>
  </div>
<?php endif ?>





    <?php if (isset($cameFrom) && $cameFrom === "header") : ?>

    <div class="row">
      <div class="col-sm-8 col-sm-offset-1">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Edit Item</h3>
          </div>
          <div class="panel-body">
            <p>After making changes, click Save. You'll be brought back to this page giving you the chance to review and make any additional changes.</p>
          </div>
        </div>
      </div>
    </div>

    <?php elseif (isset($cameFrom) && $cameFrom === "form") : ?>

    <div class="row">
      <div class="col-sm-8 col-sm-offset-1">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Item Saved</h3>
          </div>
          <div class="panel-body">
            <p>Item information was saved. Please make any additional changes and click Save if needed. Once you're finished, please click the 'Finish' button.</p>
            <a class="btn btn-primary btn-sm" href="header.php?id=<?php echo $detailData['rl_rhid']; ?>" role="button">Finish</a>
          </div>
        </div>
      </div>
    </div>

    <?php endif ?>




    <form action="details.php" method="POST" id="form-requisition" class="form-horizontal">
      <!-- next page checks existance of this field to know if form was submitted -->
      <input type="hidden" name="id" id="id" value="<?php echo $id; ?>">
      <input type="hidden" name="rl-rhid" id="rl-rhid" value="<?php echo $detailData['rl_rhid']; ?>">

      <!-- rl-id -->
      <div class="form-group">
        <label for="rl-id" class="col-sm-2 control-label">ID</label>
        <div class="col-sm-2">
          <p class="form-control-static" id="rl-id"><?php echo $detailData['rl_id']; ?></p>
          <input type="hidden" name="rl-id" value="<?php echo $detailData['rl_id']; ?>" >
        </div>
      </div>

      <!-- rl-dateadded -->
      <div class="form-group">
        <label for="rl-dateadded" class="col-sm-2 control-label">Date Added</label>
        <div class="col-sm-2">
          <p class="form-control-static" id="rl-dateadded"><?php echo $detailData['rl_dateadded']; ?></p>
          <!-- <input type="hidden" name="rl-dateadded" value="<?php //echo $detailData['rl_dateadded']; ?>" > -->
        </div>
      </div>

      <!-- rl-partnumber -->
      <div class="form-group">
        <label for="rl-partnumber" class="col-sm-2 control-label">Part Number</label>
        <div class="col-sm-2">
          <input type="text" class="form-control input-sm" id="rl-partnumber" name="rl-partnumber" value="<?php echo $detailData['rl_partnumber']; ?>" data-jval-required autofocus autocomplete="off" <?php echo $closed ? 'disabled' : ''; ?>>
        </div>
      </div>

      <!-- rl-itemname -->
      <div class="form-group">
        <label for="rl-itemname" class="col-sm-2 control-label">Item Name</label>
        <div class="col-sm-4">
          <input type="text" class="form-control input-sm" id="rl-itemname" name="rl-itemname" value="<?php echo $detailData['rl_itemname']; ?>" data-jval-required autocomplete="off" <?php echo $closed ? 'disabled' : ''; ?>>
        </div>
      </div>

      <!-- rl-itemdescription -->
      <div class="form-group">
        <label for="rl-itemdesc" class="col-sm-2 control-label">Item Description</label>
        <div class="col-sm-6">
          <input type="text" class="form-control input-sm" id="rl-itemdesc" name="rl-itemdesc" value="<?php echo $detailData['rl_itemdesc']; ?>" placeholder="Optional" autocomplete="off" <?php echo $closed ? 'disabled' : ''; ?>>
        </div>
      </div>

      <!-- rl-itemlink -->
      <div class="form-group">
        <label for="rl-itemlink" class="col-sm-2 control-label">Item Link</label>
        <div class="col-sm-6">
          <input type="text" class="form-control input-sm" id="rl-itemlink" name="rl-itemlink" value="<?php echo $detailData['rl_itemlink']; ?>" placeholder="Optional" autocomplete="off" <?php echo $closed ? 'disabled' : ''; ?>>
        </div>
      </div>

      <!-- rl-units -->
      <div class="form-group">
        <label for="rl-units" class="col-sm-2 control-label">Unit of Measure</label>
        <div class="col-sm-4">
          <input type="text" class="form-control input-sm" id="rl-units" name="rl-units" value="<?php echo $detailData['rl_units']; ?>" placeholder="Optional" autocomplete="off" <?php echo $closed ? 'disabled' : ''; ?>>
        </div>
      </div>

      <!-- rl-quantity -->
      <div class="form-group">
        <label for="rl-quantity" class="col-sm-2 control-label">Quantity</label>
        <div class="col-sm-1">
          <input type="text" class="form-control input-sm selectme" id="rl-quantity" name="rl-quantity" value="<?php echo $detailData['rl_quantity']; ?>" data-jval-min-num="1" data-jval-required autocomplete="off" <?php echo $closed ? 'disabled' : ''; ?>>
        </div>
      </div>

      <!-- rl-unitprice -->
      <div class="form-group">
        <label for="rl-unitprice" class="col-sm-2 control-label">Unit Price</label>
        <div class="col-sm-2">
          <div class="input-group">
            <div class="input-group-addon"><span class="glyphicon glyphicon-usd" aria-hidden="true"></span></div>
            <input type="text" class="form-control input-sm selectme" id="rl-unitprice" name="rl-unitprice" value="<?php echo $detailData['rl_unitprice']; ?>" data-jval-required autocomplete="off" <?php echo $closed ? 'disabled' : ''; ?>>
          </div>
        </div>
      </div>

      <!-- rl-extended -->
      <div class="form-group">
        <label for="rl-extended" class="col-sm-2 control-label">Total Price</label>
        <div class="col-sm-2">
          <!-- <p class="form-control-static" id="rl-extended"><?php //echo $detailData['rl_extended']; ?></p> -->
          <div class="input-group">
            <div class="input-group-addon"><span class="glyphicon glyphicon-usd" aria-hidden="true"></span></div>
            <input type="text" class="form-control input-sm" id="rl-extended" name="rl-extended" value="<?php echo $detailData['rl_extended']; ?>" readonly>
          </div>
        </div>
      </div>

      <!-- rl-notes -->
      <div class="form-group">
        <label for="rl-notes" class="col-sm-2 control-label">Notes</label>
        <div class="col-sm-6">
          <textarea class="form-control input-sm" name="rl-notes" id="rl-notes" rows="5" placeholder="Optional" <?php echo $closed ? 'disabled' : ''; ?>><?php echo $detailData['rl_notes']; ?></textarea>
        </div>
      </div>

      <!-- rl-datereceived -->
      <div class="form-group">
        <label for="rl-datereceived" class="col-sm-2 control-label">Date Received</label>
        <div class="col-sm-4">
          <input type="text" class="form-control input-sm date-picker" autocomplete="off" id="rl-datereceived" value="<?php echo $detailData['rl_datereceived']; ?>" name="rl-datereceived" placeholder="Optional" <?php echo $closed ? 'disabled' : ''; ?>>
        </div>
      </div>

      <!-- rl-qtyreceived -->
      <div class="form-group">
        <label for="rl-qtyreceived" class="col-sm-2 control-label">Qty Received</label>
        <div class="col-sm-1">
          <input type="text" class="form-control input-sm selectme" id="rl-qtyreceived" name="rl-qtyreceived" value="<?php echo $detailData['rl_qtyreceived']; ?>" placeholder="Optional" autocomplete="off" <?php echo $closed ? 'disabled' : ''; ?>>
        </div>
      </div>

      <!-- rl-itemstatus -->
      <div class="form-group">
        <label for="rl-itemstatus" class="col-sm-2 control-label">Item Status</label>
        <div class="col-sm-4">
          <select name="rl-itemstatus" id="rl-itemstatus" class="form-control" <?php echo $closed ? 'disabled' : ''; ?>>
            <option value="To Be Ordered" <?php echo  $detailData['rl_itemstatus'] == "To Be Ordered" ? "selected" : ""; ?> >To Be Ordered</option>
            <option value="Ordered" <?php echo  $detailData['rl_itemstatus'] == "Ordered" ? "selected" : ""; ?> >Ordered</option>
            <option value="Received" <?php echo  $detailData['rl_itemstatus'] == "Received" ? "selected" : ""; ?> >Received</option>
            <option value="Broken" <?php echo  $detailData['rl_itemstatus'] == "Broken" ? "selected" : ""; ?> >Broken</option>
            <option value="Returned" <?php echo  $detailData['rl_itemstatus'] == "Returned" ? "selected" : ""; ?> >Returned</option>
            <option value="Cancelled" <?php echo  $detailData['rl_itemstatus'] == "Cancelled" ? "selected" : ""; ?> >Cancelled</option>
            <option value="Partial" <?php echo  $detailData['rl_itemstatus'] == "Partial" ? "selected" : ""; ?> >Partial</option>
          </select>
        </div>
      </div>

      <!-- buttons -->
      <div class="form-group">
        <div class="col-sm-1 col-sm-offset-2">
          <button type="submit" id="button-submit" class="btn btn-primary btn-sm jval-submit" <?php echo $closed ? 'disabled' : ''; ?>>Save</button>
        </div>
        <div class="col-sm-1 col-sm-offset-0">
          <a class="btn btn-sm btn-default" href="header.php?id=<?php echo $detailData['rl_rhid']; ?>" role="button">Go Back</a>
          <!-- <button type="button" id="button-cancel" class="btn button-close">Cancel</button> -->
        </div>
        <div class="col-sm-1 col-sm-offset-2">
          <button type="button" id="button-delete" class="btn btn-danger" <?php echo $closed || isset($last) ? 'disabled' : ''; ?> <?php echo isset($last) ? 'title="Can not delete the last item"' : ''; ?>>Delete</button>
          <!-- <a class="btn btn-sm btn-danger" href="header.php?id=<?php echo $detailData['rl_rhid']; ?>&deleteid=<?php echo $detailData['rl_id']; ?>" role="button">Delete</a> -->
        </div>
      </div>

    </form>
  </div>

<!-- Modal Delete -->
  <div id="modal-delete" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <!-- Header -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">You are about to delete this record!</h4>
        </div>
        <!-- Body -->
        <div class="modal-body">
          <p class="">
            Are you sure you want to delete this record?
          </p>
        </div>
        <!-- Footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <a href="header.php?id=<?php echo $detailData['rl_rhid']; ?>&deleteid=<?php echo $detailData['rl_id']; ?>" class="btn btn-danger" role="button">Delete</a>
        </div>
      <!-- End -->
      </div>
    </div>
  </div>
<!-- End Modal Delete -->

  <!-- jquery -->
  <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
  <!-- bootstrap -->
  <script src="js/bootstrap.min.js"></script>
  <!-- datatables
  <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.js"></script> -->
  <script type="text/javascript" charset="utf8" src="DataTables/datatables.min.js"></script>
  <!-- daterangepicker -->
  <script src="js/moment.js"></script>
  <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
  <!-- jval -->
  <script src="http://sw:8082/prod/jval-validator/jval.js"></script>
  <!-- inline -->
  <script>
    // date picker
    $('.date-picker').daterangepicker({
      autoUpdateInput: false,
      autoApply: true,
      locale:{
        format:'YYYY-MM-DD',
        cancelLabel: 'Clear'
      },
      singleDatePicker: true,
      showDropdowns: true
    });
    $('.date-picker').on('apply.daterangepicker', function(ev, picker) {
      console.log('apply');
      $(this).val(picker.startDate.format('YYYY-MM-DD'));
    });
    $('.date-picker').on('cancel.daterangepicker', function(ev, picker) {
      console.log('cancel');
      $(this).val('');
    });
    // calculate total field
    function getTotal(){
      var frmQty = $('#rl-quantity');
      var frmPrice = $('#rl-unitprice');

      var qty = frmQty.val() > 0 ? $('#rl-quantity').val() : 0;
      var price = frmPrice.val() > 0 ? $('#rl-unitprice').val() : 0;
      //$('#rl-extended').html( '$' + (Math.round( ( (qty * price) * 100) ) / 100).toFixed(2) );
      $('#rl-extended').val( (Math.round( ( (qty * price) * 100) ) / 100).toFixed(2) );
    }
    // document ready
    $(document).ready(function(){
      $('#rl-quantity').on('input', getTotal);
      $('#rl-unitprice').on('input', getTotal);
      $('#rl-unitprice').on('change', function(){
        $('#rl-unitprice').val( Number( $('#rl-unitprice').val() ).toFixed(2) );
      });
      getTotal();
      $('#rl-unitprice').val( Number( $('#rl-unitprice').val() ).toFixed(2) );

      // Select the text on click
      var selectMe = $('.selectme');
      selectMe.on('click', function(){
        this.select();
      });
    }); // document ready
  // Modal Delete
    $('#button-delete').click(function () {
      $('#modal-delete').modal('show');
    }); // Modal Delete
  </script>
</body>

</html>
