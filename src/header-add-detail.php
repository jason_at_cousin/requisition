<?php
require_once('init.php');

// check if form was submited
$cameFrom; // detail or header
$id; // header id

if (isset($_POST['rl-rhid'])) {
  $id = $_POST['rl-rhid'];
  $cameFrom = "detail";
  $result = AddDetailEntry($_POST);
} elseif (isset($_GET['id'])) {
  $id = $_GET['id'];
  $cameFrom = "header";
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Requisition</title>
  <!-- bootstrap styles -->
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/bootstrap-theme.min.css">
  <!-- datatable styles
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.css"> -->
  <link rel="stylesheet" type="text/css" href="DataTables/datatables.min.css">
  <!-- daterangepicker styles -->
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css">
  <style>
    body {
      padding-top: 70px;
      padding-bottom: 250px;
    }

    p.navbar-right {
      padding-right: 25px;
    }
  </style>
</head>

<body>
  <!-- Nav start -->
  <nav class="navbar navbar-fixed-top <?php echo $env === "prod"?"navbar-default":"navbar-inverse"; ?>">
    <div class="container-fluid">

      <div class="navbar-header">
        <a href="index.php" class="navbar-brand">Requisition</a>
      </div>

      <ul class="nav navbar-nav">
        <li class="">
          <a href="index.php">Current</a>
        </li>
        <li class="">
          <a href="closed-reqs.php">History</a>
        </li>
        <li class="active">
          <a href="add-header.php">Add</a>
        </li>
      </ul>

      <p class="navbar-text navbar-right">
        <?php echo ($env == "prod") ? "Production" : "Development"; ?> Environment
      </p>

    </div>
  </nav>
  <!-- Nav end -->

  <div class="container">

    <?php if (isset($cameFrom) && $cameFrom === "header") : ?>

    <div class="row">
      <div class="col-sm-8 col-sm-offset-1">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Add Items</h3>
          </div>
          <div class="panel-body">
            <p>Please enter the details of an item and click Add. You'll be brought back to this page giving you the chance to enter more items.</p>
          </div>
        </div>
      </div>
    </div>

    <?php elseif (isset($cameFrom) && $cameFrom === "detail") : ?>

    <div class="row">
      <div class="col-sm-8 col-sm-offset-1">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Item Added</h3>
          </div>
          <div class="panel-body">
            <p>Item was added. Please continue to add additional items. Once you're finished, please click the 'Finish' button.</p>
            <a class="btn btn-primary btn-sm" href="header.php?id=<?php echo $id; ?>" role="button">Finish</a>
          </div>
        </div>
      </div>
    </div>

    <?php endif ?>




    <form action="header-add-detail.php" method="POST" id="form-requisition" class="form-horizontal">
      <!-- next page checks existance of this field to know if form was submitted -->
      <input type="hidden" name="form-submitted" id="form-submitted" value="detail">
      <input type="hidden" name="rl-rhid" id="rl-rhid" value="<?php echo $id; ?>">

      <!-- rl-partnumber -->
      <div class="form-group">
        <label for="rl-partnumber" class="col-sm-2 control-label">Part Number</label>
        <div class="col-sm-2">
          <input type="text" class="form-control input-sm" id="rl-partnumber" name="rl-partnumber" data-jval-required autofocus autocomplete="off">
        </div>
      </div>

      <!-- rl-itemname -->
      <div class="form-group">
        <label for="rl-itemname" class="col-sm-2 control-label">Item Name</label>
        <div class="col-sm-4">
          <input type="text" class="form-control input-sm" id="rl-itemname" name="rl-itemname" data-jval-required autocomplete="off">
        </div>
      </div>

      <!-- rl-itemdescription -->
      <div class="form-group">
        <label for="rl-itemdesc" class="col-sm-2 control-label">Item Description</label>
        <div class="col-sm-6">
          <input type="text" class="form-control input-sm" id="rl-itemdesc" name="rl-itemdesc" placeholder="Optional" autocomplete="off">
        </div>
      </div>

      <!-- rl-itemlink -->
      <div class="form-group">
        <label for="rl-itemlink" class="col-sm-2 control-label">Item Link</label>
        <div class="col-sm-6">
          <input type="text" class="form-control input-sm" id="rl-itemlink" name="rl-itemlink" placeholder="Optional" autocomplete="off">
        </div>
      </div>

      <!-- rl-units -->
      <div class="form-group">
        <label for="rl-units" class="col-sm-2 control-label">Unit of Measure</label>
        <div class="col-sm-4">
          <input type="text" class="form-control input-sm" id="rl-units" name="rl-units" placeholder="Optional" autocomplete="off">
        </div>
      </div>

      <!-- rl-quantity -->
      <div class="form-group">
        <label for="rl-quantity" class="col-sm-2 control-label">Quantity</label>
        <div class="col-sm-1">
          <input type="text" value="1" class="form-control input-sm selectme" id="rl-quantity" name="rl-quantity" data-jval-required autocomplete="off">
        </div>
      </div>

      <!-- rl-unitprice -->
      <div class="form-group">
        <label for="rl-unitprice" class="col-sm-2 control-label">Unit Price</label>
        <div class="col-sm-2">
          <input type="text" class="form-control input-sm selectme" id="rl-unitprice" name="rl-unitprice" data-jval-required autocomplete="off">
        </div>
      </div>

      <!-- rl-extended -->
      <div class="form-group">
        <label for="rl-extended" class="col-sm-2 control-label">Total Price</label>
        <div class="col-sm-2">
          <input type="text" class="form-control input-sm" id="rl-extended" name="rl-extended" data-jval-required readonly autocomplete="off">
        </div>
      </div>

      <!-- rl-notes -->
      <div class="form-group">
        <label for="rl-notes" class="col-sm-2 control-label">Notes</label>
        <div class="col-sm-6">
          <textarea class="form-control input-sm" name="rl-notes" id="rl-notes" rows="5" placeholder="Optional"></textarea>
        </div>
      </div>

      <!-- rl-datereceived -->
      <div class="form-group">
        <label for="rl-datereceived" class="col-sm-2 control-label">Date Received</label>
        <div class="col-sm-4">
          <input type="text" class="form-control input-sm date-picker" autocomplete="off" id="rl-datereceived" name="rl-datereceived" placeholder="Optional" autocomplete="off">
        </div>
      </div>

      <!-- rl-qtyreceived -->
      <div class="form-group">
        <label for="rl-qtyreceived" class="col-sm-2 control-label">Qty Received</label>
        <div class="col-sm-1">
          <input type="text" class="form-control input-sm" id="rl-qtyreceived" name="rl-qtyreceived" placeholder="Optional" autocomplete="off">
        </div>
      </div>

      <!-- rl-itemstatus -->
      <div class="form-group">
        <label for="rl-itemstatus" class="col-sm-2 control-label">Item Status</label>
        <div class="col-sm-4">
          <select name="rl-itemstatus" id="rl-itemstatus" class="form-control">
            <option value="Requested" selected>Requested</option>
            <option value="Waiting Mgmt Approval">Waiting Mgmt Approval</option>
            <option value="Questions">Questions</option>
            <option value="Ordered">Ordered</option>
            <option value="Cancelled">Cancelled</option>
            <option value="Partial Shipment">Partial Shipment</option>
            <option value="Returned">Returned</option>
            <option value="Closed">Closed</option>
          </select>
        </div>
      </div>

      <!-- buttons -->
      <div class="form-group">
        <div class="col-sm-1 col-sm-offset-2">
          <button type="submit" id="button-submit" class="btn btn-sm btn-primary jval-submit">Add</button>
        </div>
        <div class="col-sm-1 col-sm-offset-0">
          <a class="btn btn-sm btn-default" href="header.php?id=<?php echo $id; ?>" role="button">Cancel</a>
        </div>
      </div>

    </form>
  </div>
  <!-- jquery -->
  <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
  <!-- bootstrap -->
  <script src="js/bootstrap.min.js"></script>
  <!-- datatables
  <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.js"></script> -->
  <script type="text/javascript" charset="utf8" src="DataTables/datatables.min.js"></script>
  <!-- daterangepicker -->
  <script src="js/moment.js"></script>
  <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
  <!-- jval -->
  <script src="http://sw:8082/prod/jval-validator/jval.js?v=2"></script>
  <!-- inline -->
  <script>
    $('.date-picker').daterangepicker({
      autoUpdateInput: false,
      autoApply: true,
      locale:{
        format:'YYYY-MM-DD',
        cancelLabel: 'Clear'
      },
      singleDatePicker: true,
      showDropdowns: true
    });
    $('.date-picker').on('apply.daterangepicker', function(ev, picker) {
      console.log('apply');
      $(this).val(picker.startDate.format('YYYY-MM-DD'));
    });
    $('.date-picker').on('cancel.daterangepicker', function(ev, picker) {
      console.log('cancel');
      $(this).val('');
    });

    function getTotal(){
      var frmQty = $('#rl-quantity');
      var frmPrice = $('#rl-unitprice');

      var qty = frmQty.val() > 0 ? $('#rl-quantity').val() : 0;
      var price = frmPrice.val() > 0 ? $('#rl-unitprice').val() : 0;

      $('#rl-extended').val( (Math.round( ( (qty * price) * 100) ) / 100).toFixed(2) );
    }
    $(document).ready(function(){
      // calculate and round amounts and totals
      $('#rl-quantity').on('input', getTotal);
      $('#rl-unitprice').on('input', getTotal);
      $('#rl-unitprice').on('change', function(){
        $('#rl-unitprice').val( Number( $('#rl-unitprice').val() ).toFixed(2) );
      });
      getTotal();
      $('#rl-unitprice').val( Number( $('#rl-unitprice').val() ).toFixed(2) );

      // Select the text on click
      var selectMe = $('.selectme');
      selectMe.on('click', function(){
          this.select();
      });
    });
  </script>
</body>

</html>
