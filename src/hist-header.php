<?php
require_once('init.php');
if (!empty($_GET['id'])) {
    $sql = "SELECT * FROM dbo.CCA_REQUISITION_HEADER_HIST WHERE dbo.CCA_REQUISITION_HEADER_HIST.RH_ID = " . $_GET['id'] . " ORDER BY dbo.CCA_REQUISITION_HEADER_HIST.HIST_ID ASC";
}
?>
<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Requisition</title>

    <link rel="icon" href="favicon16x16.ico">
    <link rel="icon" href="favicon.ico">
    <!-- bootstrap styles -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <!-- datatable styles
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.css"> -->
    <link rel="stylesheet" type="text/css" href="DataTables/datatables.min.css">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        body {
            padding-top: 70px;
            padding-bottom: 250px;
        }

        #table-container {
            padding-left: 40px;
            padding-right: 40px;
            margin-right: 40px;
        }

        p.navbar-right {
            padding-right: 25px;
        }
    </style>
</head>
<body>
    <nav class="navbar navbar-fixed-top <?php echo $env === "prod"?"navbar-default":"navbar-inverse"; ?>">
        <div class="container-fluid">

            <div class="navbar-header">
                <a href="index.php" class="navbar-brand">Requisition</a>
            </div>

            <ul class="nav navbar-nav">
                <li class="">
                    <a href="index.php">Current</a>
                </li>
                <li class="">
                    <a href="closed-reqs.php">History</a>
                </li>
                <li class="">
                    <a href="hist-index.php">Log</a>
                </li>
                <li class="active">
                    <a href="#">Log Header</a>
                </li>
            </ul>

            <p class="navbar-text navbar-right">
                <?php echo ($env == "prod") ? "Production" : "Development"; ?> Environment : <a href="closed-reqs.php?env=<?php echo ($env == "prod") ? "dev" : "prod"; ?>" class="navbar-link">Change</a>
            </p>

        </div>
    </nav>
    <div id="table-container">
            <table id="maintable" class="table table-striped table-hover table-condensed">
                <thead>
                    <tr>
                        <th>Action</th>
                        <th>Id</th>
                        <th>Date Added</th>
                        <th>Deliver By</th>
                        <th>Requested For</th>
                        <th>Request User</th>
                        <th>Request Dept</th>
                        <th>Supplier Name</th>
                        <th>Supplier Address</th>
                        <th>Supplier Phone</th>
                        <th>Supplier Email</th>
                        <th>Supplier Contact</th>
                        <th>Ship To</th>
                        <th>Dept Approval</th>
                        <th>Dept Approval Date</th>
                        <th>Dept Approval User</th>
                        <th>Mgt Approval</th>
                        <th>Mgt Approval Date</th>
                        <th>Mgt Approval User</th>
                        <th>Request Total</th>
                        <th>Last Updated</th>
                        <th>Last Updated By</th>
                        <th>Notes</th>
                        <th>Justification</th>
                        <th>Status</th>
                        <th>Purchaser</th>
                        <th>Ordered On</th>
                        <th>Shipping Price</th>
                        <th>Shipping Method</th>
                        <th>Payment Type</th>
                        <th>Payee</th>
                        <th>Tax</th>
                    </tr>
                </thead>
                <tbody>
<?PHP
// Connect to the DB
$pdoConnection = new PDO($PDO_CONNECT_STMT, $PDO_USER, $PDO_PASSWORD);

// Execute the query and for each result row, make a table row
$cntr = 0;
foreach ($pdoConnection->query($sql, PDO::FETCH_ASSOC) as $row) {
    echo "<tr>";

    echo "<td>" . $row['HIST_ACTION']. "</td>";

    // Id
    echo "<td>";
    echo "<a href=\"hist-header.php?id=" . $row['RH_ID'] . "\">" . $row['HIST_ID'] . "</a>";
    echo "</td>";

    // Date Added
    $matches;
    preg_match("/^20\d\d-\d{1,2}-\d{1,2}/", $row['RH_DATEADDED'], $matches);
    echo "<td>" . (isset($matches[0]) ? $matches[0] : "") . "</td>";

    // Deliver By
    $matches = null;
    preg_match("/^20\d\d-\d{1,2}-\d{1,2}/", $row['RH_DATEREQUESTED'], $matches);
    echo "<td>" . (isset($matches[0]) ? $matches[0] : "") . "</td>";

    echo "<td>" . $row['RH_REQUESTEDFOR']. "</td>";
    echo "<td>" . $row['RH_REQUSER']. "</td>";
    echo "<td>" . $row['RH_REQDEPT']. "</td>";
    echo "<td>" . $row['RH_SUPPLIERNAME']. "</td>";
    echo "<td>" . $row['RH_SUPPLIERADDRESS']. "</td>";
    echo "<td>" . $row['RH_SUPPLIERPHONE']. "</td>";
    echo "<td>" . $row['RH_SUPPLIEREMAIL']. "</td>";
    echo "<td>" . $row['RH_SUPPLIERCONTACT']. "</td>";
    echo "<td>" . $row['RH_SHIPTOADDR']. "</td>";
    echo "<td>" . $row['RH_DEPTAPPROVAL']. "</td>";

    $matches = null;
    preg_match("/^20\d\d-\d{1,2}-\d{1,2}/", $row['RH_DEPTAPPROVALDATE'], $matches);
    echo "<td>" . (isset($matches[0]) ? $matches[0] : "") . "</td>";

    echo "<td>" . $row['RH_DEPTAPPROVALUSER']. "</td>";
    echo "<td>" . $row['RH_MGTAPPROVAL']. "</td>";

    $matches = null;
    preg_match("/^20\d\d-\d{1,2}-\d{1,2}/", $row['RH_MGTAPPROVALDATE'], $matches);
    echo "<td>" . (isset($matches[0]) ? $matches[0] : "") . "</td>";

    echo "<td>" . $row['RH_MGTAPPROVALUSER']. "</td>";
    echo "<td>\$" . number_format(round($row['RH_REQTOTAL'], 2), 2) . "</td>";

    $matches = null;
    preg_match("/^20\d\d-\d{1,2}-\d{1,2}/", $row['RH_LASTUPDATED'], $matches);
    echo "<td>" . (isset($matches[0]) ? $matches[0] : "") . "</td>";

    echo "<td>" . $row['RH_LASTUSER']. "</td>";
    echo "<td>" . $row['RH_NOTES']. "</td>";
    echo "<td>" . $row['RH_JUSTIFICATION']. "</td>";
    echo "<td>" . $row['RH_STATUS']. "</td>";
    echo "<td>" . $row['RH_PURCHASER']. "</td>";

    // order date
    $matches = null;
    preg_match("/^20\d\d-\d{1,2}-\d{1,2}/", $row['RH_ORDERED_DATETIME'], $matches);
    echo "<td>" . (isset($matches[0]) ? $matches[0] : "") . "</td>";

    echo "<td>\$" . number_format(round($row['RH_SHIPPINGAMOUNT'], 2), 2) . "</td>";
    echo "<td>" . $row['RH_SHIPPINGMETHOD']. "</td>";
    echo "<td>" . $row['RH_PAYMENTTYPE']. "</td>";
    echo "<td>" . $row['RH_PAYEE']. "</td>";
    echo "<td>\$" . number_format(round($row['RH_TAX'], 2), 2) . "</td>";
    echo "</tr>";
    $cntr++;
}

// Check if cntr increased. If not, there are no results so well give a blank row
if ($cntr == 0) {
    // echo "<tr>";
    // echo "<td><empty></td>";
    // echo "<td><empty></td>";
    // echo "<td><empty></td>";
    // echo "</tr>";
}

// Kill the connection to the DB
$pdoConnection = null;
?>
            </tbody>
        </table>
    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <!-- Datatables
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.js"></script> -->
    <script type="text/javascript" src="DataTables/datatables.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#maintable').DataTable({
                stateSave: true,
                "stateDuration": 60*60*24*180,
                "processing": true,
                "language":{
                    "loadingRecords": "Loading, please wait...",
                    "processing": "Working, please wait..."
                },
                fixedHeader: {
                    header: true,
                    headerOffset: 50,
                    footer: true
                }
            });
        });
    </script>
</body>
</html>
