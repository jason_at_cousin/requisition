<?php
/**
 * Display Errors
 */
ini_set('display_errors', 1);
error_reporting(E_ALL);

/**
 * Sen $env to prod or dev
 */
$env = "prod";
if (isset($_GET['env']) && ($_GET['env'] == "prod" || $_GET['env'] == "dev")) {
  $env = $_GET['env'];
  setcookie("env", $env, time() + 7776000, "/");
} else {
  if (isset($_COOKIE['env'])) {
    if ($_COOKIE['env'] == "prod") {
      $env = "prod";
    } else {
      $env = "dev";
    }
  } else {
    setcookie("env", "prod", time() + 7776000, "/");
  }
}

/**
 * Get/Set user name, redirect if not set
 */
if (isset($_GET['user']) && !isset($_COOKIE['userName'])) {
  setcookie("userName", $_GET['user'], time() + 7776000, "/");
} elseif (!isset($_GET['user']) && !isset($_COOKIE['userName'])) {
  echo "<script> window.location = 'http://cc'; </script>";
}
