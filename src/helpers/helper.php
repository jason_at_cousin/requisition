<?php
// Set the root directory (currently being set in init.ini)
// $_SESSION['moduleroot'] = preg_replace('/(\/|\\\)?\w+\.?\w*(\/|\\\)?$/i', '', __DIR__);

/******************************************************************************
 *  DELETE FUNCTIONS  *********************************************************
 ******************************************************************************/

/**
 *  Delete an entire record (header and details)
 * @param int - $id is the id of the record from the header table
 * @return bool - true on success
 */
function DeleteById($id)
{
  // Connect to db
  try {
    $pdoConn = new PDO($GLOBALS['PDO_CONNECT_STMT'], $GLOBALS['PDO_USER'], $GLOBALS['PDO_PASSWORD']);
    $pdoConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  } catch (PDOException $e) {
    printExceptionAndDie($e);
  }

  // PDO Statement
  $sql = "DELETE
      FROM dbo.CCA_REQUISITION_DETAIL
      WHERE dbo.CCA_REQUISITION_DETAIL.RL_RHID = :id ";
  $sqlStatement = $pdoConn->prepare($sql);

  // PDO Parameters
  $paramsSql[':id'] = $id;

  // PDO Execute Statement
  try {
    $sqlStatement->execute($paramsSql);
  } catch (PDOException $e) {
    printExceptionAndDie($e);
  }

  // Reset vars to be used in next query
  $sqlStatement = null;
  $paramsSql    = null;

  // PDO Statement
  $sql = "DELETE
      FROM dbo.CCA_REQUISITION_HEADER
      WHERE dbo.CCA_REQUISITION_HEADER.RH_ID = :id ";
  $sqlStatement = $pdoConn->prepare($sql);

  // PDO Parameters
  $paramsSql[':id'] = $id;

  // PDO Execute Statement
  try {
    $sqlStatement->execute($paramsSql);
  } catch (PDOException $e) {
    printExceptionAndDie($e);
  }

  // Kill DB Connection
  $pdoConn = $sqlStatement = null;

  // Return True
  return true;
}

/**
 * Delete a single detail record and update total
 * @param int - $detailId is the id of the detail record to delete
 * @param int - $headerId is the id of the header whose total needs recalculated
 * @return bool - true on success
 */
function DeleteDetailById($detailId, $headerId)
{
  // Connect to db
  try {
    $pdoConn = new PDO($GLOBALS['PDO_CONNECT_STMT'], $GLOBALS['PDO_USER'], $GLOBALS['PDO_PASSWORD']);
    $pdoConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  } catch (PDOException $e) {
    printExceptionAndDie($e);
  }

  // PDO SQL Statement
  $sql = "DELETE
      FROM dbo.CCA_REQUISITION_DETAIL
      WHERE dbo.CCA_REQUISITION_DETAIL.RL_ID = :id ";
  $sqlStatement = $pdoConn->prepare($sql);

  // PDO Parameters
  $paramsSql[':id'] = $detailId;

  // PDO Execute Statement
  try {
    $sqlStatement->execute($paramsSql);
  } catch (PDOException $e) {
    printExceptionAndDie($e);
  }

  // Close DB Connection
  $pdoConn = $sqlStatement = null;

  // Update the header
  UpdateHeaderTotal($headerId, $currentUser, GetFormattedDate());

  // Return true upon success
  return true;
}

/******************************************************************************
 *  GET FUNCTIONS  ************************************************************
 ******************************************************************************/

function GetDetails($id)
{
  // Connect the the database
  try {
    $pdoConn = new PDO($GLOBALS['PDO_CONNECT_STMT'], $GLOBALS['PDO_USER'], $GLOBALS['PDO_PASSWORD']);
    $pdoConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  } catch (PDOException $e) {
    echo("<pre>");
    echo "Message:\n", $e->getMessage(), "\n\n";
    echo "File/Line:\n", $e->getFile(), ", ", $e->getLine(), "\n\n";
    echo "Trace:\n", $e->getTraceAsString(), "\n";
    echo("</pre>");
    $pdoConn = $sqlStatement = null;
    die;
  }
  // PDO Statement
  $sqlStatement = $pdoConn->prepare(
  "SELECT
        dbo.CCA_REQUISITION_DETAIL.RL_ID,
        dbo.CCA_REQUISITION_DETAIL.RL_RHID,
        dbo.CCA_REQUISITION_DETAIL.RL_DATEADDED,
        dbo.CCA_REQUISITION_DETAIL.RL_PARTNUMBER,
        dbo.CCA_REQUISITION_DETAIL.RL_ITEMNAME,
        dbo.CCA_REQUISITION_DETAIL.RL_ITEMDESC,
        dbo.CCA_REQUISITION_DETAIL.RL_ITEMLINK,
        dbo.CCA_REQUISITION_DETAIL.RL_UNITS,
        dbo.CCA_REQUISITION_DETAIL.RL_QUANTITY,
        dbo.CCA_REQUISITION_DETAIL.RL_UNITPRICE,
        dbo.CCA_REQUISITION_DETAIL.RL_EXTENDED,
        dbo.CCA_REQUISITION_DETAIL.RL_NOTES,
        dbo.CCA_REQUISITION_DETAIL.RL_DATERECEIVED,
        dbo.CCA_REQUISITION_DETAIL.RL_QTYRECEIVED,
        dbo.CCA_REQUISITION_DETAIL.RL_ITEMSTATUS
      FROM
        dbo.CCA_REQUISITION_DETAIL
      WHERE
        dbo.CCA_REQUISITION_DETAIL.RL_RHID = :id"
  );
  // PDO Parameters
  $paramsSql[':id'] = $id;

  // PDO Execute Statement
  try {
    $sqlStatement->execute($paramsSql);
  } catch (PDOException $e) {
    echo("<pre>");
    echo "Message:\n", $e->getMessage(), "\n\n";
    echo "File/Line:\n", $e->getFile(), ", ", $e->getLine(), "\n\n";
    echo "Trace:\n", $e->getTraceAsString(), "\n";
    echo("</pre>");
    $pdoConn = $sqlStatement = null;
    die;
  }

  // PDO Fetch While
  $resultData = array();
  $idx = 0;
  while ($row = $sqlStatement->fetch(PDO::FETCH_ASSOC)) {
    foreach ($row as $key => $value) {
      // If any of the dates are 1900-01-01 00:00:00 the set to null
      if (preg_match("/^1900-01-01/", $value)) {
        $value = "";
      }
      $resultData[$idx][strtolower($key)] = $value;
    }
    $idx++;
  }
  // Kill the database connection
  $pdoConn = $sqlStatement = null;
  // Return false if $resultData had 0 elements
  if (count($resultData) <= 0) {
    return false;
  }
  // Return $resultData which holds all of the results
  return $resultData;
}

function GetDetailsHist($id)
{
  // Connect the the database
  try {
    $pdoConn = new PDO($GLOBALS['PDO_CONNECT_STMT'], $GLOBALS['PDO_USER'], $GLOBALS['PDO_PASSWORD']);
    $pdoConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  } catch (PDOException $e) {
    echo("<pre>");
    echo "Message:\n", $e->getMessage(), "\n\n";
    echo "File/Line:\n", $e->getFile(), ", ", $e->getLine(), "\n\n";
    echo "Trace:\n", $e->getTraceAsString(), "\n";
    echo("</pre>");
    $pdoConn = $sqlStatement = null;
    die;
  }
  // PDO Statement
  $sqlStatement = $pdoConn->prepare(
  "SELECT
        dbo.CCA_REQUISITION_DETAIL_HIST.HIST_ID,
        dbo.CCA_REQUISITION_DETAIL_HIST.HIST_DATEADDED,
        dbo.CCA_REQUISITION_DETAIL_HIST.RL_ID,
        dbo.CCA_REQUISITION_DETAIL_HIST.RL_RHID,
        dbo.CCA_REQUISITION_DETAIL_HIST.RL_DATEADDED,
        dbo.CCA_REQUISITION_DETAIL_HIST.RL_PARTNUMBER,
        dbo.CCA_REQUISITION_DETAIL_HIST.RL_ITEMNAME,
        dbo.CCA_REQUISITION_DETAIL_HIST.RL_ITEMDESC,
        dbo.CCA_REQUISITION_DETAIL_HIST.RL_ITEMLINK,
        dbo.CCA_REQUISITION_DETAIL_HIST.RL_UNITS,
        dbo.CCA_REQUISITION_DETAIL_HIST.RL_QUANTITY,
        dbo.CCA_REQUISITION_DETAIL_HIST.RL_UNITPRICE,
        dbo.CCA_REQUISITION_DETAIL_HIST.RL_EXTENDED,
        dbo.CCA_REQUISITION_DETAIL_HIST.RL_NOTES,
        dbo.CCA_REQUISITION_DETAIL_HIST.RL_DATERECEIVED,
        dbo.CCA_REQUISITION_DETAIL_HIST.RL_QTYRECEIVED,
        dbo.CCA_REQUISITION_DETAIL_HIST.RL_ITEMSTATUS,
        dbo.CCA_REQUISITION_DETAIL_HIST.HIST_ACTION
      FROM
        dbo.CCA_REQUISITION_DETAIL_HIST
      WHERE
        dbo.CCA_REQUISITION_DETAIL_HIST.RL_RHID = :id"
  );
  // PDO Parameters
  $paramsSql[':id'] = $id;
  // PDO Fetch While
  $queryData = array();
  $idx = 0;
  while ($row = $sqlStatement->fetch(PDO::FETCH_ASSOC)) {
    foreach ($row as $key => $value) {
      // If any of the dates are 1900-01-01 00:00:00 the set to null
      if (preg_match("/^1900-01-01/", $value)) {
        $value = "";
      }
      $queryData[$idx][strtolower($key)] = $value;
    }
    ++$idx;
  }
  // Kill the database connection
  $pdoConn = $sqlStatement = null;
  // Return false if $queryData has no results
  if (count($queryData) <= 0) {
    return false;
  }
  // Return $queryData which holds all the results
  return $queryData;
}

function GetHeader($id)
{
  // Connect the the database
  try {
    $pdoConn = new PDO($GLOBALS['PDO_CONNECT_STMT'], $GLOBALS['PDO_USER'], $GLOBALS['PDO_PASSWORD']);
    $pdoConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  } catch (PDOException $e) {
    echo("<pre>");
    echo "Message:\n", $e->getMessage(), "\n\n";
    echo "File/Line:\n", $e->getFile(), ", ", $e->getLine(), "\n\n";
    echo "Trace:\n", $e->getTraceAsString(), "\n";
    echo("</pre>");
    $pdoConn = $sqlStatement = null;
    die;
  }
  // PDO Statement
  $sqlStatement = $pdoConn->prepare(
  "SELECT TOP 1 *
      FROM dbo.CCA_REQUISITION_HEADER
      WHERE dbo.CCA_REQUISITION_HEADER.RH_ID = :id"
  );
  // PDO Parameters
  $paramsSql[':id'] = $id;

  // PDO Execute Statement
  try {
    $sqlStatement->execute($paramsSql);
  } catch (PDOException $e) {
    echo("<pre>");
    echo "Message:\n", $e->getMessage(), "\n\n";
    echo "File/Line:\n", $e->getFile(), ", ", $e->getLine(), "\n\n";
    echo "Trace:\n", $e->getTraceAsString(), "\n";
    echo("</pre>");
    $pdoConn = $sqlStatement = null;
    die;
  }

  // PDO Fetch Result
  $result = $sqlStatement->fetch(PDO::FETCH_ASSOC);
  // Kill the database connection
  $pdoConn = $sqlStatement = null;
  if (count($result) <= 0) {
    return false;
  }
  // $retVal will hold the results to return
  $retVal;
  // Loop through each column and fix the dates
  foreach ($result as $key => $column) {
  // If any of the dates are 1900-01-01 00:00:00 the set to null
    if (preg_match("/^1900-01-01/", $column)) {
      $column = "";
    }
    $matches; // Will hold matches from preg_match
  // if column is not rh_ordered_datetime, pull out yyyy-mm-dd if date
    if (strtolower($key) !== 'rh_ordered_datetime') {
      preg_match("/^20\d\d-\d{1,2}-\d{1,2}/", $column, $matches);
      $column = isset($matches[0]) ? $matches[0] : $column;
    } else { // if column IS rh_ordered_datetime, pull out yyyy-mm-dd hh:mm
      preg_match("/^20\d\d-\d{1,2}-\d{1,2} \d\d:\d\d/", $column, $matches);
      $column = isset($matches[0]) ? $matches[0] : $column;
    }
    $retVal[strtolower($key)] = $column;
  }
  // Return $retVal
  return $retVal;
}

function GetHeaderTotal($id)
{
  // Connect the the database
  try {
    $pdoConn = new PDO($GLOBALS['PDO_CONNECT_STMT'], $GLOBALS['PDO_USER'], $GLOBALS['PDO_PASSWORD']);
    $pdoConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  } catch (PDOException $e) {
    echo("<pre>");
    echo "Message:\n", $e->getMessage(), "\n\n";
    echo "File/Line:\n", $e->getFile(), ", ", $e->getLine(), "\n\n";
    echo "Trace:\n", $e->getTraceAsString(), "\n";
    echo("</pre>");
    $pdoConn = $sqlStatement = null;
    die;
  }
  // PDO Statement
  $sqlStatement = $pdoConn->prepare(
  "SELECT dbo.CCA_REQUISITION_HEADER.RH_REQTOTAL AS total
      FROM dbo.CCA_REQUISITION_HEADER
      WHERE dbo.CCA_REQUISITION_HEADER.RH_ID = :id"
  );
  // PDO Parameters
  $paramsSql[':id'] = $id;

  // PDO Execute Statement
  try {
    $sqlStatement->execute($paramsSql);
  } catch (PDOException $e) {
    echo("<pre>");
    echo "Message:\n", $e->getMessage(), "\n\n";
    echo "File/Line:\n", $e->getFile(), ", ", $e->getLine(), "\n\n";
    echo "Trace:\n", $e->getTraceAsString(), "\n";
    echo("</pre>");
    $pdoConn = $sqlStatement = null;
    die;
  }

  // PDO Fetch Result
  $result = $sqlStatement->fetch(PDO::FETCH_ASSOC);
  // Kill the database connection
  $pdoConn = $sqlStatement = null;
  if (count($result) <= 0) {
    return false;
  }

  // Return
  return $result['total'];
}

function GetHeaders()
{
  // Connect the the database
  try {
    $pdoConn = new PDO($GLOBALS['PDO_CONNECT_STMT'], $GLOBALS['PDO_USER'], $GLOBALS['PDO_PASSWORD']);
    $pdoConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  } catch (PDOException $e) {
    echo("<pre>");
    echo "Message:\n", $e->getMessage(), "\n\n";
    echo "File/Line:\n", $e->getFile(), ", ", $e->getLine(), "\n\n";
    echo "Trace:\n", $e->getTraceAsString(), "\n";
    echo("</pre>");
    $pdoConn = $sqlStatement = null;
    die;
  }
  // SQL Query
  $usersThatCanViewAllDepts = GetSettings('seeAllDepartments');

  if (array_search($_COOKIE['userName'], $usersThatCanViewAllDepts)) {
    $sql = "SELECT *, (
        SELECT dbo.CCA_REQUISITION_DETAIL.RL_ITEMNAME + ' /// '
        FROM dbo.CCA_REQUISITION_DETAIL
        WHERE dbo.CCA_REQUISITION_DETAIL.RL_RHID = dbo.CCA_REQUISITION_HEADER.RH_ID FOR XML PATH ('')
      ) AS ITEMS
      FROM dbo.CCA_REQUISITION_HEADER
      WHERE
        dbo.CCA_REQUISITION_HEADER.RH_STATUS <> 'Closed' AND
        dbo.CCA_REQUISITION_HEADER.RH_STATUS <> 'Cancelled'"
    ;
  } else {
    $currentDepartment = GetUserDept($_COOKIE['userName']);
    $usersInDepartment = GetUsersInDept($currentDepartment);
    $sql = "SELECT *,
      (
        SELECT dbo.CCA_REQUISITION_DETAIL.RL_ITEMNAME + ' /// '
        FROM dbo.CCA_REQUISITION_DETAIL
        WHERE dbo.CCA_REQUISITION_DETAIL.RL_RHID = dbo.CCA_REQUISITION_HEADER.RH_ID FOR XML PATH ('')
      ) AS ITEMS
      FROM dbo.CCA_REQUISITION_HEADER
      WHERE
        dbo.CCA_REQUISITION_HEADER.RH_STATUS <> 'Closed' AND
        dbo.CCA_REQUISITION_HEADER.RH_STATUS <> 'Cancelled' AND
        dbo.CCA_REQUISITION_HEADER.RH_REQUSER IN ('placeholder'"
    ;
    foreach ($usersInDepartment as $idx => $user) {
      $sql .= ",'" . $user . "'";
    }
    $sql .= ")";
  }

  /* $sql = "SELECT *, (
      SELECT dbo.CCA_REQUISITION_DETAIL.RL_ITEMNAME + ' /// '
      FROM dbo.CCA_REQUISITION_DETAIL
      WHERE dbo.CCA_REQUISITION_DETAIL.RL_RHID = dbo.CCA_REQUISITION_HEADER.RH_ID FOR XML PATH ('')
    ) AS ITEMS
    FROM dbo.CCA_REQUISITION_HEADER
    WHERE dbo.CCA_REQUISITION_HEADER.RH_REQUSER IN (";



    foreach ($usersCanViewAllDepts as $idx => $user) {
      $sql .= "'" . $user . "', ";
    }
    $currentDepartment = GetUserDept($_COOKIE['userName']);
    $usersInDepartment = GetUsersInDept($currentDepartment);



    foreach ($usersInDepartment as $idx => $user) {
      $sql .= "'" . $user . "', ";
    }
  $sql = preg_replace('/,\s+$/', ')', $sql); */

  // PDO Statement
  $sqlStatement = $pdoConn->prepare($sql);
  // PDO Execute Statement
  try {
    $sqlStatement->execute();
  } catch (PDOException $e) {
    echo("<pre>");
    echo "Message:\n", $e->getMessage(), "\n\n";
    echo "File/Line:\n", $e->getFile(), ", ", $e->getLine(), "\n\n";
    echo "Trace:\n", $e->getTraceAsString(), "\n";
    echo("</pre>");
    $pdoConn = $sqlStatement = null;
    die;
  }

  // PDO Fetch Result
  $result = $sqlStatement->fetchAll(PDO::FETCH_ASSOC);
  // Kill the database connection
  $pdoConn = $sqlStatement = null;
  if (count($result) <= 0) {
    return false;
  }
  // $retVal will hold the results to return
  $retVal;
  // Loop through each row
  foreach ($result as $rowNum => $row) {
    // Loop through each column and fix the dates
    foreach ($row as $colName => $column) {
      if (preg_match("/^1900-01-01/", $column)) {
        // If any of the dates are 1900-01-01 00:00:00 the set to null
        $column = "";
      } elseif (strtolower($colName) === 'rh_ordered_datetime') {
        // if column IS rh_ordered_datetime, pull out yyyy-mm-dd hh:mm
        preg_match("/^20\d\d-\d{1,2}-\d{1,2} \d\d:\d\d/", $column, $matches);
        $column = isset($matches[0]) ? $matches[0] : $column;
      } else {
        // if column is date but, not rh_ordered_datetime pull out yyyy-mm-dd
        preg_match("/^20\d\d-\d{1,2}-\d{1,2}/", $column, $matches);
        $column = isset($matches[0]) ? $matches[0] : $column;
      }

      // if (strtolower($key) !== 'rh_ordered_datetime') {
      //   preg_match("/^20\d\d-\d{1,2}-\d{1,2}/", $column, $matches);
      //   $column = isset($matches[0]) ? $matches[0] : $column;
      // } else {
      //   preg_match("/^20\d\d-\d{1,2}-\d{1,2} \d\d:\d\d/", $column, $matches);
      //   $column = isset($matches[0]) ? $matches[0] : $column;
      // }

      $retVal[$rowNum][$colName] = $column;
    }
  }

  // Return $retVal
  if (count($retVal) <= 0) {
    return false;
  }
  return $retVal;
}

function GetClosedHeaders()
{
  // Connect the the database
  try {
    $pdoConn = new PDO($GLOBALS['PDO_CONNECT_STMT'], $GLOBALS['PDO_USER'], $GLOBALS['PDO_PASSWORD']);
    $pdoConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  } catch (PDOException $e) {
    echo("<pre>");
    echo "Message:\n", $e->getMessage(), "\n\n";
    echo "File/Line:\n", $e->getFile(), ", ", $e->getLine(), "\n\n";
    echo "Trace:\n", $e->getTraceAsString(), "\n";
    echo("</pre>");
    $pdoConn = $sqlStatement = null;
    die;
  }
  // SQL Query
  $usersThatCanViewAllDepts = GetSettings('seeAllDepartments');

  if (array_search($_COOKIE['userName'], $usersThatCanViewAllDepts)) {
    $sql = "SELECT TOP 500 *, (
        SELECT dbo.CCA_REQUISITION_DETAIL.RL_ITEMNAME + ' /// '
        FROM dbo.CCA_REQUISITION_DETAIL
        WHERE dbo.CCA_REQUISITION_DETAIL.RL_RHID = dbo.CCA_REQUISITION_HEADER.RH_ID FOR XML PATH ('')
      ) AS ITEMS
      FROM dbo.CCA_REQUISITION_HEADER
      WHERE
        dbo.CCA_REQUISITION_HEADER.RH_STATUS = 'Closed' OR
        dbo.CCA_REQUISITION_HEADER.RH_STATUS = 'Cancelled'"
    ;
  } else {
    $currentDepartment = GetUserDept($_COOKIE['userName']);
    $usersInDepartment = GetUsersInDept($currentDepartment);
    $sql = "SELECT *,
      (
        SELECT dbo.CCA_REQUISITION_DETAIL.RL_ITEMNAME + ' /// '
        FROM dbo.CCA_REQUISITION_DETAIL
        WHERE dbo.CCA_REQUISITION_DETAIL.RL_RHID = dbo.CCA_REQUISITION_HEADER.RH_ID FOR XML PATH ('')
      ) AS ITEMS
      FROM dbo.CCA_REQUISITION_HEADER
      WHERE
        (
          dbo.CCA_REQUISITION_HEADER.RH_STATUS = 'Closed' OR
          dbo.CCA_REQUISITION_HEADER.RH_STATUS = 'Cancelled'
        ) AND
        dbo.CCA_REQUISITION_HEADER.RH_REQUSER IN ('placeholder'"
    ;
    foreach ($usersInDepartment as $idx => $user) {
      $sql .= ",'" . $user . "'";
    }
    $sql .= ")";
  }

  /* $sql = "SELECT *, (
      SELECT dbo.CCA_REQUISITION_DETAIL.RL_ITEMNAME + ' /// '
      FROM dbo.CCA_REQUISITION_DETAIL
      WHERE dbo.CCA_REQUISITION_DETAIL.RL_RHID = dbo.CCA_REQUISITION_HEADER.RH_ID FOR XML PATH ('')
    ) AS ITEMS
    FROM dbo.CCA_REQUISITION_HEADER
    WHERE dbo.CCA_REQUISITION_HEADER.RH_REQUSER IN (";



    foreach ($usersCanViewAllDepts as $idx => $user) {
      $sql .= "'" . $user . "', ";
    }
    $currentDepartment = GetUserDept($_COOKIE['userName']);
    $usersInDepartment = GetUsersInDept($currentDepartment);



    foreach ($usersInDepartment as $idx => $user) {
      $sql .= "'" . $user . "', ";
    }
  $sql = preg_replace('/,\s+$/', ')', $sql); */

  // PDO Statement
  $sqlStatement = $pdoConn->prepare($sql);
  // PDO Execute Statement
  try {
    $sqlStatement->execute();
  } catch (PDOException $e) {
    echo("<pre>");
    echo "Message:\n", $e->getMessage(), "\n\n";
    echo "File/Line:\n", $e->getFile(), ", ", $e->getLine(), "\n\n";
    echo "Trace:\n", $e->getTraceAsString(), "\n";
    echo("</pre>");
    $pdoConn = $sqlStatement = null;
    die;
  }

  // PDO Fetch Result
  $result = $sqlStatement->fetchAll(PDO::FETCH_ASSOC);
  // Kill the database connection
  $pdoConn = $sqlStatement = null;
  if (count($result) <= 0) {
    return false;
  }
  // $retVal will hold the results to return
  $retVal;
  // Loop through each row
  foreach ($result as $rowNum => $row) {
    // Loop through each column and fix the dates
    foreach ($row as $colName => $column) {
      if (preg_match("/^1900-01-01/", $column)) {
        // If any of the dates are 1900-01-01 00:00:00 the set to null
        $column = "";
      } elseif (strtolower($colName) === 'rh_ordered_datetime') {
        // if column IS rh_ordered_datetime, pull out yyyy-mm-dd hh:mm
        preg_match("/^20\d\d-\d{1,2}-\d{1,2} \d\d:\d\d/", $column, $matches);
        $column = isset($matches[0]) ? $matches[0] : $column;
      } else {
        // if column is date but, not rh_ordered_datetime pull out yyyy-mm-dd
        preg_match("/^20\d\d-\d{1,2}-\d{1,2}/", $column, $matches);
        $column = isset($matches[0]) ? $matches[0] : $column;
      }

      // if (strtolower($key) !== 'rh_ordered_datetime') {
      //   preg_match("/^20\d\d-\d{1,2}-\d{1,2}/", $column, $matches);
      //   $column = isset($matches[0]) ? $matches[0] : $column;
      // } else {
      //   preg_match("/^20\d\d-\d{1,2}-\d{1,2} \d\d:\d\d/", $column, $matches);
      //   $column = isset($matches[0]) ? $matches[0] : $column;
      // }

      $retVal[$rowNum][$colName] = $column;
    }
  }

  // Return $retVal
  if (count($retVal) <= 0) {
    return false;
  }
  return $retVal;
}

/**
 * Get header record from history
 * @param string
 * @return void
 */
function GetHeaderHist($id)
{
  // Connect the the database
  try {
    $pdoConn = new PDO($GLOBALS['PDO_CONNECT_STMT'], $GLOBALS['PDO_USER'], $GLOBALS['PDO_PASSWORD']);
    $pdoConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  } catch (PDOException $e) {
    echo("<pre>");
    echo "Message:\n", $e->getMessage(), "\n\n";
    echo "File/Line:\n", $e->getFile(), ", ", $e->getLine(), "\n\n";
    echo "Trace:\n", $e->getTraceAsString(), "\n";
    echo("</pre>");
    $pdoConn = $sqlStatement = null;
    die;
  }
  // PDO Statement
  $sqlStatement = $pdoConn->prepare(
  "SELECT TOP 1
      dbo.CCA_REQUISITION_HEADER_HIST.HIST_ID, dbo.CCA_REQUISITION_HEADER_HIST.HIST_DATEADDED,
      dbo.CCA_REQUISITION_HEADER_HIST.RH_ID, dbo.CCA_REQUISITION_HEADER_HIST.RH_DATEADDED,
      dbo.CCA_REQUISITION_HEADER_HIST.RH_DATEREQUESTED, dbo.CCA_REQUISITION_HEADER_HIST.RH_REQUESTEDFOR,
      dbo.CCA_REQUISITION_HEADER_HIST.RH_REQUSER, dbo.CCA_REQUISITION_HEADER_HIST.RH_REQDEPT,
      dbo.CCA_REQUISITION_HEADER_HIST.RH_SUPPLIERNAME, dbo.CCA_REQUISITION_HEADER_HIST.RH_SUPPLIERADDRESS,
      dbo.CCA_REQUISITION_HEADER_HIST.RH_SUPPLIERPHONE, dbo.CCA_REQUISITION_HEADER_HIST.RH_SUPPLIEREMAIL,
      dbo.CCA_REQUISITION_HEADER_HIST.RH_SUPPLIERCONTACT, dbo.CCA_REQUISITION_HEADER_HIST.RH_SHIPTOADDR,
      dbo.CCA_REQUISITION_HEADER_HIST.RH_DEPTAPPROVAL, dbo.CCA_REQUISITION_HEADER_HIST.RH_DEPTAPPROVALDATE,
      dbo.CCA_REQUISITION_HEADER_HIST.RH_DEPTAPPROVALUSER, dbo.CCA_REQUISITION_HEADER_HIST.RH_MGTAPPROVAL,
      dbo.CCA_REQUISITION_HEADER_HIST.RH_MGTAPPROVALDATE, dbo.CCA_REQUISITION_HEADER_HIST.RH_MGTAPPROVALUSER,
      dbo.CCA_REQUISITION_HEADER_HIST.RH_REQTOTAL, dbo.CCA_REQUISITION_HEADER_HIST.RH_LASTUPDATED,
      dbo.CCA_REQUISITION_HEADER_HIST.RH_LASTUSER, dbo.CCA_REQUISITION_HEADER_HIST.RH_NOTES,
      dbo.CCA_REQUISITION_HEADER_HIST.RH_JUSTIFICATION, dbo.CCA_REQUISITION_HEADER_HIST.RH_STATUS,
      dbo.CCA_REQUISITION_HEADER_HIST.HIST_ACTION, dbo.CCA_REQUISITION_HEADER_HIST.RH_PURCHASER
    FROM
      dbo.CCA_REQUISITION_HEADER_HIST
    WHERE
      dbo.CCA_REQUISITION_HEADER_HIST.RH_ID = :id"
  );
  // PDO Parameters
  $paramsSql[':id'] = $id;

  // PDO Execute Statement
  try {
    $sqlStatement->execute($paramsSql);
  } catch (PDOException $e) {
    echo("<pre>");
    echo "Message:\n", $e->getMessage(), "\n\n";
    echo "File/Line:\n", $e->getFile(), ", ", $e->getLine(), "\n\n";
    echo "Trace:\n", $e->getTraceAsString(), "\n";
    echo("</pre>");
    $pdoConn = $sqlStatement = null;
    die;
  }

  // PDO Fetch Results
  $result = $sqlStatement->fetch(PDO::FETCH_ASSOC);
  $pdoConn = $sqlStatement = null;
  // If 0 results, return false
  if (count($result) <= 0) {
    return false;
  }
  // will hold results that will be returned
  $retVal;
  // Loop through each column of current row
  foreach ($result as $key => $column) {
    // If any of the dates are 1900-01-01 00:00:00 the set to null
    if (preg_match("/^1900-01-01/", $column)) {
      $column = "";
    }
    // Will hold matches from preg_match
    $matches;
    preg_match("/^20\d\d-\d{1,2}-\d{1,2}/", $column, $matches);
    $column = isset($matches[0]) ? $matches[0] : $column;
    $retVal[strtolower($key)] = $column;
  }
  return $retVal;
}

/**
 * Get settings from settings.json. Pass in a string name of a setting branch
 * to retrieve. Passing in a blank string or 'all' will retreive all settings
 * in settings.json.
 * @param string - setting category
 * @return array[string]:mixed - settings
 */
function GetSettings($setting)
{
  $fileName = $_SESSION['moduleroot'] . '/settings.json';
  $fileResourceId = fopen($fileName, 'r');
  $JSONData = fread($fileResourceId, filesize($fileName));
  fclose($fileResourceId);
  $JSONData = json_decode($JSONData, true);
  if (strlen($setting) > 0 && strtolower($setting) !== 'all') {
    // new stuff
    $allSettings = explode(':', $setting);
    foreach ($allSettings as $key => $value) {
      if (isset($JSONData[$value])) {
        $JSONData = $JSONData[$value];
      }
    }
    return $JSONData;

    // if (isset($JSONData[$setting])) {
    //   return $JSONData[$setting];
    // } else {
    //   return false;
    // }
  }
  return $JSONData;
}

/**
 * Pass in an id of a detail record and get back an accociative array holding
 * the data for that single requisition detail entry.
 * @param int - an id of a requisition detail entry
 * @return array[string]:string - array[column name] = column data
 */
function GetSingleDetail($id)
{
  // Connect the the database
  try {
    $pdoConn = new PDO($GLOBALS['PDO_CONNECT_STMT'], $GLOBALS['PDO_USER'], $GLOBALS['PDO_PASSWORD']);
    $pdoConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  } catch (PDOException $e) {
    echo("<pre>");
    echo "Message:\n", $e->getMessage(), "\n\n";
    echo "File/Line:\n", $e->getFile(), ", ", $e->getLine(), "\n\n";
    echo "Trace:\n", $e->getTraceAsString(), "\n";
    echo("</pre>");
    $pdoConn = $sqlStatement = null;
    die;
  }
  // PDO Statement
  $sqlStatement = $pdoConn->prepare(
  "SELECT TOP 1
      dbo.CCA_REQUISITION_DETAIL.RL_ID, dbo.CCA_REQUISITION_DETAIL.RL_RHID,
      dbo.CCA_REQUISITION_DETAIL.RL_DATEADDED, dbo.CCA_REQUISITION_DETAIL.RL_PARTNUMBER,
      dbo.CCA_REQUISITION_DETAIL.RL_ITEMNAME, dbo.CCA_REQUISITION_DETAIL.RL_ITEMDESC,
      dbo.CCA_REQUISITION_DETAIL.RL_ITEMLINK, dbo.CCA_REQUISITION_DETAIL.RL_UNITS,
      dbo.CCA_REQUISITION_DETAIL.RL_QUANTITY, dbo.CCA_REQUISITION_DETAIL.RL_UNITPRICE,
      dbo.CCA_REQUISITION_DETAIL.RL_EXTENDED, dbo.CCA_REQUISITION_DETAIL.RL_NOTES,
      dbo.CCA_REQUISITION_DETAIL.RL_DATERECEIVED, dbo.CCA_REQUISITION_DETAIL.RL_QTYRECEIVED,
      dbo.CCA_REQUISITION_DETAIL.RL_ITEMSTATUS
    FROM
      dbo.CCA_REQUISITION_DETAIL
    WHERE
      dbo.CCA_REQUISITION_DETAIL.RL_ID = :id"
  );
  // PDO Parameters
  $paramsSql[':id'] = $id;

  // PDO Execute Statement
  try {
    $sqlStatement->execute($paramsSql);
  } catch (PDOException $e) {
    echo("<pre>");
    echo "Message:\n", $e->getMessage(), "\n\n";
    echo "File/Line:\n", $e->getFile(), ", ", $e->getLine(), "\n\n";
    echo "Trace:\n", $e->getTraceAsString(), "\n";
    echo("</pre>");
    $pdoConn = $sqlStatement = null;
    die;
  }

  // PDO Fetch Row
  $row = $sqlStatement->fetch(PDO::FETCH_ASSOC);
  // PDO Kill DB Connection
  $pdoConn = $sqlStatement = null;
  // if no results, return false
  if (count($row) <= 0) {
    return false;
  }
  // Loop through columns to fix dates
  $queryData; // Will hold the updated data
  foreach ($row as $key => $column) {
    // If any of the dates are 1900-01-01 00:00:00 the set to null
    if (preg_match("/^1900-01-01/", $column)) {
      $column = "";
    }
    $matches;
    preg_match("/^20\d\d-\d{1,2}-\d{1,2}/", $column, $matches);
    $column = isset($matches[0]) ? $matches[0] : $column;
    $queryData[strtolower($key)] = $column;
  }
  // Return
  return $queryData;
}

function GetSingleDetailHist($id)
{
  // Connect the the database
  try {
    $pdoConn = new PDO($GLOBALS['PDO_CONNECT_STMT'], $GLOBALS['PDO_USER'], $GLOBALS['PDO_PASSWORD']);
    $pdoConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  } catch (PDOException $e) {
    echo("<pre>");
    echo "Message:\n", $e->getMessage(), "\n\n";
    echo "File/Line:\n", $e->getFile(), ", ", $e->getLine(), "\n\n";
    echo "Trace:\n", $e->getTraceAsString(), "\n";
    echo("</pre>");
    $pdoConn = $sqlStatement = null;
    die;
  }
  // PDO Statement
  $sqlStatement = $pdoConn->prepare(
  "SELECT TOP 1
      dbo.CCA_REQUISITION_DETAIL_HIST.HIST_ID, dbo.CCA_REQUISITION_DETAIL_HIST.HIST_DATEADDED,
      dbo.CCA_REQUISITION_DETAIL_HIST.RL_ID, dbo.CCA_REQUISITION_DETAIL_HIST.RL_RHID,
      dbo.CCA_REQUISITION_DETAIL_HIST.RL_DATEADDED, dbo.CCA_REQUISITION_DETAIL_HIST.RL_PARTNUMBER,
      dbo.CCA_REQUISITION_DETAIL_HIST.RL_ITEMNAME, dbo.CCA_REQUISITION_DETAIL_HIST.RL_ITEMDESC,
      dbo.CCA_REQUISITION_DETAIL_HIST.RL_ITEMLINK, dbo.CCA_REQUISITION_DETAIL_HIST.RL_UNITS,
      dbo.CCA_REQUISITION_DETAIL_HIST.RL_QUANTITY, dbo.CCA_REQUISITION_DETAIL_HIST.RL_UNITPRICE,
      dbo.CCA_REQUISITION_DETAIL_HIST.RL_EXTENDED, dbo.CCA_REQUISITION_DETAIL_HIST.RL_NOTES,
      dbo.CCA_REQUISITION_DETAIL_HIST.RL_DATERECEIVED, dbo.CCA_REQUISITION_DETAIL_HIST.RL_QTYRECEIVED,
      dbo.CCA_REQUISITION_DETAIL_HIST.RL_ITEMSTATUS, dbo.CCA_REQUISITION_DETAIL_HIST.HIST_ACTION
    FROM
      dbo.CCA_REQUISITION_DETAIL_HIST
    WHERE
      dbo.CCA_REQUISITION_DETAIL_HIST.HIST_ID = :id "
  );
  // PDO Parameters
  $paramsSql[':id'] = $id;
  // PDO Fetch Results
  $row = $sqlStatement->fetch(PDO::FETCH_ASSOC);
  // Kill Connection
  $pdoConn = $sqlStatement = null;
  // If no results, return false
  if (count($row) <= 0) {
    return false;
  }
  // Loop through columns and fix dates
  $queryData; // holds fixed data
  foreach ($row as $key => $column) {
    // If any of the dates are 1900-01-01 00:00:00 the set to null
    if (preg_match("/^1900-01-01/", $column)) {
      $column = "";
    }
    $matches;
    preg_match("/^20\d\d-\d{1,2}-\d{1,2}/", $column, $matches);
    $column = isset($matches[0]) ? $matches[0] : $column;
    $queryData[strtolower($key)] = $column;
  }
  // Return $queryData
  return $queryData;
}

/**
 * Pass in a username and get the department that user belongs to.
 * @param string - user name
 * @return string - department name
 */
function GetUserDept($userName)
{
  // Db vars
  $myHost =       "192.168.123.24";
  $myUser =       "jeffrabin";
  $myPassword =   "j3ffsDB";
  $myDb =         "cpdb";
  // Connect
  $mysqli =       new mysqli($myHost, $myUser, $myPassword, $myDb);
  // Check for error
  if ($mysqli->connect_errno) {
    return false;
  }
  // Setup SQL query
  $sql = "SELECT `Directory`.CUserName AS `user`, `Directory`.CDept AS dept
      FROM `Directory`
      WHERE CUserName = '{{user}}'
      LIMIT 1
    ";
  $sql = str_replace('{{user}}', mysqli_escape_string($mysqli, $userName), $sql);
  // Fetch results
  if ($result = $mysqli->query($sql)) {
    $data = $result->fetch_assoc();
    return $data['dept'];
  } else {
    return "unknown";
  }
}

/**
 * Returns an array of all available department names.
 * @return array[]:string - department names
 */
function GetDepartments()
{
  // Db vars
  $myHost     = "192.168.123.24";
  $myUser     = "jeffrabin";
  $myPassword = "j3ffsDB";
  $myDb       = "cpdb";
  // Connect
  $mysqli     = new mysqli($myHost, $myUser, $myPassword, $myDb);
  // Check for error
  if ($mysqli->connect_errno) {
    return false;
  }
  // Setup SQL query
  $sql = "SELECT DISTINCT `Directory`.CDept AS dept FROM `Directory` ORDER BY CDept";
  // Fetch results
  $result = $mysqli->query($sql);
  $departments = array();
  if ($result->num_rows) {
    while ($row = $result->fetch_assoc()) {
      $departments[] = $row['dept'];
    }
  }
  return $departments;
}

/**
 * Pass in a department name and return an array of usernames which belong to
 * that department.
 * @param string $department
 * @return array[string]
 */
function GetUsersInDept($department)
{
  // Db vars
  $myHost     = "192.168.123.24";
  $myUser     = "jeffrabin";
  $myPassword = "j3ffsDB";
  $myDb       = "cpdb";
  // Connect
  $mysqli     = new mysqli($myHost, $myUser, $myPassword, $myDb);
  // Check for error
  if ($mysqli->connect_errno) {
    return false;
  }
  // Setup SQL query
  $department = mysqli_real_escape_string($mysqli, $department);
  $sql = "SELECT `Directory`.CUserName AS `user`
    FROM `Directory`
    WHERE `Directory`.CDept = '{{dept}}'
    ";
  $sql = str_replace('{{dept}}', $department, $sql);
  // Fetch results
  $result = $mysqli->query($sql);
  $users = array();
  if ($result->num_rows) {
    while ($row = $result->fetch_assoc()) {
      $users[] = $row['user'];
    }
  }
  mysqli_close($mysqli);
  return $users;
}

/******************************************************************************
 *  ADD FUNCTIONS  ************************************************************
 ******************************************************************************/

/**
 * Add a full requisition to the database
 * @param array[string]:array[string]:string - $data[header|details][columnname] column data
 * @return int|false - id of the inserted header record or false on fail
 */
function AddEntry($data)
{
  // Get user name from cookie else set to unknown user
  $jUserName = "Unknown User";
  if (isset($_COOKIE['userName'])) {
    $jUserName = $_COOKIE['userName'];
  } else {
    $jUserName = $_SESSION['currentuser'];
  }
  // Calculate grand total from details to be entered into header
  $grandTotal = 0;
  foreach ($data['details'] as $key => $value) {
    $grandTotal += is_numeric($value['rl-extended']) ? $value['rl-extended'] : 0 ;
  }
  if (is_numeric($data['header']['rh-shippingamount'])) {
    $grandTotal += $data['header']['rh-shippingamount'];
  }
  if (is_numeric($data['header']['rh-tax'])) {
    $grandTotal += $data['header']['rh-tax'];
  }
  $grandTotal = round($grandTotal, 2);
  // Create an ID based on UNIX timestamp to act as a key to later use to pull the actual ID from header
  $phpId = time();
  // Get the current formatted date to be used in date created fields for example
  $jCurrentDate = GetFormattedDate();
  // Connect to db
  try {
    $pdoConn = new PDO($GLOBALS['PDO_CONNECT_STMT'], $GLOBALS['PDO_USER'], $GLOBALS['PDO_PASSWORD']);
    $pdoConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  } catch (PDOException $e) {
    printExceptionAndDie($e);
  }
  // Section: SQL insert query to insert header data
  $sqlHeaderInsert = $pdoConn->prepare(
  "INSERT INTO dbo.CCA_REQUISITION_HEADER (
        dbo.CCA_REQUISITION_HEADER.RH_DATEREQUESTED, dbo.CCA_REQUISITION_HEADER.RH_REQUESTEDFOR,
        dbo.CCA_REQUISITION_HEADER.RH_REQUSER, dbo.CCA_REQUISITION_HEADER.RH_REQDEPT,
        dbo.CCA_REQUISITION_HEADER.RH_SUPPLIERNAME, dbo.CCA_REQUISITION_HEADER.RH_SUPPLIERADDRESS,
        dbo.CCA_REQUISITION_HEADER.RH_SUPPLIERPHONE, dbo.CCA_REQUISITION_HEADER.RH_SUPPLIEREMAIL,
        dbo.CCA_REQUISITION_HEADER.RH_SUPPLIERCONTACT, dbo.CCA_REQUISITION_HEADER.RH_SHIPTOADDR,
        dbo.CCA_REQUISITION_HEADER.RH_DEPTAPPROVAL, dbo.CCA_REQUISITION_HEADER.RH_DEPTAPPROVALDATE,
        dbo.CCA_REQUISITION_HEADER.RH_DEPTAPPROVALUSER, dbo.CCA_REQUISITION_HEADER.RH_MGTAPPROVAL,
        dbo.CCA_REQUISITION_HEADER.RH_MGTAPPROVALDATE, dbo.CCA_REQUISITION_HEADER.RH_MGTAPPROVALUSER,
        dbo.CCA_REQUISITION_HEADER.RH_REQTOTAL, dbo.CCA_REQUISITION_HEADER.RH_LASTUPDATED,
        dbo.CCA_REQUISITION_HEADER.RH_LASTUSER, dbo.CCA_REQUISITION_HEADER.RH_NOTES,
        dbo.CCA_REQUISITION_HEADER.RH_JUSTIFICATION, dbo.CCA_REQUISITION_HEADER.RH_STATUS,
        dbo.CCA_REQUISITION_HEADER.PHP_ID, dbo.CCA_REQUISITION_HEADER.RH_PURCHASER,
        dbo.CCA_REQUISITION_HEADER.RH_SHIPPINGAMOUNT, dbo.CCA_REQUISITION_HEADER.RH_SHIPPINGMETHOD,
        dbo.CCA_REQUISITION_HEADER.RH_ORDERED_DATETIME, dbo.CCA_REQUISITION_HEADER.RH_PAYMENTTYPE,
        dbo.CCA_REQUISITION_HEADER.RH_PAYEE, dbo.CCA_REQUISITION_HEADER.RH_TAX
      )
      VALUES
      (
        :RH_DATEREQUESTED,      :RH_REQUESTEDFOR,       :RH_REQUSER,          :RH_REQDEPT,
        :RH_SUPPLIERNAME,       :RH_SUPPLIERADDRESS,    :RH_SUPPLIERPHONE,    :RH_SUPPLIEREMAIL,
        :RH_SUPPLIERCONTACT,    :RH_SHIPTOADDR,         :RH_DEPTAPPROVAL,     :RH_DEPTAPPROVALDATE,
        :RH_DEPTAPPROVALUSER,   :RH_MGTAPPROVAL,        :RH_MGTAPPROVALDATE,  :RH_MGTAPPROVALUSER,
        :RH_REQTOTAL,           :RH_LASTUPDATED,        :RH_LASTUSER,         :RH_NOTES,
        :RH_JUSTIFICATION,      :RH_STATUS,             :PHP_ID,              :RH_PURCHASER,
        :RH_SHIPPINGAMOUNT,     :RH_SHIPPINGMETHOD,     :RH_ORDERED_DATETIME, :RH_PAYMENTTYPE,
        :RH_PAYEE,              :RH_TAX
      )"
  );
  $paramsHeaderInsert[':RH_DATEREQUESTED']    = $data['header']['rh-daterequested'];
  $paramsHeaderInsert[':RH_REQUESTEDFOR']     = $data['header']['rh-requestedfor'];
  $paramsHeaderInsert[':RH_REQUSER']          = $jUserName;
  $paramsHeaderInsert[':RH_REQDEPT']          = $data['header']['rh-reqdept'];
  $paramsHeaderInsert[':RH_SUPPLIERNAME']     = $data['header']['rh-suppliername'];
  $paramsHeaderInsert[':RH_SUPPLIERADDRESS']  = $data['header']['rh-supplieraddress'];
  $paramsHeaderInsert[':RH_SUPPLIERPHONE']    = $data['header']['rh-supplierphone'];
  $paramsHeaderInsert[':RH_SUPPLIEREMAIL']    = $data['header']['rh-supplieremail'];
  $paramsHeaderInsert[':RH_SUPPLIERCONTACT']  = $data['header']['rh-suppliercontact'];
  $paramsHeaderInsert[':RH_SHIPTOADDR']       = $data['header']['rh-shiptoaddr'];
  $paramsHeaderInsert[':RH_DEPTAPPROVAL']     = $data['header']['rh-deptapproval'];
  $paramsHeaderInsert[':RH_DEPTAPPROVALDATE'] = $data['header']['rh-deptapproval'] == "Y" ? $jCurrentDate : "";
  $paramsHeaderInsert[':RH_DEPTAPPROVALUSER'] = $data['header']['rh-deptapproval'] == "Y" ? $jUserName : "";
  $paramsHeaderInsert[':RH_MGTAPPROVAL']      = $data['header']['rh-mgtapproval'];
  $paramsHeaderInsert[':RH_MGTAPPROVALDATE']  = $data['header']['rh-mgtapproval'] == "Y" ? $jCurrentDate : "";
  $paramsHeaderInsert[':RH_MGTAPPROVALUSER']  = $data['header']['rh-mgtapproval'] == "Y" ? $jUserName : "";
  $paramsHeaderInsert[':RH_REQTOTAL']         = $grandTotal;
  // echo "grandTotal ($grandTotal) is " . gettype($grandTotal) . "<br>";
  $paramsHeaderInsert[':RH_LASTUPDATED']      = $jCurrentDate;
  $paramsHeaderInsert[':RH_LASTUSER']         = $jUserName;
  $paramsHeaderInsert[':RH_NOTES']            = $data['header']['rh-notes'];
  $paramsHeaderInsert[':RH_JUSTIFICATION']    = $data['header']['rh-justification'];
  $paramsHeaderInsert[':RH_STATUS']           = $data['header']['rh-status'];
  $paramsHeaderInsert[':RH_PURCHASER']        = $data['header']['rh-purchaser'];

  $data['header']['rh-shippingamount'] = is_numeric($data['header']['rh-shippingamount']) ? $data['header']['rh-shippingamount'] : 0.00;
  $paramsHeaderInsert[':RH_SHIPPINGAMOUNT']   = $data['header']['rh-shippingamount'];

  // echo "data['header']['rh-shippingamount'] (" . $data['header']['rh-shippingamount'] . ") is " . gettype($data['header']['rh-shippingamount']) . "<br>";
  $paramsHeaderInsert[':RH_SHIPPINGMETHOD']   = $data['header']['rh-shippingmethod'];

  // $paramsHeaderInsert[':RH_STATUS'] must be set before the following line
  $paramsHeaderInsert[':RH_ORDERED_DATETIME'] = strtolower($paramsHeaderInsert[':RH_STATUS']) === 'ordered'  ? $jCurrentDate : "";
  $paramsHeaderInsert[':RH_PAYMENTTYPE']      = isset($data['header']['rh-paymenttype']) ? $data['header']['rh-paymenttype'] : "";
  $paramsHeaderInsert[':RH_PAYEE']            = isset($data['header']['rh-payee']) ? $data['header']['rh-payee'] : "";
  $paramsHeaderInsert[':RH_TAX']              = isset($data['header']['rh-tax']) && is_numeric($data['header']['rh-tax']) ? $data['header']['rh-tax'] : 0.00;

  $paramsHeaderInsert[':PHP_ID']              = $phpId;
  // PDO Execute Statement
  try {
    $sqlHeaderInsert->execute($paramsHeaderInsert);
  } catch (PDOException $e) {
    printExceptionAndDie($e);
  }
  // END SQL insert query to insert header data
  // Section: Get header ID (rh_id) of last inserted header row

  // PDO Statement
  $sql = "SELECT
        TOP 1 dbo.CCA_REQUISITION_HEADER.RH_ID as id
        FROM dbo.CCA_REQUISITION_HEADER
        WHERE dbo.CCA_REQUISITION_HEADER.PHP_ID = :id ";
  $sqlSelectLastId = $pdoConn->prepare($sql);

  // PDO Parameters
  $paramsSelectLastId[':id'] = $phpId;

  // PDO Execute Statement
  try {
    $sqlSelectLastId->execute($paramsSelectLastId);
  } catch (PDOException $e) {
    printExceptionAndDie($e);
  }

  // PDO Fetch Row
  $result = $sqlSelectLastId->fetch(PDO::FETCH_ASSOC);
  $lastInsertedId = $result['id'];

  // END Get header ID (rh_id) of last inserted header row
  // Section: SQL insert query to insert detail data
  $sqlDetailInsert = $pdoConn->prepare(
  "INSERT INTO dbo.CCA_REQUISITION_DETAIL (
        dbo.CCA_REQUISITION_DETAIL.RL_RHID, dbo.CCA_REQUISITION_DETAIL.RL_PARTNUMBER,
        dbo.CCA_REQUISITION_DETAIL.RL_ITEMNAME, dbo.CCA_REQUISITION_DETAIL.RL_ITEMDESC,
        dbo.CCA_REQUISITION_DETAIL.RL_ITEMLINK, dbo.CCA_REQUISITION_DETAIL.RL_UNITS,
        dbo.CCA_REQUISITION_DETAIL.RL_QUANTITY, dbo.CCA_REQUISITION_DETAIL.RL_UNITPRICE,
        dbo.CCA_REQUISITION_DETAIL.RL_EXTENDED, dbo.CCA_REQUISITION_DETAIL.RL_NOTES,
        dbo.CCA_REQUISITION_DETAIL.RL_DATERECEIVED, dbo.CCA_REQUISITION_DETAIL.RL_QTYRECEIVED,
        dbo.CCA_REQUISITION_DETAIL.RL_ITEMSTATUS
      )
      VALUES
      (
        :RL_RHID,      :RL_PARTNUMBER,    :RL_ITEMNAME,      :RL_ITEMDESC,
        :RL_ITEMLINK,  :RL_UNITS,         :RL_QUANTITY,      :RL_UNITPRICE,
        :RL_EXTENDED,  :RL_NOTES,         :RL_DATERECEIVED,  :RL_QTYRECEIVED,
        :RL_ITEMSTATUS
      ) "
  );
  // Execute insert query for each detail
  foreach ($data['details'] as $key => $detail) {
  // I'm using substr to simply make sure there is no SQL Overflow Error
    $paramsDetailInsert[':RL_RHID']         = $lastInsertedId;
    $paramsDetailInsert[':RL_PARTNUMBER']   = substr($detail['rl-partnumber'], 0, 255);
    $paramsDetailInsert[':RL_ITEMNAME']     = substr($detail['rl-itemname'], 0, 255);
    $paramsDetailInsert[':RL_ITEMDESC']     = substr($detail['rl-itemdesc'], 0, 255);
    $paramsDetailInsert[':RL_ITEMLINK']     = substr($detail['rl-itemlink'], 0, 255);
    $paramsDetailInsert[':RL_UNITS']        = substr($detail['rl-units'], 0, 50);
    $paramsDetailInsert[':RL_QUANTITY']     = $detail['rl-quantity'];
    $paramsDetailInsert[':RL_UNITPRICE']    = $detail['rl-unitprice'];
    $paramsDetailInsert[':RL_EXTENDED']     = $detail['rl-extended'];
    $paramsDetailInsert[':RL_NOTES']        = substr($detail['rl-notes'], 0, 2000);
    $paramsDetailInsert[':RL_DATERECEIVED'] = $detail['rl-datereceived'];
    $paramsDetailInsert[':RL_QTYRECEIVED']  = $detail['rl-qtyreceived'];

  //$paramsDetailInsert[':RL_ITEMSTATUS']   = substr($detail['rl-itemstatus'], 0, 50);
  // If header status set to 'Ordered', set item status' to 'Ordered'
    $paramsDetailInsert[':RL_ITEMSTATUS']   = strtolower($data['header']['rh-status']) === 'ordered'  ? 'Ordered' : substr($detail['rl-itemstatus'], 0, 50);

  // PDO Execute Statement
    try {
      $sqlDetailInsert->execute($paramsDetailInsert);
    } catch (PDOException $e) {
      printExceptionAndDie($e);
    }
  }
  // END Execute insert query for each detail
  // Make sure to kill db connection
  $pdoConn = $sqlStatement = null;

  /******************
    * Email Settings *
  ******************/

  // Email if department approval = y
  if (strtolower($data['header']['rh-deptapproval']) === 'y') {
    sendDeptApproved($lastInsertedId);

  // Email if total >= 100 and department approval = y
    if ($grandTotal >= 100) {
      sendForApproval($lastInsertedId);
      sendOver100($lastInsertedId);
    }
  }

  // Email if total >= 2500 and management approval = y
  if ($grandTotal >= 1200 && strtolower($data['header']['rh-mgtapproval']) === 'y') {
    sendOver1200($lastInsertedId);
  }

  // Email if purchaser is set and management approval = y
  if (strlen($data['header']['rh-purchaser']) >= 3 && strtolower($data['header']['rh-mgtapproval']) === 'y') {
    sendPurchase($lastInsertedId, $data['header']['rh-purchaser']);
  }

  // Email if status = closed
  if (strtolower($data['header']['rh-status']) == 'closed') {
    sendToAccountingOnClosed($lastInsertedId);
  }

  // return the id of the last inserted requisition
  return $lastInsertedId;
}

/**
 * Add a detail entry to an already existing requistion
 * @param array[string]:string - $data
 * @return bool - true on success
 */
function AddDetailEntry($data)
{
  // Check is username is set in a cookie
  $jUserName = "Unknown User";
  if (isset($_COOKIE['userName'])) {
    $jUserName = $_COOKIE['userName'];
  }
  // Get current date to be used in SQL
  $jCurrentDate = GetFormattedDate();
  // PDO Connect
  try {
    $pdoConn = new PDO($GLOBALS['PDO_CONNECT_STMT'], $GLOBALS['PDO_USER'], $GLOBALS['PDO_PASSWORD']);
    $pdoConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  } catch (PDOException $e) {
    echo("<pre>");
    echo "Message:\n", $e->getMessage(), "\n\n";
    echo "File/Line:\n", $e->getFile(), ", ", $e->getLine(), "\n\n";
    echo "Trace:\n", $e->getTraceAsString(), "\n";
    echo("</pre>");
    $pdoConn = $sqlStatement = null;
    die;
  }
  // PDO Statement
  $sqlStatement = $pdoConn->prepare(
  "INSERT INTO dbo.CCA_REQUISITION_DETAIL (
        dbo.CCA_REQUISITION_DETAIL.RL_RHID, dbo.CCA_REQUISITION_DETAIL.RL_PARTNUMBER,
        dbo.CCA_REQUISITION_DETAIL.RL_ITEMNAME, dbo.CCA_REQUISITION_DETAIL.RL_ITEMDESC,
        dbo.CCA_REQUISITION_DETAIL.RL_ITEMLINK, dbo.CCA_REQUISITION_DETAIL.RL_UNITS,
        dbo.CCA_REQUISITION_DETAIL.RL_QUANTITY, dbo.CCA_REQUISITION_DETAIL.RL_UNITPRICE,
        dbo.CCA_REQUISITION_DETAIL.RL_EXTENDED, dbo.CCA_REQUISITION_DETAIL.RL_NOTES,
        dbo.CCA_REQUISITION_DETAIL.RL_DATERECEIVED, dbo.CCA_REQUISITION_DETAIL.RL_QTYRECEIVED,
        dbo.CCA_REQUISITION_DETAIL.RL_ITEMSTATUS
      )
      VALUES
      (
        :RL_RHID, :RL_PARTNUMBER,
        :RL_ITEMNAME, :RL_ITEMDESC,
        :RL_ITEMLINK, :RL_UNITS,
        :RL_QUANTITY, :RL_UNITPRICE,
        :RL_EXTENDED, :RL_NOTES,
        :RL_DATERECEIVED, :RL_QTYRECEIVED,
        :RL_ITEMSTATUS
      )"
  );
  // PDO Parameters
  $paramsSql[':RL_RHID']          = $data['rl-rhid'];
  $paramsSql[':RL_PARTNUMBER']    = $data['rl-partnumber'];
  $paramsSql[':RL_ITEMNAME']      = $data['rl-itemname'];
  $paramsSql[':RL_ITEMDESC']      = $data['rl-itemdesc'];
  $paramsSql[':RL_ITEMLINK']      = $data['rl-itemlink'];
  $paramsSql[':RL_UNITS']         = $data['rl-units'];
  $paramsSql[':RL_QUANTITY']      = $data['rl-quantity'];
  $paramsSql[':RL_UNITPRICE']     = $data['rl-unitprice'];
  $paramsSql[':RL_EXTENDED']      = $data['rl-extended'];
  $paramsSql[':RL_NOTES']         = $data['rl-notes'];
  $paramsSql[':RL_DATERECEIVED']  = $data['rl-datereceived'];
  $paramsSql[':RL_QTYRECEIVED']   = $data['rl-qtyreceived'];
  $paramsSql[':RL_ITEMSTATUS']    = $data['rl-itemstatus'];

  // PDO Execute Statement
  try {
    $sqlStatement->execute($paramsSql);
  } catch (PDOException $e) {
    echo("<pre>");
    echo "Message:\n", $e->getMessage(), "\n\n";
    echo "File/Line:\n", $e->getFile(), ", ", $e->getLine(), "\n\n";
    echo "Trace:\n", $e->getTraceAsString(), "\n";
    echo("</pre>");
    $pdoConn = $sqlStatement = null;
    die;
  }

  // PDO Kill DB Connection
  $pdoConn = $sqlStatement = null;

  // Update header total
  UpdateHeaderTotal($data['rl-rhid'], $jUserName, $jCurrentDate);

  // Return True
  return true;
}

/******************************************************************************
 *  COPY FUNCTIONS  ***********************************************************
 ******************************************************************************/

function CopyEntry($id)
{
  // get header data
  $header = GetHeader($id);
  // get detail data
  $details = GetDetails($id);
  // rename header fields ex: rh_id -> rh-id
  foreach ($header as $name => $value) {
    if (strtolower($name) === 'rh_id' || strtolower($name) === 'rh_dateadded'
    || strtolower($name) === 'rh_requser' || strtolower($name) === 'rh_lastupdated'
    || strtolower($name) === 'rh_lastuser' || strtolower($name) === 'php_id'
    || strtolower($name) === 'rh_ordered_datetime') {
      continue;
    } elseif (strtolower($name) === 'rh_deptapproval' || strtolower($name) === 'rh_mgtapproval') {
      $value = 'N';
    } elseif (strtolower($name) === 'rh_deptapprovaldate' || strtolower($name) === 'rh_deptapprovaluser'
    || strtolower($name) === 'rh_mgtapprovaldate' || strtolower($name) === 'rh_mgtapprovaluser'
    || strtolower($name) === 'rh_purchaser') {
      $value = '';
    } elseif (strtolower($name) === 'rh_status') {
      $value = 'Requested';
    }
    // add $value
    $data['header'][str_replace('_', '-', $name)] = $value;
  }
  // rename detail fields
  foreach ($details as $idx => $row) {
    foreach ($row as $name => $value) {
      if (strtolower($name) === 'rl_id' || strtolower($name) === 'rl_rhid'
      || strtolower($name) === 'rl_dateadded') {
        continue;
      } elseif (strtolower($name) === 'rl_datereceived' || strtolower($name) === 'rl_qtyreceived') {
        $value = '';
      } elseif (strtolower($name) === 'rl_itemstatus') {
        $value = 'To Be Ordered';
      }
      // add $value
      $data['details'][$idx][str_replace('_', '-', $name)] = $value;
    }
  }
  // add data to db
  return AddEntry($data);
}

/******************************************************************************
 *  PUSH/UPDATED FUNCTIONS  ***************************************************
 ******************************************************************************/

function PushChangesHeader($values)
{
  // If ID is not set, we can't do anything so Return False
  if (!isset($values['rh-id']) || strlen($values['rh-id']) < 1) {
    return false;
  }
  // Set current date
  $currentDate = GetFormattedDate();
  // Set User Name
  $currentUser = isset($_COOKIE['userName']) ? $_COOKIE['userName'] : "Unknown User";
  // Get data before changes are saved so we can compare later
  $previousState = GetHeader($values['rh-id']);
  // PDO Connection
  try {
    $pdoConn = new PDO($GLOBALS['PDO_CONNECT_STMT'], $GLOBALS['PDO_USER'], $GLOBALS['PDO_PASSWORD']);
    $pdoConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  } catch (PDOException $e) {
    printExceptionAndDie($e);
  }
  // PDO Statement
  $sql = "UPDATE dbo.CCA_REQUISITION_HEADER
      SET
        dbo.CCA_REQUISITION_HEADER.RH_DATEREQUESTED     = :RH_DATEREQUESTED,
        dbo.CCA_REQUISITION_HEADER.RH_REQUESTEDFOR      = :RH_REQUESTEDFOR,
        dbo.CCA_REQUISITION_HEADER.RH_REQDEPT           = :RH_REQDEPT,
        dbo.CCA_REQUISITION_HEADER.RH_SUPPLIERNAME      = :RH_SUPPLIERNAME,
        dbo.CCA_REQUISITION_HEADER.RH_SUPPLIERADDRESS   = :RH_SUPPLIERADDRESS,
        dbo.CCA_REQUISITION_HEADER.RH_SUPPLIERPHONE     = :RH_SUPPLIERPHONE,
        dbo.CCA_REQUISITION_HEADER.RH_SUPPLIEREMAIL     = :RH_SUPPLIEREMAIL,
        dbo.CCA_REQUISITION_HEADER.RH_SUPPLIERCONTACT   = :RH_SUPPLIERCONTACT,
        dbo.CCA_REQUISITION_HEADER.RH_SHIPTOADDR        = :RH_SHIPTOADDR,
        dbo.CCA_REQUISITION_HEADER.RH_DEPTAPPROVAL      = :RH_DEPTAPPROVAL,
        dbo.CCA_REQUISITION_HEADER.RH_DEPTAPPROVALDATE  = :RH_DEPTAPPROVALDATE,
        dbo.CCA_REQUISITION_HEADER.RH_DEPTAPPROVALUSER  = :RH_DEPTAPPROVALUSER,
        dbo.CCA_REQUISITION_HEADER.RH_MGTAPPROVAL       = :RH_MGTAPPROVAL,
        dbo.CCA_REQUISITION_HEADER.RH_MGTAPPROVALDATE   = :RH_MGTAPPROVALDATE,
        dbo.CCA_REQUISITION_HEADER.RH_MGTAPPROVALUSER   = :RH_MGTAPPROVALUSER,
        dbo.CCA_REQUISITION_HEADER.RH_REQTOTAL          = :RH_REQTOTAL,
        dbo.CCA_REQUISITION_HEADER.RH_LASTUPDATED       = :RH_LASTUPDATED,
        dbo.CCA_REQUISITION_HEADER.RH_LASTUSER          = :RH_LASTUSER,
        dbo.CCA_REQUISITION_HEADER.RH_NOTES             = :RH_NOTES,
        dbo.CCA_REQUISITION_HEADER.RH_JUSTIFICATION     = :RH_JUSTIFICATION,
        dbo.CCA_REQUISITION_HEADER.RH_STATUS            = :RH_STATUS,
        dbo.CCA_REQUISITION_HEADER.RH_PURCHASER         = :RH_PURCHASER,
        dbo.CCA_REQUISITION_HEADER.RH_ORDERED_DATETIME  = :RH_ORDERED_DATETIME,
        dbo.CCA_REQUISITION_HEADER.RH_SHIPPINGAMOUNT    = :RH_SHIPPINGAMOUNT,
        dbo.CCA_REQUISITION_HEADER.RH_SHIPPINGMETHOD    = :RH_SHIPPINGMETHOD,
        dbo.CCA_REQUISITION_HEADER.RH_PAYMENTTYPE       = :RH_PAYMENTTYPE,
        dbo.CCA_REQUISITION_HEADER.RH_PAYEE             = :RH_PAYEE,
        dbo.CCA_REQUISITION_HEADER.RH_TAX               = :RH_TAX
      WHERE
        dbo.CCA_REQUISITION_HEADER.RH_ID = :id
    ";
  $sqlStatement = $pdoConn->prepare($sql);
  // PDO Parameters
  $paramsSql[':RH_DATEREQUESTED']     = isset($values['rh-daterequested']) ? $values['rh-daterequested'] : "";
  $paramsSql[':RH_REQUESTEDFOR']      = isset($values['rh-requestedfor']) ? $values['rh-requestedfor'] : "";
  $paramsSql[':RH_REQDEPT']           = isset($values['rh-reqdept']) ? $values['rh-reqdept'] : "";
  $paramsSql[':RH_SUPPLIERNAME']      = isset($values['rh-suppliername']) ? $values['rh-suppliername'] : "";
  $paramsSql[':RH_SUPPLIERADDRESS']   = isset($values['rh-supplieraddress']) ? $values['rh-supplieraddress'] : "";
  $paramsSql[':RH_SUPPLIERPHONE']     = isset($values['rh-supplierphone']) ? $values['rh-supplierphone'] : "";
  $paramsSql[':RH_SUPPLIEREMAIL']     = isset($values['rh-supplieremail']) ? $values['rh-supplieremail'] : "";
  $paramsSql[':RH_SUPPLIERCONTACT']   = isset($values['rh-suppliercontact']) ? $values['rh-suppliercontact'] : "";
  $paramsSql[':RH_SHIPTOADDR']        = isset($values['rh-shiptoaddr']) ? $values['rh-shiptoaddr'] : "";

  // management approval
  $paramsSql[':RH_MGTAPPROVAL']       = isset($values['rh-mgtapproval']) ? $values['rh-mgtapproval'] : "";
  $paramsSql[':RH_MGTAPPROVALDATE']   = isset($values['rh-mgtapproval']) && $values['rh-mgtapproval'] == "Y" ? ($values['rh-mgtapproval'] != $values['rh-mgtapproval-was'] ? $currentDate : $values['rh-mgtapprovaldate']) : "";
  $paramsSql[':RH_MGTAPPROVALUSER']   = isset($values['rh-mgtapproval']) && $values['rh-mgtapproval'] == "Y" ? ($values['rh-mgtapproval'] != $values['rh-mgtapproval-was'] ? $currentUser : $values['rh-mgtapprovaluser']) : "";

  // department approval
  $paramsSql[':RH_DEPTAPPROVAL']      = strtolower($paramsSql[':RH_MGTAPPROVAL']) == 'y' ? "Y" : $values['rh-deptapproval'];
  $paramsSql[':RH_DEPTAPPROVALDATE']  = $paramsSql[':RH_DEPTAPPROVAL'] == "Y" ? ($paramsSql[':RH_DEPTAPPROVAL'] != $values['rh-deptapproval-was'] ? $currentDate : $values['rh-deptapprovaldate']) : "";
  $paramsSql[':RH_DEPTAPPROVALUSER']  = $paramsSql[':RH_DEPTAPPROVAL'] == "Y" ? ($paramsSql[':RH_DEPTAPPROVAL'] != $values['rh-deptapproval-was'] ? $currentUser : $values['rh-deptapprovaluser']) : "";

  $paramsSql[':RH_REQTOTAL']          = isset($values['rh-reqtotal']) && is_numeric($values['rh-reqtotal']) ? $values['rh-reqtotal'] : 0.00;
  $paramsSql[':RH_LASTUPDATED']       = $currentDate;
  $paramsSql[':RH_LASTUSER']          = $currentUser;
  $paramsSql[':RH_NOTES']             = isset($values['rh-notes']) ? $values['rh-notes'] : "";
  $paramsSql[':RH_JUSTIFICATION']     = isset($values['rh-justification']) ? $values['rh-justification'] : "";
  $paramsSql[':RH_PURCHASER']         = isset($values['rh-purchaser']) ? trim($values['rh-purchaser']) : "";

  // status
  // array of status
  $statusCodes = GetSettings('status');
  // array key of submitted status
  $currentStatusKey = array_search($values['rh-status'], $statusCodes);
  // if key found and key >= 4(waiting to be ordered) then keep submitted value
  if ($currentStatusKey !== false && $currentStatusKey >= 4) {
    $paramsSql[':RH_STATUS'] = $values['rh-status'];
  } elseif (strtolower($paramsSql[':RH_DEPTAPPROVAL']) == 'y' && strtolower($paramsSql[':RH_MGTAPPROVAL']) == 'n' && $values['rh-deptapproval'] != $values['rh-deptapproval-was']) {
    $paramsSql[':RH_STATUS'] = $statusCodes[1]; // Waiting Mgmt Approval
  } elseif (strtolower($paramsSql[':RH_MGTAPPROVAL']) == 'y' && $paramsSql[':RH_PURCHASER'] == '' && $paramsSql[':RH_MGTAPPROVAL'] != $values['rh-mgtapproval-was']) {
    $paramsSql[':RH_STATUS'] = $statusCodes[3]; // Waiting Select Purchaser
  } elseif (strlen($paramsSql[':RH_PURCHASER']) > 2 && $values['rh-purchaser'] != $values['rh-purchaser-was']) {
    $paramsSql[':RH_STATUS'] = $statusCodes[4]; // Waiting To Be Ordered
  } else {
    $paramsSql[':RH_STATUS'] = $values['rh-status'];
  }

  if (isset($values['rh-ordered-datetime']) && preg_match('/^20\d\d-\d{1,2}-\d{1,2} \d\d:\d\d/', $values['rh-ordered-datetime'])) {
    $paramsSql[':RH_ORDERED_DATETIME'] = $values['rh-ordered-datetime'];
  } else {
    // $paramsSQL[':RH_STATUS'] must be set before the following line
    $paramsSql[':RH_ORDERED_DATETIME'] = strtolower($paramsSql[':RH_STATUS']) === 'ordered'  ? $currentDate : "";
  }

  $paramsSql[':RH_SHIPPINGAMOUNT']    = isset($values['rh-shippingamount']) && is_numeric($values['rh-shippingamount']) ? $values['rh-shippingamount'] : 0.00;
  $paramsSql[':RH_SHIPPINGMETHOD']    = isset($values['rh-shippingmethod']) ? $values['rh-shippingmethod'] : "";
  $paramsSql[':RH_PAYMENTTYPE']       = isset($values['rh-paymenttype']) ? $values['rh-paymenttype'] : "";
  $paramsSql[':RH_PAYEE']             = isset($values['rh-payee']) ? $values['rh-payee'] : "";
  $paramsSql[':RH_TAX']               = isset($values['rh-tax']) && is_numeric($values['rh-tax']) ? $values['rh-tax'] : 0.00;

  $paramsSql[':id']                   = $values['rh-id'];
  // PDO Execute Statement
  try {
    $sqlStatement->execute($paramsSql);
  } catch (PDOException $e) {
    printExceptionAndDie($e);
  }
  // Kill db connection
  $pdoConn = $sqlStatement = null;
  // If status = ordered, update all items to ordered
  if ((isset($values['rh-status']) && $values['rh-status'] === "Ordered" ) && isset($values['rh-id'])) {
    // params header-id, status
    setItemStatusForId($values['rh-id'], $values['rh-status']);
  }
  // Update the header total, sum all items and shipping amount
  $newTotal = UpdateHeaderTotal($values['rh-id'], $currentUser, $currentDate);

  /****************************************************************************
  * Send emails if needed *****************************************************
  *****************************************************************************/

  // if dept approved = y but wasn't before send dept approved mail
  if (strtolower($values['rh-deptapproval']) == 'y' && strtolower($values['rh-deptapproval-was']) != 'y') {
    sendDeptApproved($values['rh-id']);
    if ($newTotal >= 100) {
      // Send to lisa for approval if over 100
      sendForApproval($values['rh-id']);
      // Send to marty if over 100
      sendOver100($values['rh-id']);
    }
  }

  // if dept approved = y and previous dept approved != y send dept approved and over100 mail
  // if ($values['rh-deptapproval'] == "Y" && $newTotal >= 100 &&
  // ($previousState['rh_deptapproval'] != "Y" || $previousState['rh_reqtotal'] < 100)) {
  //   sendForApproval($values['rh-id']);
  //   sendOver100($values['rh-id']);
  // }

  // if mgmt approved and total >= 1200 and previous is different send over 1200 mail
  if (strtolower($paramsSql[':RH_MGTAPPROVAL']) == 'y' && strtolower($paramsSql[':RH_MGTAPPROVAL']) != 'y' && $newTotal >= 1200) {
    sendOver1200($values['rh-id']);
  }
  // if ($paramsSql[':RH_MGTAPPROVAL'] == "Y" && $newTotal >= 1200 &&
  // ($previousState['rh_mgtapproval'] != "Y" || $previousState['rh_reqtotal'] < 1200)) {
  //   sendOver1200($values['rh-id']);
  // }

  // if purchaser selected but wasn't before send purchase mail
  if (strlen($values['rh-purchaser']) > 2 && strtolower($values['rh-purchaser']) != strtolower($values['rh-purchaser-was'])) {
    sendPurchase($values['rh-id'], $values['rh-purchaser']);
  }
  // if (strlen($values['rh-purchaser']) > 2
  //   && (
  //     (($newTotal < 100 && $values['rh-deptapproval'] == "Y")
  //       &&
  //       ($previousState['rh_reqtotal'] >= 100 || $previousState['rh_deptapproval'] != $values['rh-deptapproval']))
  //     ||
  //     (($newTotal >= 100 && $paramsSql[':RH_MGTAPPROVAL'] == "Y")
  //       &&
  //       ($previousState['rh_reqtotal'] < 100 || $previousState['rh_mgtapproval'] != $paramsSql[':RH_MGTAPPROVAL']))
  //   )
  // ) {
  //   sendPurchase($values['rh-id'], $values['rh-purchaser']);
  // }

  // Email if status = closed
  if (strtolower($values['rh-status']) == 'closed') {
    sendToAccountingOnClosed($values['rh-id']);
  }

  // Return True
  return true;
}

function PushChangesDetail($values)
{
  // If ID doesn't exist, we can't do anything
  if (!isset($values['rl-id']) || strlen($values['rl-id']) < 1) {
    return false;
  }
  // Set Current Date
  $currentDate = GetFormattedDate();
  // Set User Name
  $currentUser = isset($_COOKIE['userName']) ? $_COOKIE['userName'] : "Unknown User";
  // Connect To DB
  try {
    $pdoConn = new PDO($GLOBALS['PDO_CONNECT_STMT'], $GLOBALS['PDO_USER'], $GLOBALS['PDO_PASSWORD']);
    $pdoConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  } catch (PDOException $e) {
    printExceptionAndDie($e);
    // echo("<pre>");
    // echo "Message:\n", $e->getMessage(), "\n\n";
    // echo "File/Line:\n", $e->getFile(), ", ", $e->getLine(), "\n\n";
    // echo "Trace:\n", $e->getTraceAsString(), "\n";
    // echo("</pre>");
    // $pdoConn = $sqlStatement = null;
    // die;
  }
  // PDO Statement
  $sqlStatement = $pdoConn->prepare("
      UPDATE
        dbo.CCA_REQUISITION_DETAIL
      SET
        dbo.CCA_REQUISITION_DETAIL.RL_PARTNUMBER    = :RL_PARTNUMBER,
        dbo.CCA_REQUISITION_DETAIL.RL_ITEMNAME      = :RL_ITEMNAME,
        dbo.CCA_REQUISITION_DETAIL.RL_ITEMDESC      = :RL_ITEMDESC,
        dbo.CCA_REQUISITION_DETAIL.RL_ITEMLINK      = :RL_ITEMLINK,
        dbo.CCA_REQUISITION_DETAIL.RL_UNITS         = :RL_UNITS,
        dbo.CCA_REQUISITION_DETAIL.RL_QUANTITY      = :RL_QUANTITY,
        dbo.CCA_REQUISITION_DETAIL.RL_UNITPRICE     = :RL_UNITPRICE,
        dbo.CCA_REQUISITION_DETAIL.RL_EXTENDED      = :RL_EXTENDED,
        dbo.CCA_REQUISITION_DETAIL.RL_NOTES         = :RL_NOTES,
        dbo.CCA_REQUISITION_DETAIL.RL_DATERECEIVED  = :RL_DATERECEIVED,
        dbo.CCA_REQUISITION_DETAIL.RL_QTYRECEIVED   = :RL_QTYRECEIVED,
        dbo.CCA_REQUISITION_DETAIL.RL_ITEMSTATUS    = :RL_ITEMSTATUS
      WHERE
        dbo.CCA_REQUISITION_DETAIL.rl_id = :ID
      ");
  // PDO Parameters
  $paramsSql = null;
  $paramsSql[':RL_PARTNUMBER']    = isset($values['rl-partnumber'])   ? $values['rl-partnumber']  : "";
  $paramsSql[':RL_ITEMNAME']      = isset($values['rl-itemname'])     ? $values['rl-itemname']    : "";
  $paramsSql[':RL_ITEMDESC']      = isset($values['rl-itemdesc'])     ? $values['rl-itemdesc']    : "";
  $paramsSql[':RL_ITEMLINK']      = isset($values['rl-itemlink'])     ? $values['rl-itemlink']    : "";
  $paramsSql[':RL_UNITS']         = isset($values['rl-units'])        ? $values['rl-units']       : "";
  $paramsSql[':RL_QUANTITY']      = isset($values['rl-quantity'])     ? $values['rl-quantity'] : "";
  $paramsSql[':RL_UNITPRICE']     = isset($values['rl-unitprice'])    ? $values['rl-unitprice']   : "";
  $paramsSql[':RL_EXTENDED']      = isset($values['rl-extended'])     ? $values['rl-extended']    : "";
  $paramsSql[':RL_NOTES']         = isset($values['rl-notes'])        ? $values['rl-notes']       : "";
  $paramsSql[':RL_DATERECEIVED']  = isset($values['rl-datereceived']) ? $values['rl-datereceived']: "";
  $paramsSql[':RL_QTYRECEIVED']   = isset($values['rl-qtyreceived'])  ? $values['rl-qtyreceived'] : "";
  $paramsSql[':RL_ITEMSTATUS']    = isset($values['rl-itemstatus'])   ? $values['rl-itemstatus']  : "";
  $paramsSql[':ID']               = isset($values['rl-id'])           ? $values['rl-id']     : "";
  // PDO Execute Statement
  try {
    $sqlStatement->execute($paramsSql);
  } catch (PDOException $e) {
    printExceptionAndDie($e);
  // echo("<pre>");
  // echo "Message:\n", $e->getMessage(), "\n\n";
  // echo "File/Line:\n", $e->getFile(), ", ", $e->getLine(), "\n\n";
  // echo "Trace:\n", $e->getTraceAsString(), "\n";
  // print_r($values);
  // echo("</pre>");
  // $pdoConn = $sqlStatement = null;
  // die;
  }
  // Kill Connection To DB
  $pdoConn = $sqlStatement = null;
  // Update Total in Header
  if (!UpdateHeaderTotal($values['rl-rhid'], $currentUser, $currentDate)) {
    return false;
  }
  // Return
  return true;
}

/******************************************************************************
 *  HELPER FUNCTIONS  *********************************************************
 ******************************************************************************/

function printExceptionAndDie($e)
{
  echo("<pre>");
  echo "Message:\n", $e->getMessage(), "\n\n";
  echo "File/Line:\n", $e->getFile(), ", ", $e->getLine(), "\n\n";
  echo "Trace:\n", $e->getTraceAsString(), "\n";
  echo("</pre>");
  $pdoConn = $sqlStatement = null;
  //die;
}



function setItemStatusForId($headerId, $status)
{
  // Connect To DB
  try {
    $pdoConn = new PDO($GLOBALS['PDO_CONNECT_STMT'], $GLOBALS['PDO_USER'], $GLOBALS['PDO_PASSWORD']);
    $pdoConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  } catch (PDOException $e) {
    echo("<pre>");
    echo "Error connecting\n\n";
    echo "Message:\n", $e->getMessage(), "\n\n";
    echo "File/Line:\n", $e->getFile(), ", ", $e->getLine(), "\n\n";
    echo "Trace:\n", $e->getTraceAsString(), "\n";
    echo("</pre>");
    $pdoConn = $sqlStatement = null;
    die;
  }
  // PDO Statement
  $sqlStatement = $pdoConn->prepare("
      UPDATE dbo.CCA_REQUISITION_DETAIL
      SET dbo.CCA_REQUISITION_DETAIL.RL_ITEMSTATUS = :status
      WHERE
      dbo.CCA_REQUISITION_DETAIL.RL_RHID = :id
    ");
  // PDO Parameters
  $paramsSql = null;
  $paramsSql[':status'] = isset($status)    ? $status   : "";
  $paramsSql[':id']     = isset($headerId)  ? $headerId : "";
  // PDO Execute Statement
  try {
    $sqlStatement->execute($paramsSql);
  } catch (PDOException $e) {
    echo("<pre>");
    echo("Error Executing Statement");
    echo "Message:\n", $e->getMessage(), "\n\n";
    echo "File/Line:\n", $e->getFile(), ", ", $e->getLine(), "\n\n";
    echo "Trace:\n", $e->getTraceAsString(), "\n\n";

    echo "\$status:\n", $e->getTraceAsString(), "\n";
    echo "\$id:\n", $e->getTraceAsString(), "\n";

    echo("</>");
    $pdoConn = $sqlStatement = null;
    die;
  }
  // Kill Connection To DB
  $pdoConn = $sqlStatement = null;
  // Return
  return true;
}



/** Set the status in the header of an entry.
 *
 * @param string $headerId
 * @param string $status
 * @return bool true on success
 */
function setHeaderStatus ($headerId, $status) {
  // Connect To DB
  try {
    $pdoConn = new PDO($GLOBALS['PDO_CONNECT_STMT'], $GLOBALS['PDO_USER'], $GLOBALS['PDO_PASSWORD']);
    $pdoConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  } catch (PDOException $e) {
    echo("<pre>");
    echo "Error connecting\n\n";
    echo "Message:\n", $e->getMessage(), "\n\n";
    echo "File/Line:\n", $e->getFile(), ", ", $e->getLine(), "\n\n";
    echo "Trace:\n", $e->getTraceAsString(), "\n";
    echo("</pre>");
    $pdoConn = $sqlStatement = null;
    die;
  }
  // PDO Statement
  $sqlStatement = $pdoConn->prepare("
      UPDATE dbo.CCA_REQUISITION_HEADER
      SET dbo.CCA_REQUISITION_HEADER.RH_STATUS = :status
      WHERE
      dbo.CCA_REQUISITION_HEADER.RH_ID = :id
    ");
  // PDO Parameters
  $paramsSql = null;
  $paramsSql[':status'] = $status;
  $paramsSql[':id']     = $headerId;
  // PDO Execute Statement
  try {
    $sqlStatement->execute($paramsSql);
  } catch (PDOException $e) {
    echo("<pre>");
    echo("Error Executing Statement");
    echo "Message:\n", $e->getMessage(), "\n\n";
    echo "File/Line:\n", $e->getFile(), ", ", $e->getLine(), "\n\n";
    echo "Trace:\n", $e->getTraceAsString(), "\n\n";
    echo "\$status:\n", $e->getTraceAsString(), "\n";
    echo "\$id:\n", $e->getTraceAsString(), "\n";
    echo("</pre>");
    $pdoConn = $sqlStatement = null;
    die;
  }
  // Kill Connection To DB
  $pdoConn = $sqlStatement = null;
  // Return
  return true;
}



function UpdateHeaderTotal($headerId, $user, $date) {
  // PDO Connection
  try {
    $pdoConn = new PDO($GLOBALS['PDO_CONNECT_STMT'], $GLOBALS['PDO_USER'], $GLOBALS['PDO_PASSWORD']);
    $pdoConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  } catch (PDOException $e) {
    printExceptionAndDie($e);
  }

  /*******************
   ** GET ITEM TOTAL **
  ********************/
  // PDO Statement
  $sqlGetTotal = $pdoConn->prepare("
      SELECT SUM ( dbo.CCA_REQUISITION_DETAIL.RL_EXTENDED ) AS itemTotal
      FROM dbo.CCA_REQUISITION_DETAIL
      WHERE dbo.CCA_REQUISITION_DETAIL.RL_RHID = :id
    ");
  // PDO Parameters
  $paramsSql = null;
  $paramsSql[':id'] = $headerId;
  // PDO Execute Statement
  try {
    $sqlGetTotal->execute($paramsSql);
  } catch (PDOException $e) {
    printExceptionAndDie($e);
  }
  // Fetch Results (total)
  $itemTotal = $sqlGetTotal->fetch(PDO::FETCH_ASSOC);
  $itemTotal = isset($itemTotal['itemTotal']) ? $itemTotal['itemTotal'] : 0.00;
  $itemTotal = is_numeric($itemTotal) ? $itemTotal : 0.00;

  /*******************************
   ** GET SHIPPING AND TAX TOTAL **
  ********************************/
  // SQL query to get shipping and tax
  $sql = "SELECT
          dbo.CCA_REQUISITION_HEADER.RH_SHIPPINGAMOUNT AS shippingamount,
          dbo.CCA_REQUISITION_HEADER.RH_TAX AS tax
        FROM dbo.CCA_REQUISITION_HEADER
        WHERE dbo.CCA_REQUISITION_HEADER.RH_ID = :id
      ";
  // Create PDO Statement
  $sqlGetShippingTax = $pdoConn->prepare($sql);
  // PDO Parameters
  $paramsSql = null;
  $paramsSql[':id'] = $headerId;
  // PDO Execute Statement
  try {
    $sqlGetShippingTax->execute($paramsSql);
  } catch (PDOException $e) {
    printExceptionAndDie($e);
  }
  // Fetch Results (total)
  $shippingAndTax = $sqlGetShippingTax->fetch(PDO::FETCH_ASSOC);

  $shipping = isset($shippingAndTax['shippingamount']) ? $shippingAndTax['shippingamount'] : '0.00';
  $shipping = is_numeric($shipping) ? $shipping : '0.00';

  $tax = isset($shippingAndTax['tax']) ? $shippingAndTax['tax'] : '0.00';
  $tax = is_numeric($tax) ? $tax : '0.00';

  /***************************************
   ** Add item total and shipping amount **
  ****************************************/
  // Add item total and shipping amount
  $grandTotal = round(($itemTotal + $shipping + $tax), 2);

  /*************************************
   ** Update $grandTotal into database **
  **************************************/
  // PDO Statement - update total in header
  $sql = "UPDATE dbo.CCA_REQUISITION_HEADER
        SET
          dbo.CCA_REQUISITION_HEADER.RH_REQTOTAL = :RH_REQTOTAL,
          dbo.CCA_REQUISITION_HEADER.RH_LASTUPDATED = :RH_LASTUPDATED,
          dbo.CCA_REQUISITION_HEADER.RH_LASTUSER = :RH_LASTUSER
        WHERE dbo.CCA_REQUISITION_HEADER.RH_ID = :id
      ";
  $sqlHeaderUpdate = $pdoConn->prepare($sql);
  // PDO Parameters
  $paramsSql = null;
  $paramsSql[':RH_REQTOTAL'] = $grandTotal;
  $paramsSql[':RH_LASTUPDATED'] = $date;
  $paramsSql[':RH_LASTUSER'] = $user;
  $paramsSql[':id'] = $headerId;
  // PDO Execute Statement
  try {
    $sqlHeaderUpdate->execute($paramsSql);
  } catch (PDOException $e) {
    echo("<pre>");
    echo "Message:\n", $e->getMessage(), "\n\n";
    echo "File/Line:\n", $e->getFile(), ", ", $e->getLine(), "\n\n";
    echo "Trace:\n", $e->getTraceAsString(), "\n";
    echo("</pre>");
    $pdoConn = $sqlStatement = null;
    die;
  }
  // Kill db connection
  $pdoConn = $sqlStatement = null;
  // Return True
  return $grandTotal;
}

function isMgmtApproval($user){
  $users = GetSettings('managerApproval');
  if (array_search($user, $users)) {
    return true;
  }
  return false;
}

// Section: These functions shouldn't change from project to project

function GetFormattedDate()
{
  return date('Y-m-d H:i:s');
}

function ItemExists($item, $location)
{
  // Connect to db; Execute query; Kill db connection
  try {
    $pdoConn = new PDO($GLOBALS['PDO_CONNECT_STMT'], $GLOBALS['PDO_USER'], $GLOBALS['PDO_PASSWORD']);
    $pdoConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  } catch (PDOException $e) {
    echo("<pre>");
    echo "Message:\n", $e->getMessage(), "\n\n";
    echo "File/Line:\n", $e->getFile(), ", ", $e->getLine(), "\n\n";
    echo "Trace:\n", $e->getTraceAsString(), "\n";
    echo("</pre>");
    $pdoConn = $sqlStatement = null;
    die;
  }
  // SQL PDO Statement
  $sqlStatement = $pdoConn->prepare(
  "SELECT COUNT(dbo.SWCCSSTOK.stocknumber) as num
      FROM dbo.SWCCSSTOK
      WHERE dbo.SWCCSSTOK.stocknumber     = :itemNumber
      AND dbo.SWCCSSTOK.locationnumber    = :location"
  );
  // SQL PDO Parameters
  $paramsSql[':itemNumber']   = $item;
  $paramsSql[':location']     = $location;

  // PDO Execute Statement
  try {
    $sqlStatement->execute($paramsSql);
  } catch (PDOException $e) {
    echo("<pre>");
    echo "Message:\n", $e->getMessage(), "\n\n";
    echo "File/Line:\n", $e->getFile(), ", ", $e->getLine(), "\n\n";
    echo "Trace:\n", $e->getTraceAsString(), "\n";
    echo("</pre>");
    $pdoConn = $sqlStatement = null;
    die;
  }

  // Fetch entire result
  $result = $sqlStatement->fetch(PDO::FETCH_ASSOC);
  // If No Results, Return False
  if ($result['num'] <= 0) {
    return false;
  }
  // Return True
  return true;
}

function GetCustomerName($customerNumber)
{
  // Connect to db; Execute query; Kill db connection
  try {
    $pdoConn = new PDO($GLOBALS['PDO_CONNECT_STMT'], $GLOBALS['PDO_USER'], $GLOBALS['PDO_PASSWORD']);
    $pdoConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  } catch (PDOException $e) {
    echo("<pre>");
    echo "Message:\n", $e->getMessage(), "\n\n";
    echo "File/Line:\n", $e->getFile(), ", ", $e->getLine(), "\n\n";
    echo "Trace:\n", $e->getTraceAsString(), "\n";
    echo("</pre>");
    $pdoConn = $sqlStatement = null;
    die;
  }
  // PDO Statement
  $sqlStatement = $pdoConn->prepare(
  "SELECT dbo.SWCCRCUST.customername AS name
      FROM dbo.SWCCRCUST
      WHERE LTRIM(dbo.SWCCRCUST.customernumber) = :customerNumber "
  );
  // PDO Parameters
  $paramsSql[':customerNumber'] = $customerNumber;

  // PDO Execute Statement
  try {
    $sqlStatement->execute($paramsSql);
  } catch (PDOException $e) {
    echo("<pre>");
    echo "Message:\n", $e->getMessage(), "\n\n";
    echo "File/Line:\n", $e->getFile(), ", ", $e->getLine(), "\n\n";
    echo "Trace:\n", $e->getTraceAsString(), "\n";
    echo("</pre>");
    $pdoConn = $sqlStatement = null;
    die;
  }

  // Fetch Row
  $result = $sqlStatement->fetch(PDO::FETCH_ASSOC);
  // Kill the database connection
  $pdoConn = $sqlStatement = null;
  // If No Results, Return False
  if (count($result) <= 0) {
    return false;
  }
  // Return Customer Name
  return $result['name'];
}
