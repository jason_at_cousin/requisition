<?php
require_once($_SESSION['moduleroot'] . '/init.php');
// old code
// require_once($_SESSION['moduleroot'] . '/PHPMailer/PHPMailerAutoload.php');
require_once($_SESSION['moduleroot'] . '/jason/SendMail.php');


/**
 * Main sendmail function
 *
 * @param string - $subject
 * @param string - $body
 * @param array[string]:string - $to[name] = email address
 * @return void
 */
function sendCustomMail($subject, $body, $to) // updated to db mail
{
  $jMail = new \jason\SendMail();
  // clear everything first
  $jMail->clear();
  // If we're in the sw dev folder, don't send to everyone
  if (preg_match('/southware.+((dev)|(test))/', __DIR__) || ((isset($GLOBALS['env']) && $GLOBALS['env'] != "prod") || (isset($_SESSION['env']) && $_SESSION['env'] != "prod"))) {
    // if user is jeff only send to jeff or if jason only send to jason
    if (stripos($_COOKIE['userName'], 'jeff') > -1)
      $jMail->addTo('jeffrabin@cousin.com', 'Jeff Rabin');
    else
      $jMail->addTo('jason@cousin.com', 'Jason');
    // since we're in dev, add to the body
    $newBody = "You're receiving this email because someone was using this tool in either 'dev' or 'test'. \t\n";
    $newBody .= "This email will only be seen by Jason or Jeff for testing/debugging. \t\n\n";
    $body = $newBody . $body;
  } else {
    // if we're not in dev, add addresses like normal
    foreach ($to as $emailName => $emailAddress)
      $jMail->addTo($emailAddress, $emailName);
  }
  // set the subject, body and send it
  $jMail->setSubject($subject);
  $jMail->setBodyText($body);
  $jMail->sendEmail();
  // return so we don't run the old code below
  return;

  /** old way **/

  $mail = new PHPMailer;
  /**
   * server settings
   */
  $mail->isSMTP();
  $mail->Host = 'smtp.office365.com';
  $mail->SMTPAuth = true;
  $mail->Username = 'info@laurajanelle.com';
  $mail->Password = 'b0wnDrnw';
  $mail->SMTPSecure = 'tls';
  $mail->Port = 587;
  // $mail->SMTPDebug = 3;
  /**
   * email settings
   */
  $mail->addReplyTo('computer@cousin.com', 'Computer');
  $mail->isHTML(false);
  $mail->setFrom('info@laurajanelle.com', 'Requisition');
  // $mail->setFrom('computer@cousin.com', 'Computer'); // Setting from that's different that user account fails
  $mail->Subject = $subject;
  /* Setup Recipients */
  // If we're in the sw dev folder, don't send to everyone
  if (preg_match('/southware.+((dev)|(test))/', __DIR__) || ((isset($GLOBALS['env']) && $GLOBALS['env'] != "prod") || (isset($_SESSION['env']) && $_SESSION['env'] != "prod"))) {
    $mail->addAddress('jason@cousin.com', 'Jason');
    if (stripos($_COOKIE['userName'], 'jeff') > -1) {
      $mail->addAddress('jeffrabin@cousin.com', 'Jeff Rabin');
    }
    $newBody = "You're receiving this email because someone was using this tool in either 'dev' or 'test'. \t\n";
    $newBody .= "This email will only be seen by Jason and Jeff for testing/debugging. \t\n\n";
    $body = $newBody . $body;
  } else {
    foreach ($to as $emailName => $emailAddress) {
      $mail->addAddress($emailAddress, $emailName);
    }
    $mail->addBCC('jason@cousin.com');
  }
  /* Message Body */
  $mail->Body = $body;
  /* Send the Email */
  if (!$mail->send()) {
    echo $mail->ErrorInfo;
  }
}

/*****************************
 * Unique sendmail functions *
 *****************************/

/**
 * Send to lisa for approval if dept approved and total >= 100
 * @param string
 * @return void
 */
function sendForApproval($id)
{
  $sendTo = null;
  $sendTo['Lisa'] = "lisa@cousin.com";

  $subject = "A requisition request requires your approval";

  $body = "Here are the details of the request needing approved:\t\n\n";
  $body .= getEmailDetails($id);
  $body .= "\t\n";

  foreach ($sendTo as $eName => $eEmail) {
    $to = null;
    $to[$eName] = $eEmail;
    preg_match('/^.[^@]+/', $eEmail, $matches);
    $user = $matches[0];
    $newBody = $body . "http://cc/modules/requisition/header.php?id=" . $id . "&env=" . $_COOKIE['env'] . "&user=" . $user;

    sendCustomMail($subject, $newBody, $to);

    // pause so multiple emails will send successfully, 1000000 = 1 second
    // usleep(75 * 10000); // 75 = 3/4 of a second
  }
}


/**
 * Send to Develyn if mgmt approved and total >= 1200
 */
function sendOver1200($id)
{
  $sendTo = null;
  $sendTo['Develyn'] = "develyn@cousin.com";

  $subject = "A requisition request was submitted for $1,200.00 or more";

  $body = "You're receiving this email because you wanted to be notified of requests totaling $1,200.00 or more:\t\n\n";
  $body .= getEmailDetails($id);
  $body .= "\t\n";

  foreach ($sendTo as $eName => $eEmail) {
    $to = null;
    $to[$eName] = $eEmail;
    preg_match('/^.[^@]+/', $eEmail, $matches);
    $user = $matches[0];
    $newBody = $body . "http://cc/modules/requisition/header.php?id=" . $id . "&env=" . $_COOKIE['env'] . "&user=" . $user;

    sendCustomMail($subject, $newBody, $to);

    // pause so multiple emails will send successfully, 1000000 = 1 second
    // usleep(75 * 10000); // 75 = 3/4 of a second
  }
}


/**
 * Send to Jeff if dept approved
 */
function sendDeptApproved($id)
{
  $sendTo = null;
  $sendTo['Jeff Rabin'] = "jeffrabin@cousin.com";
  // $sendTo['Marty DiMura'] = "marty@cousin.com";

  $subject = "A requisition request was approved by the department";

  $body = "You're receiving this email because you wanted to be notified when a request is approved by the department:\t\n\n";
  $body .= getEmailDetails($id);
  $body .= "\t\n";

  foreach ($sendTo as $eName => $eEmail) {
    $to = null;
    $to[$eName] = $eEmail;
    preg_match('/^.[^@]+/', $eEmail, $matches);
    $user = $matches[0];
    $newBody = $body . "http://cc/modules/requisition/header.php?id=" . $id . "&env=" . $_COOKIE['env'] . "&user=" . $user;

    sendCustomMail($subject, $newBody, $to);

    // pause so multiple emails will send successfully, 1000000 = 1 second
    // usleep(75 * 10000); // 75 = 3/4 of a second
  }
}


/**
 * Send to Marty if dept approved and total >= 100
 */
function sendOver100($id)
{
  $sendTo = null;
  $sendTo['Marty DiMura'] = "marty@cousin.com";

  $subject = "A requisition request was submitted for $100.00 or more";

  $body = "You're receiving this email because you wanted to be notified of requests totaling $100.00 or more:\t\n\n";
  $body .= getEmailDetails($id);
  $body .= "\t\n";

  foreach ($sendTo as $eName => $eEmail) {
    $to = null;
    $to[$eName] = $eEmail;
    preg_match('/^.[^@]+/', $eEmail, $matches);
    $user = $matches[0];
    $newBody = $body . "http://cc/modules/requisition/header.php?id=" . $id . "&env=" . $_COOKIE['env'] . "&user=" . $user;

    sendCustomMail($subject, $newBody, $to);

    // pause so multiple emails will send successfully, 1000000 = 1 second
    // usleep(75 * 10000); // 75 = 3/4 of a second
  }
}


/**
 * Send to Purchaser when mgmt approved and purchaser selected
 *
 * @param int - $id
 * @param string - $to
 */
function sendPurchase($id, $to)
{
  $purchasers = GetSettings('purchasers');
  $purchasersLC = array_change_key_case($purchasers);
  if (array_key_exists(strtolower($to), $purchasersLC)) {
    $purchaser = $purchasersLC[strtolower($to)];
  }

  $purchaserName = $purchaser['first'] . ' ' . $purchaser['last'];
  $purchaserUserName = $purchaser['username'];
  $purchaserEmail = $purchaser['email'];

  $subject = "Can you please make an order for this requisition";

  $body = "Someone has chosen you to place an order for this requisition:\t\n\n";
  $body .= getEmailDetails($id);
  $body .= "\t\n";

  $to = null;
  $to[$purchaserName] = $purchaserEmail;

  $newBody = $body . "http://cc/modules/requisition/header.php?id=" . $id . "&env=" . $_COOKIE['env'] . "&user=" . $purchaserUserName;

  sendCustomMail($subject, $newBody, $to);
}


/**
 * Send to Purchaser when mgmt approved and purchaser selected
 *
 * @param int - $id
 */
function sendToAccountingOnClosed($id)
{
  $recipients = GetSettings('accounting:users');

  $subject = "Requisition has been closed";

  $body = "You're receiving this email because a requisition has just been closed.\t\n";
  $body .= "Here are some of the details:\t\n\n";
  $body .= getEmailDetails($id);
  $body .= "\t\n";

  foreach ($recipients as $name => $details) {
    $to = null;
    $to[$name] = $details['email'];

    $newBody = $body . "http://cc/modules/requisition/header.php?id=" . $id . "&env=" . $_COOKIE['env'] . "&user=" . $details['username'];

    sendCustomMail($subject, $newBody, $to);
  }
}


/**
 * Get a list of details to be send in an email
 *
 * @param int - $id The header id of the requisition to send details of
 * @return string - The details of the requisition
 */
function getEmailDetails($id)
{
  $headerData = GetHeader($id);
  // emailDetails
  $emailDetails = "Request was submitted by " . ucwords($headerData['rh_requser']) . " on " . $headerData['rh_dateadded'] . "\t\n";
  $emailDetails .= "Total Amount: " . $headerData['rh_reqtotal'] . "\t\n";
  $emailDetails .= isset($headerData['rh_daterequested']) && strlen(trim($headerData['rh_daterequested'])) > 0 ? "Need By: " . trim($headerData['rh_daterequested']) . "\t\n" : "";
  $emailDetails .= isset($headerData['rh_requestedfor']) && strlen(trim($headerData['rh_requestedfor'])) > 0 ? "For: " . trim($headerData['rh_requestedfor']) . "\t\n" : "";
  $emailDetails .= isset($headerData['rh_reqdept']) && strlen(trim($headerData['rh_reqdept'])) > 0 ? "Dept: " . trim($headerData['rh_reqdept']) . "\t\n" : "";
  $emailDetails .= isset($headerData['rh_suppliername']) && strlen(trim($headerData['rh_suppliername'])) > 0 ? "Supplier: " . trim($headerData['rh_suppliername']) . "\t\n" : "";
  $emailDetails .= isset($headerData['rh_supplieraddress']) && strlen(trim($headerData['rh_supplieraddress'])) > 0 ? "Supplier Address: " . trim($headerData['rh_supplieraddress']) . "\t\n" : "";
  $emailDetails .= isset($headerData['rh_supplierphone']) && strlen(trim($headerData['rh_supplierphone'])) > 0 ? "Supplier Phone: " . trim($headerData['rh_supplierphone']) . "\t\n" : "";
  $emailDetails .= isset($headerData['rh_supplieremail']) && strlen(trim($headerData['rh_supplieremail'])) > 0 ? "Supplier Email: " . trim($headerData['rh_supplieremail']) . "\t\n" : "";
  $emailDetails .= isset($headerData['rh_suppliercontact']) && strlen(trim($headerData['rh_suppliercontact'])) > 0 ? "Supplier Contact: " . trim($headerData['rh_suppliercontact']) . "\t\n" : "";
  $emailDetails .= isset($headerData['rh_shiptoaddr']) && strlen(trim($headerData['rh_shiptoaddr'])) > 0 ? "Ship To: " . trim($headerData['rh_shiptoaddr']) . "\t\n" : "";
  $emailDetails .= isset($headerData['rh_notes']) && strlen(trim($headerData['rh_notes'])) > 0 ? "Notes: " . trim($headerData['rh_notes']) . "\t\n" : "";
  $emailDetails .= isset($headerData['rh_justification']) && strlen(trim($headerData['rh_justification'])) > 0 ? "Justification: " . trim($headerData['rh_justification']) . "\t\n" : "";
  $emailDetails .= "\t\n\t\n";
  // return
  return $emailDetails;
}
