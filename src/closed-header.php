<?php
require_once('init.php');

$deletedDetailId = '';
$deletedDetailSuccess = false;
$headerData;
$id;
$status;
$updateSuccess = false;

// set $id and $status if passed via URL
if (isset($_GET['copy'])) {
  // copy entry
    $id = CopyEntry($_GET['copy']);
    $status = "copy";
  // set $headerData
    $headerData = GetHeader($id);
  // set $id and $status if form submitted
} elseif (isset($_GET['id'])) {
    $id = $_GET['id'];
    $status = "get";
  // set $deletedDetailId and $deletedDetailSuccess
  if (isset($_GET['deleteid'])) {
    $deletedDetailId = $_GET['deleteid'];
    $deletedDetailSuccess = DeleteDetailById($deletedDetailId, $id);
  } elseif (isset($_GET['resend']) && $_GET['resend'] == 'true') {
    // send lisa's email
    sendForApproval($id);
  }
  // set $headerData
    $headerData = GetHeader($id);
  // set $id and $status if form submitted
} elseif (isset($_POST['rh-id'])) {
    $id = $_POST['rh-id'];
    $status = "post";

  // set $updateSuccess
  if (PushChangesHeader($_POST)) {
    $updateSuccess = true;
  }
  // set $headerData
    $headerData = GetHeader($id);
} else {
    $status = false;
}

// department start
// Holds the department name of the current user
$currentDept;
// Holds a list of distinct departments
$departments;

// $currentDept
if ($userDept = GetUserDept($currentUser)) {
    $currentDept = $userDept;
} else {
    $currentDept = "unknown";
}

if (!$departments = GetDepartments()) {
    $departments = null;
    $departments[] = $currentDept;
}
// department end

$closed = strtolower($headerData['rh_status']) === 'closed' ? true : false ;
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Requisition</title>
  <!-- bootstrap styles -->
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/bootstrap-theme.min.css">
  <!-- datatable styles
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.css"> -->
  <link rel="stylesheet" type="text/css" href="DataTables/datatables.min.css">
  <!-- daterangepicker styles -->
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
    body {
      padding-top: 70px;
      padding-bottom: 250px;
    }

    .details-header-old{
      margin-top: 60px;
      margin-bottom: 20px;
    }

    #table-container {
      padding-right: 40px;
      padding-left: 40px;
      margin-top: 60px;
      margin-right: 40px;
    }

    p.navbar-right {
      padding-right: 25px;
    }

    .totals{
      margin-left: 40px;
    }

    .btn-spc-right {
      margin-right: 10px;
    }
  </style>
</head>
<body>
  <!-- Alert Still Testing -->
    <!-- <div class="container">
      <div class="row">
        <div class="col-xs-8 col-xs-offset-1">
          <div class="alert alert-warning alert-dismissable" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <p>
              <strong>Warning:</strong> This system is still being tested and updated.
              You may occasionally find typos or encounter errors. If you do, please
              let <a href="mailto:jeffrabin@cousin.com?subject=Requisition%20Problem">Jeff</a> know.
              Thanks.
            </p>
          </div>
        </div>
      </div>
    </div> -->
  <!-- Alert Still Testing -->

  <!-- Nav start -->
    <nav class="navbar navbar-fixed-top <?php echo $GLOBALS['env'] === "prod"?"navbar-default":"navbar-inverse"; ?>">
      <div class="container-fluid">

        <div class="navbar-header">
          <a href="index.php" class="navbar-brand">Requisition</a>
        </div>

        <ul class="nav navbar-nav">
          <li>
            <a href="index.php">Current</a>
          </li>

          <li class="active">
            <a href="closed-reqs.php">History</a>
          </li>
          <li class="active">
            <a href="#">Header</a>
          </li>

          <li>
            <a href="add-header.php">Add</a>
          </li>

          <li class="">
              <a href="hist-index.php">Log</a>
          </li>
        </ul>

        <p class="navbar-text navbar-right">
            <?php echo ($GLOBALS['env'] == "prod") ? "Production" : "Development"; ?> Environment
        </p>

      </div>
    </nav>
  <!-- Nav end -->

  <div class="container">
    <!-- Debug Mode -->
        <?php if (isset($_COOKIE['debug']) && $_COOKIE['debug'] === "1") : ?>
        <div class="row">
          <div class="col-sm-8 col-sm-offset-1">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">DEBUG</h3>
              </div>
              <div class="panel-body">
                <pre>$headerData = <?php print_r($headerData) ?></pre>
              </div>
            </div>
          </div>
        </div>
        <?php endif ?>
    <!-- Show if updated -->
        <?php if ($updateSuccess) : ?>
        <div class="row">
          <div class="col-sm-8 col-sm-offset-1">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">Header Saved</h3>
              </div>
              <div class="panel-body">
                <p>
                  Header information was saved. If needed, you may make additional changes and click 'Update'.
                  If you're finished, please click the 'Finish' button to go back to the main list.
                </p>
                <a class="btn btn-sm btn-primary" href="index.php" role="button">Finish</a>
              </div>
            </div>
          </div>
        </div>
        <?php endif ?>
    <!-- Show if deleted -->
        <?php if ($deletedDetailSuccess) : ?>
        <div class="row">
          <div class="col-sm-8 col-sm-offset-1">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">Record Deleted</h3>
              </div>
              <div class="panel-body">
                <p>
                  Detail record (ID: <?php echo $deletedDetailId; ?>) was deleted from the database.
                  If needed, you may make additional changes and click 'Update'. If you're finished,
                  please click the 'Finish' button to go back to the main list.
                </p>
                <a class="btn btn-sm btn-primary" href="index.php" role="button">Finish</a>
              </div>
            </div>
          </div>
        </div>
        <?php endif ?>
    <!-- Form -->
      <form action="header.php" method="POST" id="form-requisition" class="form-horizontal">
      <!-- next page checks existance of this field to know if form was submitted -->
        <input type="hidden" name="form-submitted" id="form-submitted" value="header">

      <!-- rh_id -->
        <div class="form-group">
          <label for="rh-id" class="col-sm-2 control-label">ID</label>
          <div class="col-sm-1">
            <p class="form-control-static" id="rh-id"><?php echo $headerData['rh_id']; ?></p>
            <input type="hidden" name="rh-id" value="<?php echo $headerData['rh_id']; ?>">
          </div>
        </div>

      <!-- rh_dateadded -->
        <div class="form-group">
          <label for="rh-dateadded" class="col-sm-2 control-label">Date Added</label>
          <div class="col-sm-4">
            <p class="form-control-static" id="rh-dateadded"><?php echo $headerData['rh_dateadded']; ?></p>
            <input type="hidden" name="rh-dateadded" value="<?php echo $headerData['rh_dateadded']; ?>">
          </div>
        </div>

      <!-- rh_daterequested -->
        <div class="form-group">
          <label for="rh-daterequested" class="col-sm-2 control-label">Deliver By</label>
          <div class="col-sm-4">
            <input
              type="text"
              placeholder="Optional"
              name="rh-daterequested"
              id="rh-daterequested"
              class="form-control input-sm date-picker"
              autocomplete="off"
              value="<?php echo preg_match("/^1900\-01\-01/", $headerData['rh_daterequested']) ? "" : $headerData['rh_daterequested']; ?>"
              <?php echo $closed ? 'disabled' : ''; ?>
            >
          </div>
        </div>

      <!-- rh-requestedfor -->
        <div class="form-group">
          <label for="rh-requestedfor" class="col-sm-2 control-label">Requested For</label>
          <div class="col-sm-4">
            <input
              value="<?php echo $headerData['rh_requestedfor']; ?>"
              type="text"
              placeholder="Optional"
              name="rh-requestedfor"
              id="rh-requestedfor"
              data-jval-max="50"
              class="form-control input-sm"
              autocomplete="off"
              <?php echo $closed ? 'disabled' : ''; ?>
            >
          </div>
        </div>

      <!-- rh-requser -->
        <div class="form-group">
          <label for="rh-requser" class="col-sm-2 control-label">Requested By</label>
          <div class="col-sm-4">
            <p class="form-control-static" id="rh-requser"><?php echo $headerData['rh_requser']; ?></p>
            <input type="hidden" name="rh-requser" value="<?php echo $headerData['rh_requser']; ?>" >
          </div>
        </div>

      <!-- rh-reqdept -->
        <div class="form-group">
          <label for="rh-reqdept" class="col-sm-2 control-label">Requesting Department</label>
          <div class="col-sm-4">
            <select class="form-control input-sm" id="rh-reqdept" name="rh-reqdept" <?php echo $closed ? 'disabled' : ''; ?>>
                <?php
                foreach ($departments as $idx => $department) {
                    echo "<option value=\"" . ucwords($department) . "\"" . (strtolower($headerData['rh_reqdept']) == strtolower($department)?'selected':'') . ">" . ucwords($department) . "</option>";
                }
                ?>
            </select>
          </div>
        </div>

      <!-- rh-suppliername -->
        <div class="form-group">
          <label for="rh-suppliername" class="col-sm-2 control-label">Supplier Name</label>
          <div class="col-sm-4">
            <input
              <?php echo $closed ? 'disabled' : ''; ?>
              autocomplete="off"
              class="form-control input-sm"
              data-jval-min="3"
              data-jval-min="50"
              data-jval-required
              id="rh-suppliername"
              name="rh-suppliername"
              type="text"
              value="<?php echo $headerData['rh_suppliername']; ?>"
            >
          </div>
        </div>

      <!-- rh-supplieraddress -->
        <div class="form-group">
          <label for="rh-supplieraddress" class="col-sm-2 control-label">Supplier Address</label>
          <div class="col-sm-4">
            <textarea class="form-control input-sm" name="rh-supplieraddress" id="rh-supplieraddress" rows="5" placeholder="Optional" <?php echo $closed ? 'disabled' : ''; ?>>
              <?php echo $headerData['rh_supplieraddress']; ?>
            </textarea>
          </div>
        </div>

      <!-- rh-supplierphone -->
        <div class="form-group">
          <label for="rh-supplierphone" class="col-sm-2 control-label">Supplier Phone</label>
          <div class="col-sm-4">
            <div class="input-group">
              <div class="input-group-addon"><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span></div>
              <input
                <?php echo $closed ? 'disabled' : ''; ?>
                autocomplete="off"
                class="form-control input-sm"
                data-jval-max="25"
                data-jval-min="7"
                id="rh-supplierphone"
                name="rh-supplierphone"
                placeholder="Optional"
                type="text"
                value="<?php echo $headerData['rh_supplierphone']; ?>"
              >
            </div>
          </div>
        </div>

      <!-- rh-supplieremail -->
        <div class="form-group">
          <label for="rh-supplieremail" class="col-sm-2 control-label">Supplier Email</label>
          <div class="col-sm-4">
            <div class="input-group">
              <div class="input-group-addon"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span></div>
              <input
                <?php echo $closed ? 'disabled' : ''; ?>
                autocomplete="off"
                class="form-control input-sm"
                data-jval-email
                data-jval-max="100"
                id="rh-supplieremail"
                name="rh-supplieremail"
                placeholder="Optional"
                type="text"
                value="<?php echo $headerData['rh_supplieremail']; ?>"
              >
            </div>
          </div>
        </div>

      <!-- rh-suppliercontact -->
        <div class="form-group">
          <label for="rh-suppliercontact" class="col-sm-2 control-label">Supplier Contact</label>
          <div class="col-sm-4">
            <div class="input-group">
              <div class="input-group-addon"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></div>
              <input
                <?php echo $closed ? 'disabled' : ''; ?>
                autocomplete="off"
                class="form-control input-sm"
                data-jval-max="50"
                data-jval-min="3"
                id="rh-suppliercontact"
                name="rh-suppliercontact"
                placeholder="Optional"
                type="text"
                value="<?php echo $headerData['rh_suppliercontact']; ?>"
              >
            </div>
          </div>
        </div>

      <!-- rh-shiptoaddr -->
        <div class="form-group">
          <label for="rh-shiptoaddr" class="col-sm-2 control-label">Ship To Address</label>
          <div class="col-sm-4">
            <textarea
              <?php echo $closed ? 'disabled' : ''; ?>
              class="form-control input-sm"
              id="rh-shiptoaddr"
              name="rh-shiptoaddr"
              placeholder="Optional if not CCA's address"
              rows="5"
            >
              <?php echo $headerData['rh_shiptoaddr']; ?>
            </textarea>
          </div>
        </div>

      <!-- rh-deptapproval -->
        <input
          type="hidden"
          name="rh-deptapproval-was"
          id="rh-deptapproval-was"
          value="<?php echo isset($headerData['rh_deptapproval']) ? $headerData['rh_deptapproval'] : ""; ?>">
        <div class="form-group">
          <label for="rh-deptapproval" class="col-sm-2 control-label">Dept Approval</label>
          <div class="col-sm-4">
            <select name="rh-deptapproval" id="rh-deptapproval" class="form-control" <?php echo $closed ? 'disabled' : ''; ?>>
              <option value="N" <?php echo $headerData['rh_deptapproval'] == "N" ? "selected" : ""; ?> >
                Not Approved
              </option>
              <option value="Y" <?php echo $headerData['rh_deptapproval'] == "Y" ? "selected" : ""; ?> >
                Approved
              </option>
            </select>
          </div>
        </div>

      <!-- Dept Approval Date/User -->
        <input type="hidden" name="rh-deptapprovaldate" id="rh-deptapprovaldate" value="<?php echo $headerData['rh_deptapprovaldate']; ?>" >
        <input type="hidden" name="rh-deptapprovaluser" id="rh-deptapprovaluser" value="<?php echo $headerData['rh_deptapprovaluser']; ?>" >

        <?php if ($headerData['rh_deptapproval'] == "Y") : ?>
      <!-- rh-deptapprovaldate -->
        <div class="form-group">
          <label for="rh-deptapprovaldate-text" class="col-sm-2 control-label">Dept Approval Date</label>
          <div class="col-sm-4">
            <p class="form-control-static" id="rh-deptapprovaldate-text">
                <?php echo $headerData['rh_deptapprovaldate']; ?>
            </p>
          </div>
        </div>

      <!-- rh-deptapprovaluser -->
        <div class="form-group">
          <label for="rh-deptapprovaluser-text" class="col-sm-2 control-label">Dept Approval User</label>
          <div class="col-sm-4">
            <p class="form-control-static" id="rh-deptapprovaluser-text"><?php echo $headerData['rh_deptapprovaluser']; ?></p>
          </div>
        </div>
        <?php endif ?>
      <!-- rh-mgtapproval -->
        <input
          id="rh-mgtapproval-was"
          name="rh-mgtapproval-was"
          type="hidden"
          value="<?php echo isset($headerData['rh_mgtapproval']) ? $headerData['rh_mgtapproval'] : ""; ?>"
        >
        <div class="form-group">
          <label for="rh-mgtapproval" class="col-sm-2 control-label">Mgmt Approval</label>
          <div class="col-sm-4">
            <select name="rh-mgtapproval" id="rh-mgtapproval" class="form-control" <?php echo $closed ? 'disabled' : ''; ?>>
              <option value="N" <?php echo $headerData['rh_mgtapproval'] == "N" ? "selected" : ""; ?> >Not Approved</option>
              <option value="Y" <?php echo $headerData['rh_mgtapproval'] == "Y" ? "selected" : ""; ?> >Approved</option>
            </select>
          </div>
        </div>

      <!-- Mgmt Approval Date/User -->
        <input
          value="<?php echo $headerData['rh_mgtapprovaldate']; ?>"
          type="hidden"
          name="rh-mgtapprovaldate"
          id="rh-mgtapprovaldate">
        <input
          value="<?php echo $headerData['rh_mgtapprovaluser']; ?>"
          type="hidden"
          name="rh-mgtapprovaluser"
          id="rh-mgtapprovaluser">

        <?php if ($headerData['rh_mgtapproval'] == "Y") : ?>
      <!-- rh-mgtapprovaldate -->
        <div class="form-group">
          <label for="rh-mgtapprovaldate-text" class="col-sm-2 control-label">Mgmt Approval Date</label>
          <div class="col-sm-4">
            <p class="form-control-static" id="rh-mgtapprovaldate-text"><?php echo $headerData['rh_mgtapprovaldate']; ?></p>
          </div>
        </div>

      <!-- rh-mgtapprovaluser -->
        <div class="form-group">
          <label for="rh-mgtapprovaluser-text" class="col-sm-2 control-label">Mgmt Approval User</label>
          <div class="col-sm-4">
            <p class="form-control-static" id="rh-mgtapprovaluser-text"><?php echo $headerData['rh_mgtapprovaluser']; ?></p>
          </div>
        </div>
        <?php endif ?>

      <!-- rh-purchaser -->
        <input
          type="hidden"
          name="rh-purchaser-was"
          id="rh-purchaser-was"
          value="<?php echo isset($headerData['rh_purchaser']) ? $headerData['rh_purchaser'] : ""; ?>">
        <div class="form-group">
          <label for="rh-purchaser" class="col-sm-2 control-label">Purchaser</label>
          <div class="col-sm-4">
            <select name="rh-purchaser" id="rh-purchaser" class="form-control" <?php echo $closed ? 'disabled' : ''; ?>>
              <option value="" <?php echo $headerData['rh_purchaser'] == "" ? "selected" : ""; ?> >Select Purchaser</option>
                <?php
                $purchasers = GetSettings('purchasers');
                foreach ($purchasers as $purchaser => $details) {
                    echo '<option value="' . $purchaser . '" ' . ($headerData['rh_purchaser'] == $purchaser ? "selected" : "") . '>' . $purchaser . '</option>';
                }
                ?>
            </select>
          </div>
        </div>

      <!-- subtotal -->
        <?php
          $subtotal = floatval($headerData['rh_reqtotal']);
          $subtotal -= is_numeric($headerData['rh_tax']) ? $headerData['rh_tax'] : 0.00;
          $subtotal -= is_numeric($headerData['rh_shippingamount']) ? $headerData['rh_shippingamount'] : 0.00;
        ?>
        <div class="form-group">
          <label for="rh-reqtotal" class="col-sm-2 control-label">Sub Total</label>
          <div class="col-sm-4">
            <p class="form-control-static" id="rh-reqtotal">$<?php echo number_format($subtotal, 2, '.', ','); ?></p>
            <input type="hidden" name="rh-reqtotal" value="<?php echo $subtotal; ?>" >
          </div>
        </div>

      <!-- rh-tax -->
      <div class="form-group">
        <label for="rh-tax" class="col-sm-2 control-label">Tax</label>
        <div class="col-sm-4">
          <div class="input-group">
            <div class="input-group-addon"><span class="glyphicon glyphicon-usd" aria-hidden="true"></span></div>
            <input
              class="form-control input-sm"
              data-jval-min-num="0"
              id="rh-tax"
              name="rh-tax"
              placeholder="Optional"
              type="text"
              value="<?php echo $headerData['rh_tax']; ?>"
              <?php echo $closed ? 'disabled' : ''; ?>
            >
          </div>
        </div>
      </div>

      <!-- rh-shippingamount -->
        <div class="form-group">
          <label for="rh-shippingamount" class="col-sm-2 control-label">Shipping Amount</label>
          <div class="col-sm-4">
            <div class="input-group">
              <div class="input-group-addon"><span class="glyphicon glyphicon-usd" aria-hidden="true"></span></div>
              <input
                type="text"
                class="form-control input-sm"
                id="rh-shippingamount"
                name="rh-shippingamount"
                data-jval-min-num="0"
                placeholder="Optional"
                value="<?php echo $headerData['rh_shippingamount']; ?>"
                <?php echo $closed ? 'disabled' : ''; ?>
              >
            </div>
          </div>
        </div>

      <!-- rh-shippingmethod -->
        <div class="form-group">
          <label for="rh-shippingmethod" class="col-sm-2 control-label">Shipping Method</label>
          <div class="col-sm-4">
            <select name="rh-shippingmethod" id="rh-shippingmethod" class="form-control" <?php echo $closed ? 'disabled' : ''; ?>>
              <option value="" <?php echo $headerData['rh_shippingmethod'] == "" ? "selected" : ""; ?> >Select Shipping Method</option>
              <option value="FOB Collect" <?php echo $headerData['rh_shippingmethod'] == "FOB Collect" ? "selected" : ""; ?> >FOB Collect</option>
              <option value="Free" <?php echo $headerData['rh_shippingmethod'] == "Free" ? "selected" : ""; ?> >Free</option>
              <option value="PPD (Pre-Paid)" <?php echo $headerData['rh_shippingmethod'] == "PPD (Pre-Paid)" ? "selected" : ""; ?> >PPD (Pre-Paid)</option>
            </select>
          </div>
        </div>

      <!-- rh-reqtotal -->
        <div class="form-group">
          <label for="rh-reqtotal" class="col-sm-2 control-label">Grand Total</label>
          <div class="col-sm-4">
            <p class="form-control-static" id="rh-reqtotal">$<?php echo number_format($headerData['rh_reqtotal'], 2, '.', ','); ?></p>
            <input type="hidden" name="rh-reqtotal" value="<?php echo $headerData['rh_reqtotal']; ?>" >
          </div>
        </div>

      <!-- rh-notes -->
        <div class="form-group">
          <label for="rh-notes" class="col-sm-2 control-label">Notes</label>
          <div class="col-sm-6">
            <textarea class="form-control input-sm" name="rh-notes" id="rh-notes" rows="5" placeholder="Optional" <?php echo $closed ? 'disabled' : ''; ?>><?php echo $headerData['rh_notes']; ?></textarea>
          </div>
        </div>

      <!-- rh-justification -->
        <div class="form-group">
          <label for="rh-justification" class="col-sm-2 control-label">Justification</label>
          <div class="col-sm-6">
            <input
              value="<?php echo $headerData['rh_justification']; ?>"
              type="text"
              name="rh-justification"
              id="rh-justification"
              data-jval-required data-jval-min="3"
              data-jval-max="255"
              class="form-control input-sm"
              autocomplete="off"
              <?php echo $closed ? 'disabled' : ''; ?>
            >
          </div>
        </div>

      <!-- rh-status -->
        <?php $reqStatus = GetSettings('status'); ?>
        <div class="form-group">
          <label for="rh-status" class="col-sm-2 control-label">Status</label>
          <div class="col-sm-4">
            <select name="rh-status" id="rh-status" class="form-control" <?php echo $closed ? 'disabled' : ''; ?>>
              <?php
              foreach ($reqStatus as $key => $val) {
                echo "<option value=\"$val\"", strtolower($headerData['rh_status']) == strtolower($val) ? "selected" : "", ">$val</option>";
              }
              ?>
            </select>
          </div>
        </div>

        <?php if (preg_match('/^20\d\d-\d{1,2}-\d{1,2} \d\d:\d\d/', $headerData['rh_ordered_datetime'])) : ?>
      <!-- rh-ordered-datetime -->
        <div class="form-group">
          <label for="rh-ordered-datetime-label" class="col-sm-2 control-label">Ordered Date/Time</label>
          <div class="col-sm-4">
            <p class="form-control-static" id="rh-ordered-datetime-label">
                <?php echo $headerData['rh_ordered_datetime']; ?>
            </p>
            <input
              type="hidden"
              id="rh-ordered-datetime"
              name="rh-ordered-datetime"
              value="<?php echo $headerData['rh_ordered_datetime']; ?>">
          </div>
        </div>
        <?php endif; ?>


      <!-- rh-paymenttype -->
      <div class="form-group">
        <label for="rh-paymenttype" class="col-sm-2 control-label">Payment Type</label>
        <div class="col-sm-4">
          <select name="rh-paymenttype" id="rh-paymenttype" class="form-control" <?php echo $closed ? 'disabled' : ''; ?>>
            <option value="" <?php echo $headerData['rh_paymenttype'] == "" ? "selected" : ""; ?> >Choose Type</option>
            <option value="CC" <?php echo $headerData['rh_paymenttype'] == "CC" ? "selected" : ""; ?> >CC</option>
            <option value="Open Terms" <?php echo $headerData['rh_paymenttype'] == "Open Terms" ? "selected" : ""; ?> >Open Terms</option>
          </select>
        </div>
      </div>

      <!-- rh-payee -->
      <div class="form-group">
        <label for="rh-payee" class="col-sm-2 control-label">Payee</label>
        <div class="col-sm-4">
          <input type="text" class="form-control input-sm" id="rh-payee" name="rh-payee" autocomplete="off" readonly  value="<?php echo $headerData['rh_payee']; ?>" <?php echo $closed ? 'disabled' : ''; ?>>
        </div>
      </div>


      <!-- buttons -->
        <div class="form-group form-group-sm">
          <div class="col-sm-4 col-sm-offset-2">

            <!-- <button type="submit" id="button-submit" class="btn btn-sm btn-primary jval-submit btn-spc-right" <?php echo $closed ? 'disabled' : ''; ?>>Update</button> -->

            <!-- <a class="btn btn-sm btn-default btn-spc-right<?php echo $closed ? ' disabled' : ''; ?>" href="index.php" role="button" <?php echo $closed ? 'disabled' : ''; ?>>Cancel</a> -->

            <a class="btn btn-sm btn-primary btn-spc-right" href="header.php?copy=<?php echo $id; ?>" role="button">Copy</a>
            <a class="btn btn-sm btn-success btn-spc-right" href="header.php?reopen=<?php echo $id; ?>" role="button">Re-Open</a>
            <!-- <a class="btn btn-sm btn-info btn-spc-right<?php echo $closed ? ' disabled' : ''; ?>" href="header.php?copy=<?php echo $id; ?>" role="button" <?php echo $closed ? 'disabled' : ''; ?>>Copy</a> -->
            <?php
            // if (strtolower($headerData['rh_deptapproval']) == 'y') {
            //   $deptApproved = true;
            // } else {
            //   $deptApproved = false;
            // }

            // if ($deptApproved && !$closed) :
            ?>
            <!-- <a class="btn btn-sm btn-warning" href="header.php?id=<?php echo $id; ?>&resend=true" role="button">Re-Send Email</a> -->
            <?php
            // endif;
            ?>
          </div>
          <div class="col-sm-2 col-sm-offset-1">
            <button type="button" id="button-delete" class="btn btn-sm btn-danger">Delete</button>
          </div>
        </div>
      </form>
    <!-- form end -->
  </div>

<!-- Details -->
    <?php
      $detailsData = GetDetails($id);
    if ($detailsData != false) :
      $oneLeft = count($detailsData) == 1 ? true : false;
    ?>
    <!-- DEBUG MODE -->
    <?php if (isset($_COOKIE['debug']) && $_COOKIE['debug'] === "1") : ?>
      <div class="row">
        <div class="col-sm-8 col-sm-offset-1">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">DEBUG</h3>
            </div>
            <div class="panel-body">
              <pre>$detailsData = <?php print_r($detailsData) ?></pre>
            </div>
          </div>
        </div>
      </div>
    <?php endif ?>

    <!-- Table -->
    <div id="table-container">
    <div class="row ">
      <div class="col-sm-1 col-sm-offset-0">
        <div class="details-header">
          <h3>Details</h3>
        </div>
      </div>
    </div>
    <!-- <div class="row ">
      <div class="col-sm-1 col-sm-offset-0">
        <div style="margin-top:5px;margin-bottom:3px;">
        <a class="btn btn-success btn-sm<?php echo $closed ? ' disabled' : ''; ?>" href="header-add-detail.php?id=<?php echo $headerData['rh_id']; ?>" role="button" <?php echo $closed ? 'disabled' : ''; ?>>Add Detail</a>
        </div>
      </div>
    </div> -->
    <hr>
    <table id="maintable" class="table table-striped table-hover table-condensed">
      <thead>
        <tr>
          <th>Id</th>
          <th>Item Status</th>
          <th>Date Added</th>
          <th>Part Number</th>
          <th>Item Name</th>
          <th>Item Description</th>
          <th>Item Link</th>
          <th>Units</th>
          <th>Quantity</th>
          <th>Unit Price</th>
          <th>Extended</th>
          <th>Notes</th>
          <th>Date Recieved</th>
          <th>Quantity Recieved</th>
        </tr>
      </thead>
      <tbody>
        <?php
        foreach ($detailsData as $row) {
            echo "<tr>";
            // Id, links to edit detail
            // echo "<td>", str_replace("{{id}}", $row['rl_id'], "<a href=\"details.php?id={{id}}" . ($closed ? '&closed=true' : '') . "\">{{id}}</a>"), "</td>";
            $oneLeft = $oneLeft ? '&last=true' : '';
            echo "<td><a href=\"closed-details.php?id=" . $row['rl_id'] . ($closed ? '&closed=true' : '') . $oneLeft . "\">" . $row['rl_id'] . "</a></td>";
            echo "<td>" . $row['rl_itemstatus']. "</td>";

            // Date added
            $matches;
            preg_match("/^20\d\d-\d{1,2}-\d{1,2}/", $row['rl_dateadded'], $matches);
            echo "<td>" . (isset($matches[0]) ? $matches[0] : "") . "</td>";

            echo "<td>" . $row['rl_partnumber']. "</td>";
            echo "<td>" . $row['rl_itemname']. "</td>";
            echo "<td>" . $row['rl_itemdesc']. "</td>";
            // Item link
            echo "<td>    <a href=\"" . $row['rl_itemlink'] . "\" target=\"_blank\">" . ($row['rl_itemlink'] ? "LINK" : "") . "</a>    </td>";
            echo "<td>" . $row['rl_units']. "</td>";
            echo "<td>" . $row['rl_quantity']. "</td>";

            // Unit price
            echo "<td>\$" . number_format(round($row['rl_unitprice'], 2), 2) . "</td>";

            // Extended price
            echo "<td>\$" . number_format(round($row['rl_extended'], 2), 2) . "</td>";

            echo "<td>" . $row['rl_notes']. "</td>";

            // date received
            $matches = null;
            preg_match("/^20\d\d-\d{1,2}-\d{1,2}/", $row['rl_datereceived'], $matches);
            echo "<td>" . (isset($matches[0]) ? $matches[0] : "") . "</td>";

            echo "<td>" . $row['rl_qtyreceived']. "</td>";
            echo "</tr>";
        }
        ?>
          </tbody>
        </table>
      </div>
      <!-- Totals -->
      <div class="container">

        <div class="row">
          <div class="col-xs-2">
          <p class="lead text-right">
            Sub Total:
          </p>
          </div>
          <div class="col-xs-1">
          <p class="lead text-right">
          $<?php echo number_format($subtotal, 2, '.', ','); ?>
            </p>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-2">
            <p class="lead text-right">
            Tax:
            </p>
          </div>
          <div class="col-xs-1">
            <p class="lead text-right">
          $<?php echo number_format($headerData['rh_tax'], 2, '.', ','); ?>
            </p>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-2">
            <p class="lead text-right">
            Shipping:
            </p>
          </div>
          <div class="col-xs-1">
            <p class="lead text-right">
          $<?php echo number_format($headerData['rh_shippingamount'], 2, '.', ','); ?>
            </p>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-2">
            <p class="lead text-right">
            Grand Total:
            </p>
          </div>
          <div class="col-xs-1">
            <p class="lead text-right">
          $<?php echo number_format($headerData['rh_reqtotal'], 2, '.', ','); ?>
            </p>
          </div>
        </div>
      </div>
      <?php
    endif;
      ?>
<!-- End Details -->

<!-- Modal Delete -->
  <div id="modal-delete" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">You are about to delete this record!</h4>
        </div>

        <div class="modal-body">
          <p class="">
            Are you sure you want to delete this record?
          </p>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <a href="index.php?delete=<?php echo $id; ?>" class="btn btn-danger" role="button">Delete</a>
        </div>

      </div>
    </div>
  </div>
<!-- End Modal Delete -->

<!-- jquery -->
  <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<!-- bootstrap -->
  <script src="js/bootstrap.min.js"></script>
<!-- datatables
  <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.js"></script> -->
  <script type="text/javascript" src="DataTables/datatables.min.js"></script>
<!-- daterangepicker -->
  <script src="js/moment.js"></script>
  <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<!-- jval -->
  <script src="http://sw:8082/prod/jval-validator/jval.js"></script>
<!-- inline -->
  <script>
    // datepicker
    $('.date-picker').daterangepicker({
      autoUpdateInput: false,
      autoApply: true,
      locale:{
        format:'YYYY-MM-DD',
        cancelLabel: 'Clear'
      },
      singleDatePicker: true,
      showDropdowns: true
    });
    $('.date-picker').on('apply.daterangepicker', function(ev, picker) {
      console.log('apply');
      $(this).val(picker.startDate.format('YYYY-MM-DD'));
    });
    $('.date-picker').on('cancel.daterangepicker', function(ev, picker) {
      console.log('cancel');
      $(this).val('');
    });

    // document ready
    $(document).ready(function() {
      // datatable
      $('#maintable').DataTable({
        "pageLength": 100,
        stateSave: true,
        "stateDuration": 60*60*24*180,
        "processing": true,
        "language":{
          "loadingRecords": "Loading, please wait...",
          "processing": "Working, please wait..."
        },
        fixedHeader: {
          header: true,
          headerOffset: 50,
          footer: true
        }
      });

      // format request total w/ 2 decimals
      var rhReqtotal = $('#rh-reqtotal');
      if (rhReqtotal.val().length) {
        rhReqtotal.val( Number( rhReqtotal.val() ).toFixed(2) );
      }
      rhReqtotal.on('change', function() {
        if (rhReqtotal.val().length) {
          rhReqtotal.val( Number( rhReqtotal.val() ).toFixed(2) );
        }
      });
    });

    // Modal Delete
    $('#button-delete').click(function () {
      $('#modal-delete').modal('show');
    });

    // Payment Type - Initialize
    if ($('#rh-paymenttype').val() == 'CC') {
      $('#rh-payee').attr('readonly', null);
      $('#rh-payee').attr('placeholder', 'Required');
      $('#rh-payee').attr('data-jval-required', 'true');
    }

    // Payment Type - onChange
    $('#rh-paymenttype').on('change',function(){
      if ($('#rh-paymenttype').val() == 'CC') {
        $('#rh-payee').attr('readonly', null);
        $('#rh-payee').attr('placeholder', 'Required');
        $('#rh-payee').attr('data-jval-required', 'true');
      } else {
        $('#rh-payee').attr('placeholder', null);
        $('#rh-payee').attr('data-jval-required', null);
        $('#rh-payee').attr('readonly', 'true');
      }
    });
  </script>
</body>
</html>
