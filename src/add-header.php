<?php
require_once('init.php');
/**
 * Start session
 */
// if (session_status() !== PHP_SESSION_ACTIVE) {
//     session_start();
// }

/**
 * This is a new requisition so lets remove any previous requisition details
 */
unset($_SESSION['header']);
unset($_SESSION['details']);

// require_once('init.php');

$currentDept; // Holds the department name of the current user
$departments; // Holds a list of distinct departments

// $currentDept
if ($userDept = GetUserDept($currentUser)) {
    $currentDept = $userDept;
} else {
    $currentDept = "unknown";
}

if (!$departments = GetDepartments()) {
    $departments= null;
    $departments[] = $currentDept;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Requisition</title>
  <!-- bootstrap styles -->
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/bootstrap-theme.min.css">
  <!-- datatable styles
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.css"> -->
  <link rel="stylesheet" type="text/css" href="DataTables/datatables.min.css">
  <!-- daterangepicker styles -->
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
    body {
      padding-top: 70px;
      padding-bottom: 250px;
    }

    p.navbar-right {
      padding-right: 25px;
    }
  </style>
</head>

<body>
  <!-- Alert Still Testing -->
    <!-- <div class="container">
      <div class="row">
        <div class="col-xs-8 col-xs-offset-1">
          <div class="alert alert-warning alert-dismissable" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>Warning:</strong> This system is still being tested and updated. You may occasionally find typos or encounter errors. If you do, please let <a href="mailto:jeffrabin@cousin.com?subject=Requisition%20Problem">Jeff</a> know. Thanks.
          </div>
        </div>
      </div>
    </div> -->
  <!-- Alert Still Testing -->

  <!-- Nav start -->
  <nav class="navbar navbar-fixed-top <?php echo $env === "prod"?"navbar-default":"navbar-inverse"; ?>">
    <div class="container-fluid">

      <div class="navbar-header">
        <a href="index.php" class="navbar-brand">Requisition</a>
      </div>

      <ul class="nav navbar-nav">
        <li class="">
          <a href="index.php">Current</a>
        </li>
        <li class="">
          <a href="closed-reqs.php">History</a>
        </li>
        <li class="active">
          <a href="add-header.php">Add</a>
        </li>
      </ul>

      <p class="navbar-text navbar-right">
        <?php echo ($env == "prod") ? "Production" : "Development"; ?> Environment
      </p>

    </div>
  </nav>
  <!-- Nav end -->

  <div class="container">


    <div class="row">
      <div class="col-sm-8 col-sm-offset-1">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Requisition Details</h3>
          </div>
          <div class="panel-body">
            <p>Please enter the main details of your request and click Continue. You'll be given the chance to enter the item details on the next page.</p>
          </div>
        </div>
      </div>
    </div>


    <form action="add-detail.php" method="POST" id="form-requisition" class="form-horizontal">
      <!-- next page checks existance of this field to know if form was submitted -->
      <input type="hidden" name="form-submitted" id="form-submitted" value="header">

      <!-- rh_daterequested -->
      <div class="form-group">
        <label for="rh-daterequested" class="col-sm-2 control-label">Deliver By</label>
        <div class="col-sm-4">
          <div class="input-group">
            <div class="input-group-addon"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></div>
            <input type="text" class="form-control input-sm date-picker" id="rh-daterequested" autocomplete="off" name="rh-daterequested" placeholder="Optional">
          </div>
        </div>
      </div>

      <!--rh-requestedfor
        Requested For   -->
      <div class="form-group">
        <label for="rh-requestedfor" class="col-sm-2 control-label">Requested For</label>
        <div class="col-sm-4">
          <input type="text" class="form-control input-sm" id="rh-requestedfor" name="rh-requestedfor" placeholder="Optional" autocomplete="off">
        </div>
      </div>

      <!--rh-reqdept
        Requesting Department   -->
      <div class="form-group">
        <label for="rh-reqdept" class="col-sm-2 control-label">Requesting Department</label>
        <div class="col-sm-4">
          <!--<input type="text" class="form-control input-sm" id="rh-reqdept" name="rh-reqdept" data-jval-required data-jval-min="2" autocomplete="off" value="<?php echo $currentDept; ?>" >-->
          <select class="form-control input-sm" id="rh-reqdept" name="rh-reqdept">
            <?php
            foreach ($departments as $idx => $department) {
                echo "<option value=\"" . ucwords($department) . "\"" . (strtolower($currentDept) == strtolower($department)?'selected':'') . ">" . ucwords($department) . "</option>";
            }
            ?>
          </select>
        </div>
      </div>

      <!-- rh-suppliername -->
      <div class="form-group">
        <label for="rh-suppliername" class="col-sm-2 control-label">Supplier Name</label>
        <div class="col-sm-4">
          <input type="text" class="form-control input-sm" id="rh-suppliername" name="rh-suppliername" data-jval-required data-jval-min="3" autocomplete="off">
        </div>
      </div>

      <!-- rh-supplieraddress -->
      <div class="form-group">
        <label for="rh-supplieraddress" class="col-sm-2 control-label">Supplier Address</label>
        <div class="col-sm-4">
          <textarea class="form-control input-sm" name="rh-supplieraddress" id="rh-supplieraddress" rows="5" placeholder="Optional"></textarea>
        </div>
      </div>

      <!-- rh-supplierphone -->
      <div class="form-group">
        <label for="rh-supplierphone" class="col-sm-2 control-label">Supplier Phone</label>
        <div class="col-sm-4">
          <div class="input-group">
            <div class="input-group-addon"><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span></div>
            <input type="text" class="form-control input-sm" id="rh-supplierphone" name="rh-supplierphone" placeholder="Optional" autocomplete="off">
          </div>
        </div>
      </div>

      <!-- rh-supplieremail -->
      <div class="form-group">
        <label for="rh-supplieremail" class="col-sm-2 control-label">Supplier Email</label>
        <div class="col-sm-4">
          <div class="input-group">
            <div class="input-group-addon"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span></div>
            <input type="text" class="form-control input-sm" id="rh-supplieremail" name="rh-supplieremail" placeholder="Optional" data-jval-email autocomplete="off">
          </div>
        </div>
      </div>

      <!-- rh-suppliercontact -->
      <div class="form-group">
        <label for="rh-suppliercontact" class="col-sm-2 control-label">Supplier Contact</label>
        <div class="col-sm-4">
          <div class="input-group">
            <div class="input-group-addon"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></div>
            <input type="text" class="form-control input-sm" id="rh-suppliercontact" name="rh-suppliercontact" placeholder="Optional" autocomplete="off">
          </div>
        </div>
      </div>

      <!-- rh-shiptoaddr -->
      <div class="form-group">
        <label for="rh-shiptoaddr" class="col-sm-2 control-label">Ship To Address</label>
        <div class="col-sm-4">
          <textarea class="form-control input-sm" name="rh-shiptoaddr" id="rh-shiptoaddr" rows="5" placeholder="Optional if not CCA's address"></textarea>
        </div>
      </div>

      <!-- rh-deptapproval -->
      <div class="form-group">
        <label for="rh-deptapproval" class="col-sm-2 control-label">Dept Approval</label>
        <div class="col-sm-4">
          <select name="rh-deptapproval" id="rh-deptapproval" class="form-control">
            <option value="N" selected>Not Approved</option>
            <option value="Y">Approved</option>
          </select>
        </div>
      </div>

      <!-- rh-mgtapproval -->
      <div class="form-group">
        <label for="rh-mgtapproval" class="col-sm-2 control-label">Mgmt Approval</label>
        <div class="col-sm-4">
          <select name="rh-mgtapproval" id="rh-mgtapproval" class="form-control">
            <option value="N" selected>Not Approved</option>
            <option value="Y">Approved</option>
          </select>
        </div>
      </div>

      <!-- rh-purchaser -->
      <div class="form-group">
        <label for="rh-purchaser" class="col-sm-2 control-label">Purchaser</label>
        <div class="col-sm-4">
          <select name="rh-purchaser" id="rh-purchaser" class="form-control">
            <option value="" selected>Select Purchaser</option>
            <?php
              $purchasers = GetSettings('purchasers');
            foreach ($purchasers as $purchaser => $details) {
                echo '<option value="' . $purchaser . '">' . $purchaser . '</option>';
            }
            ?>
            <!--
              <option value="Aaron M">Aaron M</option>
              <option value="Amanda">Amanda</option>
              <option value="Austin C">Austin C</option>
              <option value="Denny">Denny</option>
              <option value="Develyn">Develyn</option>
              <option value="Jeff R">Jeff R</option>
              <option value="Kelly C">Kelly C</option>
              <option value="Laura B">Laura B</option>
              <option value="Lisa">Lisa</option>
              <option value="Marty">Marty</option>
              <option value="Rick">Rick</option>
              <option value="Russ">Russ</option>
              <option value="Sharon E">Sharon E</option>
            -->
          </select>
        </div>
      </div>

      <!-- rh-tax -->
      <div class="form-group">
        <label for="rh-tax" class="col-sm-2 control-label">Tax Amount</label>
        <div class="col-sm-4">
          <div class="input-group">
            <div class="input-group-addon"><span class="glyphicon glyphicon-usd" aria-hidden="true"></span></div>
            <input type="text" class="form-control input-sm" id="rh-tax" name="rh-tax" data-jval-min-num="0" placeholder="Optional">
          </div>
        </div>
      </div>

      <!-- rh-shippingamount -->
      <div class="form-group">
        <label for="rh-shippingamount" class="col-sm-2 control-label">Freight Amount</label>
        <div class="col-sm-4">
          <div class="input-group">
            <div class="input-group-addon"><span class="glyphicon glyphicon-usd" aria-hidden="true"></span></div>
            <input type="text" class="form-control input-sm" id="rh-shippingamount" name="rh-shippingamount" data-jval-min-num="0" placeholder="Optional">
          </div>
        </div>
      </div>

      <!-- rh-shippingmethod -->
      <div class="form-group">
        <label for="rh-shippingmethod" class="col-sm-2 control-label">Freight method</label>
        <div class="col-sm-4">
          <select name="rh-shippingmethod" id="rh-shippingmethod" class="form-control">
            <option value="" selected>Select Freight Method</option>
            <option value="FOB Collect">FOB Collect</option>
            <option value="Free">Free</option>
            <option value="PPD (Pre-Paid)">PPD (Pre-Paid)</option>
          </select>
        </div>
      </div>

      <!-- rh-notes -->
      <div class="form-group">
        <label for="rh-notes" class="col-sm-2 control-label">Notes</label>
        <div class="col-sm-6">
          <textarea class="form-control input-sm" name="rh-notes" id="rh-notes" rows="5" placeholder="Optional"></textarea>
        </div>
      </div>

      <!-- rh-justification -->
      <div class="form-group">
        <label for="rh-justification" class="col-sm-2 control-label">Justification</label>
        <div class="col-sm-6">
          <input type="text" class="form-control input-sm" id="rh-justification" name="rh-justification" data-jval-required autocomplete="off">
        </div>
      </div>

      <!-- rh-status -->
      <?php $reqStatus = GetSettings('status'); ?>
      <div class="form-group">
        <label for="rh-status" class="col-sm-2 control-label">Status</label>
        <div class="col-sm-4">
          <select name="rh-status" id="rh-status" class="form-control">
            <option value="Requested" selected>Requested</option>
            <?php
            foreach ($reqStatus as $key => $val) {
              echo "<option value=\"$val\">$val</option>";
            }
            ?>
          </select>
        </div>
      </div>

      <!-- rh-paymenttype -->
      <div class="form-group">
        <label for="rh-paymenttype" class="col-sm-2 control-label">Payment Type</label>
        <div class="col-sm-4">
          <select name="rh-paymenttype" id="rh-paymenttype" class="form-control">
            <option value="" selected>Choose Type</option>
            <option value="CC">CC</option>
            <option value="Open Terms">Open Terms</option>
          </select>
        </div>
      </div>

      <!-- rh-payee -->
      <div class="form-group">
        <label for="rh-payee" class="col-sm-2 control-label">Payee</label>
        <div class="col-sm-4">
          <input type="text" class="form-control input-sm" id="rh-payee" name="rh-payee" autocomplete="off" readonly>
        </div>
      </div>

      <!-- buttons -->
      <div class="form-group">
        <div class="col-sm-1 col-sm-offset-2">
          <button type="submit" id="button-submit" class="btn btn-primary btn-sm jval-submit">Continue</button>
        </div>
        <div class="col-sm-1 col-sm-offset-0">
          <button type="button" id="button-cancel" class="btn btn-default btn-sm button-close">Cancel</button>
        </div>
      </div>

    </form>
  </div>

  <!-- jquery -->
  <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
  <!-- bootstrap -->
  <script src="js/bootstrap.min.js"></script>
  <!-- datatables
  <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.js"></script> -->
  <script type="text/javascript" charset="utf8" src="DataTables/datatables.min.js"></script>
  <!-- daterangepicker -->
  <script src="js/moment.js"></script>
  <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
  <!-- jval -->
  <script src="http://sw:8082/prod/jval-validator/jval.js"></script>
  <!-- inline -->
  <script>
    // Date Picker
    $('.date-picker').daterangepicker({
      autoUpdateInput: false,
      autoApply: true,
      locale:{
        format:'YYYY-MM-DD',
        cancelLabel: 'Clear'
      },
      singleDatePicker: true,
      showDropdowns: true
    });
    $('.date-picker').on('apply.daterangepicker', function(ev, picker) {
      console.log('apply');
      $(this).val(picker.startDate.format('YYYY-MM-DD'));
    });
    $('.date-picker').on('cancel.daterangepicker', function(ev, picker) {
      console.log('cancel');
      $(this).val('');
    });

    // Payment Type
    $('#rh-paymenttype').on('change',function(){
      if ($('#rh-paymenttype').val() == 'CC') {
        $('#rh-payee').attr('readonly', null);
        $('#rh-payee').attr('placeholder', 'Required');
        $('#rh-payee').attr('data-jval-required', 'true');
      } else {
        $('#rh-payee').attr('placeholder', null);
        $('#rh-payee').attr('data-jval-required', null);
        $('#rh-payee').attr('readonly', 'true');
      }
    });
  </script>
</body>

</html>
