<?php
/***********************************
 * Display PHP errors and warnings *
 ***********************************/
ini_set('display_errors', 1);
error_reporting(E_ALL);

/*****************
 * Start Session *
 *****************/
if (session_status() !== PHP_SESSION_ACTIVE) {
  session_start();
}

/**********************
 * Set root directory *
 **********************/
$_SESSION['moduleroot'] = __DIR__;

/***********************************
 * Set debug mode true if ?debug=1 *
 ***********************************/
if (isset($_GET['debug'])) {
  if ($_GET['debug'] === '1') {
    $_SESSION['debug'] = true;
    setcookie('debug', true);
  } else {
    $_SESSION['debug'] = false;
    setcookie('debug', false);
  }
}

/********************************************************************
 * Set environment ($env) to production (prod) or development (dev) *
 ********************************************************************/
$env = "prod";
$_SESSION['env'] = 'prod';
if (isset($_GET['env']) && ($_GET['env'] == "prod" || $_GET['env'] == "dev")) {
  $env = $_GET['env'];
  $_SESSION['env'] = $_GET['env'];
  setcookie("env", $env, time() + 7776000, "/");
} else {
  if (isset($_COOKIE['env'])) {
    if ($_COOKIE['env'] == "prod") {
      $env = "prod";
      $_SESSION['env'] = 'prod';
    } else {
      $env = "dev";
      $_SESSION['env'] = 'dev';
    }
  } else {
    setcookie("env", "prod", time() + 7776000, "/");
  }
}

/******************************************
 * Get/Set user name, redirect if not set *
 ******************************************/
$currentUser;
if (isset($_GET['user']) && !isset($_COOKIE['userName'])) {
  $currentUser = $_GET['user'];
  $_SESSION['currentuser'] = $_GET['user'];
  setcookie("userName", $_GET['user'], time() + 7776000, "/"); // 90 days
} elseif (isset($_COOKIE['userName'])) {
  $currentUser = $_COOKIE['userName'];
  $_SESSION['currentuser'] = $_COOKIE['userName'];
  setcookie("userName", $currentUser, time() + 7776000, "/"); // 90 days
} elseif (!isset($_GET['user']) && !isset($_COOKIE['userName'])) {
  echo '<script>';
  echo 'window.location = "http://cc";';
  echo '</script>';
}

/*************************************
 * Constants for database connection *
 *************************************/
$SELECTED_DB      = $env === "prod" ? "swdb" : "dev";
$PDO_CONNECT_STMT = "sqlsrv:Server=srv-swdb;Database=" . $SELECTED_DB;
$PDO_USER         = "cousinrw";
$PDO_PASSWORD     = "Cousin_789";

/************************
 * Require helper files *
 ************************/
require_once(__DIR__ . '/helpers/email-custom.php');
require_once(__DIR__ . '/helpers/helper.php');
