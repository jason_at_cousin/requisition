<?php
require_once('init.php');

//check if form was submited
$cameFrom;
if (isset($_POST['form-submitted'])) {
  $cameFrom = $_POST['form-submitted'];
  if ($cameFrom === "header") {
    $_SESSION['header'] = $_POST;
  } elseif ($cameFrom === 'detail') {
    $_SESSION['details'][] = $_POST;
  }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Requisition</title>
  <!-- bootstrap styles -->
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/bootstrap-theme.min.css">
  <!-- datatable styles
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.css"> -->
  <link rel="stylesheet" type="text/css" href="DataTables/datatables.min.css">
  <!-- daterangepicker styles -->
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
    body {
      padding-bottom: 250px;
      padding-top: 70px;
    }
    p.navbar-right {
      padding-right: 25px;
    }
    table {
      margin-top: 15px;
    }
  </style>
  <?php echo count($_POST) === 0 ? "<script>window.location = 'add-header.php';</script>" : ""; ?>
</head>

<body>
  <!-- Nav start -->
  <nav class="navbar navbar-fixed-top <?php echo $env === "prod"?"navbar-default":"navbar-inverse"; ?>">
    <div class="container-fluid">

      <div class="navbar-header">
        <a href="index.php" class="navbar-brand">Requisition</a>
      </div>

      <ul class="nav navbar-nav">
        <li class="">
          <a href="index.php">Current</a>
        </li>
        <li class="">
          <a href="closed-reqs.php">History</a>
        </li>
        <li class="active">
          <a href="add-header.php">Add</a>
        </li>
      </ul>

      <p class="navbar-text navbar-right">
        <?php echo ($env == "prod") ? "Production" : "Development"; ?> Environment
      </p>

    </div>
  </nav>
  <!-- Nav end -->



  <!-- DEBUG MODE -->
  <?php if (isset($_COOKIE['debug']) && $_COOKIE['debug'] === "1") : ?>
    <div class="row">
      <div class="col-sm-8 col-sm-offset-1">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">DEBUG</h3>
          </div>
          <div class="panel-body">
            <pre>$_SESSION = <?php print_r($_SESSION) ?></pre>
          </div>
        </div>
      </div>
    </div>
  <?php endif ?>



  <div class="container">

    <?php if (isset($cameFrom) && $cameFrom === "header") : ?>

    <div class="row">
      <div class="col-sm-8 col-sm-offset-1">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Add Items</h3>
          </div>
          <div class="panel-body">
            <p>
              Please enter the details of an item and click Add. You'll be brought
              back to this page giving you the chance to enter more items.
            </p>
          </div>
        </div>
      </div>
    </div>

    <?php elseif (isset($cameFrom) && $cameFrom === "detail") : ?>

    <?php
      $lastIndex = count($_SESSION['details']) - 1;
      // echo "<pre>", $lastIndex;
      // print_r($_SESSION);
      // echo "</pre>";
      // die();
      $lastItem['number'] =   $_SESSION['details'][$lastIndex]['rl-partnumber'];
      $lastItem['name'] =     ucfirst($_SESSION['details'][$lastIndex]['rl-itemname']);
      $lastItem['qty'] =      $_SESSION['details'][$lastIndex]['rl-quantity'];
      $lastItem['price'] =    $_SESSION['details'][$lastIndex]['rl-unitprice'];
      $lastItem['total'] =    $_SESSION['details'][$lastIndex]['rl-extended'];
    ?>
    <div class="row">
      <div class="col-sm-6 col-sm-offset-2">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">
              <?php
                $shortName = strlen($lastItem['name']) > 20
                  ? substr($lastItem['name'], 0, 17) . "..."
                  : $lastItem['name']
                ;
              ?>
              Added: <?php echo $shortName, " (", $lastItem['number'], ") x ", $lastItem['qty'], " @ \$", $lastItem['price'], ": $", $lastItem['total']; ?>
            </h3>
          </div>
          <div class="panel-body">
            <p>You may continue to add additional items. Once you've finished, click the 'Finish' button.</p>
            <a class="btn btn-primary btn-sm" href="add-finish.php?id=815361bc885f43c987a7c40602fab335" role="button">
              Finish
            </a>
            <?php
            if (count($_SESSION['details'])) {
              //echo "<h3>Items Added</h3>";
              echo "<p><table class=\"table table-striped table-condensed\">";
              echo "<tr> <th> Item #</th> <th> Name</th> <th> Qty</th> <th> Price</th> <th> Total</th> </tr>";
              foreach ($_SESSION['details'] as $idx => $item) {
                echo "<tr>";
                echo "<td> ", $item['rl-partnumber'], " </td>";
                echo "<td> ", ucfirst($item['rl-itemname']), " </td>";
                echo "<td> ", $item['rl-quantity'], " </td>";
                echo "<td> \$", $item['rl-unitprice'], " </td>";
                echo "<td> \$", $item['rl-extended'], " </td>";
                echo "</tr>";
              }
              echo "</table></p>";
            }
            ?>
          </div>
        </div>
      </div>
    </div>

    <?php endif ?>




    <form action="#" method="POST" id="form-requisition" class="form-horizontal">
      <!-- next page checks existance of this field to know if form was submitted -->
      <input type="hidden" name="form-submitted" id="form-submitted" value="detail">

      <!-- rl-partnumber -->
      <div class="form-group">
        <label for="rl-partnumber" class="col-sm-2 control-label">Part Number</label>
        <div class="col-sm-2">
          <input type="text" class="form-control input-sm" id="rl-partnumber" name="rl-partnumber" data-jval-required data-jval-max="255" autofocus autocomplete="off">
        </div>
      </div>

      <!-- rl-itemname -->
      <div class="form-group">
        <label for="rl-itemname" class="col-sm-2 control-label">Item Name</label>
        <div class="col-sm-4">
          <input type="text" class="form-control input-sm" id="rl-itemname" name="rl-itemname" data-jval-required data-jval-max="255" autocomplete="off">
        </div>
      </div>

      <!-- rl-itemdescription -->
      <div class="form-group">
        <label for="rl-itemdesc" class="col-sm-2 control-label">Item Description</label>
        <div class="col-sm-6">
          <input type="text" class="form-control input-sm" id="rl-itemdesc" name="rl-itemdesc" placeholder="Optional" data-jval-max="255" autocomplete="off">
        </div>
      </div>

      <!-- rl-itemlink -->
      <div class="form-group">
        <label for="rl-itemlink" class="col-sm-2 control-label">Item Link</label>
        <div class="col-sm-6">
          <input type="text" class="form-control input-sm" id="rl-itemlink" name="rl-itemlink" placeholder="Optional" data-jval-max="2100" autocomplete="off">
        </div>
      </div>

      <!-- rl-units -->
      <div class="form-group">
        <label for="rl-units" class="col-sm-2 control-label">Unit of Measure</label>
        <div class="col-sm-4">
          <input type="text" class="form-control input-sm" id="rl-units" name="rl-units" placeholder="Optional" data-jval-max="255" autocomplete="off">
        </div>
      </div>

      <!-- rl-quantity -->
      <div class="form-group">
        <label for="rl-quantity" class="col-sm-2 control-label">Quantity</label>
        <div class="col-sm-1">
          <input type="text" class="form-control input-sm selectme" id="rl-quantity" name="rl-quantity" data-jval-required data-jval-min-num="1" autocomplete="off">
        </div>
      </div>

      <!-- rl-unitprice -->
      <div class="form-group">
        <label for="rl-unitprice" class="col-sm-2 control-label">Unit Price</label>
        <div class="col-sm-2">
          <input type="text" class="form-control input-sm selectme" id="rl-unitprice" name="rl-unitprice" data-jval-required autocomplete="off">
        </div>
      </div>

      <!-- rl-extended -->
      <div class="form-group">
        <label for="rl-extended" class="col-sm-2 control-label">Total Price</label>
        <div class="col-sm-2">
          <input type="text" class="form-control input-sm" id="rl-extended" name="rl-extended" readonly>
        </div>
      </div>

      <!-- rl-notes -->
      <div class="form-group">
        <label for="rl-notes" class="col-sm-2 control-label">Notes</label>
        <div class="col-sm-6">
          <textarea class="form-control input-sm" name="rl-notes" id="rl-notes" rows="5" data-jval-max="2000" placeholder="Optional"></textarea>
        </div>
      </div>

      <!-- rl-datereceived -->
      <div class="form-group">
        <label for="rl-datereceived" class="col-sm-2 control-label">Date Received</label>
        <div class="col-sm-4">
          <input type="text" class="form-control input-sm date-picker" autocomplete="off" id="rl-datereceived" name="rl-datereceived" placeholder="Optional">
        </div>
      </div>

      <!-- rl-qtyreceived -->
      <div class="form-group">
        <label for="rl-qtyreceived" class="col-sm-2 control-label">Qty Received</label>
        <div class="col-sm-1">
          <input type="text" class="form-control input-sm" id="rl-qtyreceived" name="rl-qtyreceived" placeholder="Optional" autocomplete="off">
        </div>
      </div>

      <!-- rl-itemstatus -->
      <div class="form-group">
        <label for="rl-itemstatus" class="col-sm-2 control-label">Item Status</label>
        <div class="col-sm-4">
          <select name="rl-itemstatus" id="rl-itemstatus" class="form-control">
            <option value="To Be Ordered" selected>To Be Ordered</option>
            <option value="Ordered">Ordered</option>
            <option value="Received">Received</option>
            <option value="Broken">Broken</option>
            <option value="Returned">Returned</option>
            <option value="Cancelled">Cancelled</option>
            <option value="Partial">Partial</option>
          </select>
        </div>
      </div>

      <!-- buttons -->
      <div class="form-group">
        <div class="col-sm-1 col-sm-offset-2">
          <button type="submit" id="button-submit" class="btn btn-primary btn-sm jval-submit">Add</button>
        </div>
        <div class="col-sm-1 col-sm-offset-0">
          <a class="btn btn-sm btn-default" href="index.php" role="button">Cancel</a>
          <!--<button type="button" id="button-cancel" class="btn button-close">Cancel</button>-->
        </div>
      </div>

    </form>
  </div>
  <!-- jquery -->
  <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
  <!-- bootstrap -->
  <script src="js/bootstrap.min.js"></script>
  <!-- datatables -->
  <script type="text/javascript" charset="utf8" src="DataTables/datatables.min.js"></script>
  <!-- daterangepicker -->
  <script src="js/moment.js"></script>
  <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
  <!-- jval -->
  <script src="http://sw:8082/prod/jval-validator/jval.js"></script>
  <!-- inline -->
  <script>
    // Section: datepicker
    $('.date-picker').daterangepicker({
      autoUpdateInput: false,
      autoApply: true,
      locale:{
        format:'YYYY-MM-DD',
        cancelLabel: 'Clear'
      },
      singleDatePicker: true,
      showDropdowns: true
    });
    $('.date-picker').on('apply.daterangepicker', function(ev, picker) {
      console.log('apply');
      $(this).val(picker.startDate.format('YYYY-MM-DD'));
    });
    $('.date-picker').on('cancel.daterangepicker', function(ev, picker) {
      console.log('cancel');
      $(this).val('');
    });
    // Section: document.ready
    $(document).ready(function(){
      // assign elements
      var frmQty = $('#rl-quantity');
      var frmPrice = $('#rl-unitprice');
      var frmExtendedPrice = $('#rl-extended');

      // Select the text on click
      var selectMe = $('.selectme');
      selectMe.on('click', function(){
        this.select();
      });

      // Function: getTotal
      function getTotal(){
        // get values if set else 0
        var qty = frmQty.val() > 0 ? frmQty.val() : 0;
        var price = frmPrice.val() > 0 ? frmPrice.val() : 0;
        // set extended price
        frmExtendedPrice.val( (Math.round( ( (qty * price) * 100) ) / 100).toFixed(2) );
      }
      frmQty.on('input', getTotal);
      frmPrice.on('input', getTotal);
      frmPrice.on('change', function(){
        // if frmPrice value is number and > 0 then format with 2 decimal otherwise make blank
        if(Number(frmPrice.val()) !== NaN && Number(frmPrice.val()) > 0){
          frmPrice.val( Number( frmPrice.val() ).toFixed(2) );
        }else{
          frmPrice.val("");
        }
      });
      getTotal();
      //frmPrice.val( Number( frmPrice.val() ).toFixed(2) );
    });
  </script>
</body>

</html>
