<?php

/**
 * @author Jason Harding
 */

namespace jason;


class SendMail
{
  private $connection = null;
  private $lastError = '';
  private $params = array(':to' => '', ':cc' => '', ':bcc' => '', ':subject' => '', ':bodytext' => '', ':bodyhtml' => '');
  private $password = null;
  private $server = null;
  private $user = null;


  /**
   * Class Constructor
   *
   * @constructor
   */
  function __construct(string $user = null, string $password = null, string $server = null)
  {
    // user and password
    if ($user !== null && $password !== null) {
      $this->user = $user;
      $this->password = $password;
    }
    // server
    if ($server !== null) {
      $this->server = $server;
    }
    // connect to db
    $this->connect();
  }


  /**
   * Class Destructor
   *
   * @destructor
   */
  function __destruct()
  {
    $this->connection = null;
  }


  /**
   * Connects $this to the db
   *
   * @method connect()
   * @throws Exception
   */
  private function connect()
  {
    $statement = 'sqlsrv:Server=srv-swdb;Database=swdb';
    $user = 'cousinrw';
    $password = 'Cousin_789';
    // PDO Connection
    try {
      $this->connection = new \PDO($statement, $user, $password);
    } catch (PDOException $e) {
      $this->connection = null;
      $this->lastError = $e->getMessage();
      throw new Exception(__FILE__ . " " . __class__ . " " . __METHOD__ . ": " . $this->lastError, 1);
    }
  }


  /**
   * Generates SQL
   *
   * @method getQuery()
   * @return string
   */
  private function getQuery()
  {
    // part 1
    $sql1 = "INSERT INTO dbo.email_scheduler
      (
        dbo.email_scheduler.sendto,
        dbo.email_scheduler.cc,
        dbo.email_scheduler.bcc,
        dbo.email_scheduler.subject,
        dbo.email_scheduler.bodytext,
        dbo.email_scheduler.bodyhtml
    ";
    // part 2
    $sql2 = ")
      VALUES
      (
        :to,
        :cc,
        :bcc,
        :subject,
        :bodytext,
        :bodyhtml
    ";
    // if username and password are set use them
    if ($this->user !== null && $this->password !== null) {
      $sql1 .= ", dbo.email_scheduler.username, dbo.email_scheduler.password";
      $sql2 .= ", :user, :password";
      $this->params[':user'] = $this->user;
      $this->params[':password'] = $this->password;
      // if server is set use it
      if ($this->server !== null) {
        $sql1 .= ", dbo.email_scheduler.server";
        $sql2 .= ", :server";
        $this->params[':server'] = $this->server;
      }
    }
    // part 3
    $sql3 = ")";
    // return sql
    return $sql1 . $sql2 . $sql3;
  }


  /**
   * Check if email address is valid
   *
   * @method isValidEmailAddress()
   * @param string $email
   * @return bool
   */
  private static function isValidEmailAddress(string $email)
  {
    // check if valid email address
    if (preg_match("/^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$/", $email)) {
      return true;
    }
    return false;
  }


  /**
   * Add 'to' address
   *
   * @method addTo()
   */
  public function addTo(string $email, string $name = null)
  {
    // check if valid email address
    if (!self::isValidEmailAddress($email)) {
      throw new Exception(__FILE__ . " " . __class__ . " " . __METHOD__ . ": Invalid email address", 1);
    }
    // email@address.com[:full name]
    $emailToAdd = $email . ($name === null ? "" : ":$name");
    // if not the first email insert comma
    $this->params[':to'] .= $this->params[':to'] === "" ? "" : ",";
    // add the email/name combo
    $this->params[':to'] .= $emailToAdd;
  }


  /**
   * Clear the to,cc,bcc,subject and body fields
   *
   * @method clear()
   */
  public function clear()
  {
    $this->params[':to'] = "";
    $this->params[':cc'] = "";
    $this->params[':bcc'] = "";
    $this->params[':subject'] = "";
    $this->params[':bodytext'] = "";
    $this->params[':bodyhtml'] = "";
  }


  /**
   * Add 'cc' address
   *
   * @method addCc()
   */
  public function addCc(string $email, string $name = null)
  {
    // check if valid email address
    if (!self::isValidEmailAddress($email)) {
      throw new Exception(__FILE__ . " " . __class__ . " " . __METHOD__ . ": Invalid email address", 1);
    }
    // email@address.com[:full name]
    $emailToAdd = $email . ($name === null ? "" : ":$name");
    // if not the first email insert comma
    $this->params[':cc'] .= $this->params[':cc'] === "" ? "" : ",";
    // add the email/name combo
    $this->params[':cc'] .= $emailToAdd;
  }


  /**
   * Set the html body
   *
   * @method setBodyHtml()
   * @param string $html
   */
  public function setBodyHtml(string $html)
  {
    $this->params[':bodyhtml'] = $html;
  }


  /**
   * Add 'bcc' address
   *
   * @method addBcc()
   */
  public function addBcc(string $email)
  {
    // check if valid email address
    if (!self::isValidEmailAddress($email)) {
      throw new Exception(__FILE__ . " " . __class__ . " " . __METHOD__ . ": Invalid email address", 1);
    }
    // if not the first email insert comma
    $this->params[':bcc'] .= $this->params[':bcc'] === "" ? "" : ",";
    // add the email/name combo
    $this->params[':bcc'] .= $email;
  }


  /**
   * Set the subject of the email
   *
   * @method setSubject()
   * @param string $subject
   */
  public function setSubject(string $subject)
  {
    $this->params[':subject'] = $subject;
  }


  /**
   * Set the plain text body
   *
   * @method setBodyText($body)
   * @param string $text
   */
  public function setBodyText(string $text)
  {
    $this->params[':bodytext'] = $text;
  }


  /**
   * Send the email
   *
   * @return bool true on success
   */
  public function sendEmail()
  {
    if ($this->params[':to'] === "" ||
      $this->params[':subject'] === "" || ($this->params[':bodyhtml'] === "" &&
      $this->params[':bodytext'] === "")) {
      throw new Exception(__FILE__ . " " . __class__ . " " . __METHOD__ . ": The To, Subject and either plaintext or html body need to be set at minimum", 1);
    }
    // PDO Statement
    $sqlStatement = $this->connection->prepare($this->getQuery());
    // PDO Execute Statement
    if (!$sqlStatement->execute($this->params)) {
      $pdoErrors = $sqlStatement->errorInfo();
      throw new Exception(__FILE__ . " " . __class__ . " " . __METHOD__ . ": $pdoErrors[3]", 1);
      $pdoConn = $sqlStatement = null;
      die();
    }
    // PDO Affected Rows
    $affectedRows = $sqlStatement->rowCount();
    $sqlStatement = null;
    if ($affectedRows > 0) {
      return true;
    }
    return false;
  }

/*
   **CLASS**
  SendMail($user, $password, $server)

  addTo ($email, string $name)
  addCc ($email, string $name)
  addBcc ($email)

  setSubject ($subject)

  setBodyText ($text)
  setBodyHtml ($html)

  clear ()
  sendEmail ()
   */
}