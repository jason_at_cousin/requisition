<?php
require_once('init.php');

//check how we arrived here
$stay = false;
$success;
if (isset($_GET['id']) && $_GET['id'] === "815361bc885f43c987a7c40602fab335") {
    $stay = true;
    $success = AddEntry($_SESSION);
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Requisition</title>
    <!-- bootstrap styles -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <!-- datatable styles
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.css"> -->
    <link rel="stylesheet" type="text/css" href="DataTables/datatables.min.css">
    <!-- daterangepicker styles -->
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        body {
            padding-top: 70px;
            padding-bottom: 250px;
        }

        p.navbar-right {
            padding-right: 25px;
        }
    </style>
    <?php echo $stay ? "" : "<script>window.location = 'add-header.php';</script>"; ?>
</head>

<body>
    <!-- Nav start -->
    <nav class="navbar navbar-fixed-top <?php echo $env === "prod"?"navbar-default":"navbar-inverse"; ?>">
        <div class="container-fluid">

            <div class="navbar-header">
                <a href="index.php" class="navbar-brand">Requisition</a>
            </div>

            <ul class="nav navbar-nav">
                <li class="">
                    <a href="index.php">Current</a>
                </li>
                <li class="">
                    <a href="closed-reqs.php">History</a>
                </li>
                <li class="active">
                    <a href="add-header.php">Add</a>
                </li>
            </ul>

            <p class="navbar-text navbar-right">
                <?php echo ($env == "prod") ? "Production" : "Development"; ?> Environment
            </p>

        </div>
    </nav>
    <!-- Nav end -->

    <div class="container">

        <?php if (isset($success) && $success) : ?>

        <div class="row">
            <div class="col-sm-8 col-sm-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Request was saved</h3>
                    </div>
                    <div class="panel-body">
                        <p>Your request has been saved. You may continue or close this window.</p>
                        <?php //<button class="btn btn-primary button-close">Close</button> ?>
                        <a class="btn btn-sm btn-primary" href="index.php" role="button">Continue</a>
                    </div>
                </div>
            </div>
        </div>

        <?php else : ?>

        <div class="row">
            <div class="col-sm-8 col-sm-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Oops!</h3>
                    </div>
                    <div class="panel-body">
                        <a>There was a problem saving your request. Please try again and if the problem continues, contact <a href="mailto:computer@cousin.com">IT</a>.</p>
                        <button class="btn btn-sm btn-primary button-close">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <?php endif ?>

    </div>

    <!-- jquery -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <!-- bootstrap -->
    <script src="js/bootstrap.min.js"></script>
    <!-- datatables -->
    <script type="text/javascript" charset="utf8" src="DataTables/datatables.min.js"></script>
    <!-- daterangepicker -->
    <script src="js/moment.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    <!-- jval -->
    <script src="http://sw:8082/prod/jval-validator/jval.js"></script>
    <!-- inline -->
    <script>
        // Attempt to refresh view
        window.onunload = function () {
            // 1 deep
            window.opener.location.reload(true);
            window.parent.location.reload(true);
            // 2 deep
            window.opener.parent.location.reload(true);
            window.parent.opener.location.reload(true);
            // 3 deep
            window.opener.opener.opener.location.reload(true);
            window.parent.opener.opener.location.reload(true);
            window.opener.opener.parent.location.reload(true);
        };
        // Cancel button
        $('.button-close').click(function () {
            window.close();
        });
    </script>
</body>

</html>
