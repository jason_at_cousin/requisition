<?php
require_once('init.php');

// if $_GET['delete'] = id then delete id
if (isset($_GET['delete'])) {
    $deleteResult = DeleteById($_GET['delete']);
}

$headerResult = GetClosedHeaders();
?>
<!DOCTYPE HTML>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Requisition</title>

  <link rel="icon" href="favicon16x16.ico">
  <link rel="icon" href="favicon.ico">
  <!-- bootstrap styles -->
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/bootstrap-theme.min.css">
  <!-- datatable styles -->
  <link rel="stylesheet" type="text/css" href="DataTables/datatables.min.css">
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
    body {
      padding-bottom: 250px;
      padding-top: 70px;
    }
    #table-container {
      margin-right: 40px;
      padding-left: 40px;
      padding-right: 40px;
    }
    p.navbar-right {
      padding-right: 25px;
    }
  </style>
</head>

<body>
  <!-- Alert Still Testing -->
    <!-- <div class="container">
      <div class="row">
        <div class="col-xs-8 col-xs-offset-1">
          <div class="alert alert-warning alert-dismissable" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <p>
              <strong>Warning:</strong> This system is still being tested and updated.
              You may occasionally find typos or encounter errors. If you do, please
              let <a href="mailto:jeffrabin@cousin.com?subject=Requisition%20Problem">Jeff</a>
              know. Thanks.
            </p>
          </div>
        </div>
      </div>
    </div> -->
  <!-- Alert Still Testing -->

  <!-- Navbar -->
    <nav class="navbar navbar-fixed-top <?php echo $env === "prod"?"navbar-default":"navbar-inverse"; ?>">
      <div class="container-fluid">
        <div class="navbar-header">
          <a href="index.php" class="navbar-brand">Requisition</a>
        </div>

        <ul class="nav navbar-nav">

          <li>
            <a href="index.php">Current</a>
          </li>

          <li class="active">
            <a href="closed-reqs.php">History</a>
          </li>

          <li>
            <a href="add-header.php">Add</a>
          </li>

          <li class="">
              <a href="hist-index.php">Log</a>
          </li>

        </ul>

        <p class="navbar-text navbar-right">
            <?php echo ($env == "prod") ? "Production" : "Development"; ?> Environment :
          <a href="index.php?env=<?php echo ($env == "prod") ? "dev" : "prod"; ?>" class="navbar-link">Change</a>
        </p>
      </div>
    </nav>
  <!-- Navbar -->
  <!-- Table -->
    <div id="table-container">
      <table id="maintable" class="table table-striped table-hover table-condensed">
        <thead>
          <tr>
            <!-- // col 1 -->
            <th>Id</th>
            <!-- // col 2 -->
            <th>Status</th>
            <!-- // col 2.1 -->
            <th>Date Ordered</th>
            <!-- // col 3 -->
            <th>Date Added</th>
            <!-- // col 4 -->
            <th>Deliver By</th>
            <!-- // col 5 -->
            <th>Requested For</th>
            <!-- // col 6 -->
            <th>Request User</th>
            <!-- // col 7 -->
            <th>Request Dept</th>
            <!-- // col 8 -->
            <th>Supplier Name</th>
            <!-- // col 20 -->
            <th>Request Total</th>
            <!-- // col 20.01 -->
            <th>Items</th>
            <!-- // col 14 -->
            <th>Dept Approval</th>
            <!-- // col 15 -->
            <th>Dept Approval Date</th>
            <!-- // col 16 -->
            <th>Dept Approval User</th>
            <!-- // col 17 -->
            <th>Mgt Approval</th>
            <!-- // col 18 -->
            <th>Mgt Approval Date</th>
            <!-- // col 19 -->
            <th>Mgt Approval User</th>
            <!-- // col 21 -->
            <th>Last Updated</th>
            <!-- // col 22 -->
            <th>Last User</th>
            <!-- // col 23 -->
            <th>Notes</th>
            <!-- // col 24 -->
            <th>Justification</th>
            <!-- // col 9 -->
            <th>Supplier Address</th>
            <!-- // col 10 -->
            <th>Supplier Phone</th>
            <!-- // col 11 -->
            <th>Supplier Email</th>
            <!-- // col 12 -->
            <th>Supplier Contact</th>
            <!-- // col 13 -->
            <th>Ship To</th>
          </tr>
        </thead>
        <tbody>

            <?PHP
          // Execute the query and for each result row, make a table row
            $cntr = 0;
            if ($headerResult) {
              foreach ($headerResult as $row) {
                echo "<tr>";
                // col id
                echo "<td>";
                  echo str_replace("{{id}}", $row['RH_ID'], "<a href=\"closed-header.php?id={{id}}\">{{id}}</a>");
                echo "</td>";
                // col status
                echo "<td>" . $row['RH_STATUS']. "</td>";
                // col ordered date
                echo "<td>" . $row['RH_ORDERED_DATETIME']. "</td>";
                // col date added
                $matches;
                preg_match("/^20\d\d-\d{1,2}-\d{1,2}/", $row['RH_DATEADDED'], $matches);
                echo "<td>" . (isset($matches[0]) ? $matches[0] : "") . "</td>";
                // col date requested
                $matches = null;
                preg_match("/^20\d\d-\d{1,2}-\d{1,2}/", $row['RH_DATEREQUESTED'], $matches);
                echo "<td>" . (isset($matches[0]) ? $matches[0] : "") . "</td>";
                // col requested for
                echo "<td>" . $row['RH_REQUESTEDFOR']. "</td>";
                // col request user
                echo "<td>" . $row['RH_REQUSER']. "</td>";
                // col request department
                echo "<td>" . $row['RH_REQDEPT']. "</td>";
                // col supplier name
                echo "<td>" . $row['RH_SUPPLIERNAME']. "</td>";
                // col total
                echo "<td>\$" . number_format(round($row['RH_REQTOTAL'], 2), 2) . "</td>";
                // col items ordered
                $row['ITEMS'] = trim(preg_replace('/\s\/+\s$/', '', $row['ITEMS']));
                $htmlSafeItems = htmlentities($row['ITEMS']);
                echo "<td title=\"$htmlSafeItems\">" . substr($row['ITEMS'], 0, 50) . (strlen($row['ITEMS']) >= 50?'...':'') . "</td>";
                // col dept approval
                echo "<td>" . $row['RH_DEPTAPPROVAL']. "</td>";
                // col dept approval date
                $matches = null;
                preg_match("/^20\d\d-\d{1,2}-\d{1,2}/", $row['RH_DEPTAPPROVALDATE'], $matches);
                echo "<td>" . (isset($matches[0]) ? $matches[0] : "") . "</td>";
                // col depr approval user
                echo "<td>" . $row['RH_DEPTAPPROVALUSER']. "</td>";
                // col mgmt approval
                echo "<td>" . $row['RH_MGTAPPROVAL']. "</td>";
                // col mgmt approval date
                $matches = null;
                preg_match("/^20\d\d-\d{1,2}-\d{1,2}/", $row['RH_MGTAPPROVALDATE'], $matches);
                echo "<td>" . (isset($matches[0]) ? $matches[0] : "") . "</td>";
                // col mgmt approval user
                echo "<td>" . $row['RH_MGTAPPROVALUSER']. "</td>";
                // col last updated
                $matches = null;
                preg_match("/^20\d\d-\d{1,2}-\d{1,2}/", $row['RH_LASTUPDATED'], $matches);
                echo "<td>" . (isset($matches[0]) ? $matches[0] : "") . "</td>";
                // col last user
                echo "<td>" . $row['RH_LASTUSER']. "</td>";
                // col notes
                $row['RH_NOTES'] = trim($row['RH_NOTES']);
                $htmlSafeNotes = htmlentities($row['RH_NOTES']);
                echo "<td title=\"$htmlSafeNotes\">" . substr($row['RH_NOTES'], 0, 50) . (strlen($row['RH_NOTES']) >= 50?'...':'') .  "</td>";
                // echo "<td>" . $row['RH_NOTES']. "</td>";
                // col justification
                $row['RH_JUSTIFICATION'] = trim($row['RH_JUSTIFICATION']);
                $htmlSafeJustification = htmlentities($row['RH_JUSTIFICATION']);
                echo "<td title=\"$htmlSafeJustification\">" . substr($row['RH_JUSTIFICATION'], 0, 50) . (strlen($row['RH_JUSTIFICATION']) >= 50?'...':'') .  "</td>";
                // echo "<td>" . $row['RH_JUSTIFICATION']. "</td>";
                // col supplier address
                echo "<td>" . $row['RH_SUPPLIERADDRESS']. "</td>";
                // col supplier phone
                echo "<td>" . $row['RH_SUPPLIERPHONE']. "</td>";
                // col supplier email
                echo "<td>" . $row['RH_SUPPLIEREMAIL']. "</td>";
                // col supplier contact
                echo "<td>" . $row['RH_SUPPLIERCONTACT']. "</td>";
                // col shipto address
                echo "<td>" . $row['RH_SHIPTOADDR']. "</td>";
                echo "</tr>";
                $cntr++;
              }
            }
            ?>
        </tbody>
      </table>
    </div>
  <!-- Table -->
  <!-- jquery -->
  <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous" ></script>
  <!-- bootstrap -->
  <script src="js/bootstrap.min.js"></script>
  <!-- datatables -->
  <script type="text/javascript" src="DataTables/datatables.min.js"></script>
  <!-- inline -->
  <script type="text/javascript">
    // Datatable
    $(document).ready(function(){
      $('#maintable').DataTable({
        "pageLength": 100,
        "order": [[0, 'desc']],
        stateSave: true,
        "stateDuration": 60*60*24*180,
        "processing": true,
        "language":{
          "loadingRecords": "Loading, please wait...",
          "processing": "Working, please wait..."
        },
        fixedHeader: {
          header: true,
          headerOffset: 50,
          footer: true
        }
      });
    });
  </script>
</body>
</html>
